#pragma region Inclusions librairies

#include <stdio.h>      
#include <stdlib.h>     
#include <unistd.h>     
#include <math.h>
#include <fcntl.h>
#include <array>
#include <GL/glut.h>

#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/drawings.h"	// Drawing functions
#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/beziers.h"	// Define blocks
#include "../lib/headers/cube.h"	// Define blocks

#pragma endregion

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -03.0
#define MAX_X_BOUNDING  03.0
#define MIN_Y_BOUNDING -03.0
#define MAX_Y_BOUNDING  03.0

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y);

float u = 0.5;
float v = 0.5;
float radius = 1.0;
Vector x,y;
Point lightPos;
bool auto_scroll;

#pragma endregion

Cube c;

GLfloat Light0Pos[4] = { 2.0, 1.5, 1.5, 0.0 };

// GLfloat Light0Dif[4] = { 1.0, 1.0, 1.0, 0.0 };	// Default
GLfloat Light0Dif[4] = { 1.0, 0.0, 0.0, 0.0 };		// Style 1
// GLfloat Light0Dif[4] = { 0.0, 1.0, 0.0, 0.0 };		// Style 2
// GLfloat Light0Dif[4] = { 0.0, 0.0, 1.0, 0.0 };		// Style 3
GLfloat Light0Amb[4] = { 1.0, 1.0, 1.0, 0.0 };	// Default
// GLfloat Light0Amb[4] = { 1.0, 0.0, 0.0, 0.0 };		// Style 1
// GLfloat Light0Amb[4] = { 0.0, 1.0, 0.0, 0.0 };		// Style 2
// GLfloat Light0Amb[4] = { 0.0, 0.0, 1.0, 0.0 };		// Style 3
GLfloat Light0Spe[4] = { 1.0, 1.0, 1.0, 0.0 };	// Default
// GLfloat Light0Spe[4] = { 1.0, 0.0, 0.0, 0.0 };		// Style 1
// GLfloat Light0Spe[4] = { 0.0, 1.0, 0.0, 0.0 };		// Style 2
// GLfloat Light0Spe[4] = { 0.0, 0.0, 1.0, 0.0 };		// Style 3
GLfloat fogColors[4] = { 0.3, 0.5, 0.0, 0.0};

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP5 : Lumieres");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	c = Cube();
	Vector v = Vector(1.0, 0.0, 0.0);

	// Affichages vecteurs plan XY
	x = Vector(2.0,0.0,0.0);
	y = Vector(0.0,3.0,0.0);

	auto_scroll = false;

	// Verifie la superposition des objets
	glEnable(GL_DEPTH_TEST);

	// Ajoute lumières
	glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, Light0Dif);
	glLightfv(GL_LIGHT0, GL_AMBIENT, Light0Amb);
	glLightfv(GL_LIGHT0, GL_SPECULAR, Light0Spe);

	// Allume lumières
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glFogf(GL_FOG_MODE, GL_LINEAR);
	glFogf(GL_FOG_DENSITY, 1.5);
	glFogfv(GL_FOG_COLOR, fogColors);
	glFogf(GL_FOG_START, 0);
	glFogf(GL_FOG_END, 2);
	glEnable(GL_FOG);

	// Ne trace que les cotés "positifs" d'une face (mm coté normale)
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	lightPos = Point(Light0Pos[0], Light0Pos[1], Light0Pos[2]);
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	gluLookAt(
		sin(u*2*M_PI) * radius, cos(v*2*M_PI) * tan(v*2*M_PI) * radius, cos(u*2*M_PI) * radius,
		0.0, 0.0, 0.0,
		0.0, 1.0, 0.0
	);
	c.drawCube(false);
	lightPos.doGlVertex3f_Alone();
	if (auto_scroll) {
		u += 0.00005;
		usleep(160);
		glutPostRedisplay();
	}
	// DrawGrid(MIN_X_BOUNDING,MAX_X_BOUNDING,MIN_Y_BOUNDING,MAX_Y_BOUNDING);
	// DrawLine(x);
	// DrawLine(y);
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		// case 113:
		// 	exit(1);
		// 	break; 
		case 100:
			u = u-0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 113:
			u = u+0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 122:
			v = v-0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 115:
			v = v+0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 114:
			auto_scroll = !auto_scroll;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();

		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion
