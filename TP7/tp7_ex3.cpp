#include <math.h>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>

#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/model.h"	// Model class
#include "../lib/headers/drawings.h"	// Drawing functions

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
float MIN_X_BOUNDING = -15.0;
float MAX_X_BOUNDING =  15.0;
float MIN_Y_BOUNDING = -15.0;
float MAX_Y_BOUNDING =  15.0;
float MIN_Z_BOUNDING = -15.0;
float MAX_Z_BOUNDING =  15.0;

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y);
GLvoid readModel(const char* path, Point*tab);

// Render and light parameters
bool drawAll;
GLenum drawMode;
Vector x,y;
double radius;
float u = 0.25;
float v = 0.5;
Point lightPos;
Vector cameraVec;
float current;
GLfloat Light0Pos[4];
Point center;

// Actual content
Model m;

#pragma endregion

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(600, 50);
	glutCreateWindow("OpenGL TP7 : Maillages Avances");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	// Ajoute lumières
	glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);

	// Allume lumières
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	radius = 0.0;
	m = Model("../models/triceratops.off");

	x = Vector(Point(0.0,0.0,0.0), Point(2.0,0.0,0.0));
	y = Vector(Point(0.0,0.0,0.0), Point(0.0,3.0,0.0));

	float lowest = m.getLowestBoundary();
	current = (m.getLargestBoundary());

	float largest = current;

	float delta = largest - lowest;
	
	center = Point(0.0,0.0,0.0); // Point(abs(largest-lowest)/2.0+lowest, abs(largest-lowest)/2.0+lowest, abs(largest-lowest)/2.0+lowest);

	center = m.getCentroid();

	if (delta < 0.5) {
		lowest *= 5.0f;
		largest *= 5.0f;
		std::cout << "NEW  --  lowest / largest : " << lowest << " / " << largest << std::endl;
	}

	float coef = 0.0;

	MIN_X_BOUNDING = center.getX() - (1.0+coef) * delta	;
	MAX_X_BOUNDING = center.getX() + (1.0+coef) * delta	;
	MIN_Y_BOUNDING = center.getY() - (1.0+coef) * delta	;
	MAX_Y_BOUNDING = center.getY() + (1.0+coef) * delta	;
	MIN_Z_BOUNDING = center.getZ() - (1.0+coef) * delta	* 2.0; 
	MAX_Z_BOUNDING = center.getZ() + (1.0+coef) * delta	* 2.0; 

	std::cout << "boundaries established : " << MIN_X_BOUNDING << "," << MAX_X_BOUNDING << std::endl;

	Light0Pos[0] = 0.0;
	Light0Pos[1] = 0.0;
	Light0Pos[2] = MIN_Z_BOUNDING;
	Light0Pos[3] = 0.0;
	lightPos = Point(Light0Pos[0], Light0Pos[1], Light0Pos[2]);

	radius = 1.0;

	drawAll = true;
	drawMode = GL_TRIANGLES;
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	Vector toCamNorm = Vector( cos(u*2*M_PI) * sin(v*M_PI), cos(v*M_PI), sin(v*M_PI) * sin(u*2*M_PI));
	Vector cam = toCamNorm * radius;
	gluLookAt(
		center.getX() + toCamNorm.getX(), center.getY() + toCamNorm.getY(), center.getZ() + toCamNorm.getZ(),
		center.getX(), center.getY(), center.getZ(), // // 0.0, 0.0, 0.0,	// 
		0.0, 1.0, 0.0
	);

	float deltaX = abs(MAX_X_BOUNDING - MIN_X_BOUNDING) / 2.0;
	float coef = 1.0;

	// ortho : 
	glOrtho(center.getX() - radius * coef * deltaX,
		center.getX() + radius * coef * deltaX,
		center.getY() - radius * coef * deltaX,
		center.getY() + radius * coef * deltaX,
		center.getZ() - radius * coef * deltaX ,
		center.getZ() + radius * coef * deltaX 
	);

	DrawPoint(lightPos);
	DrawLine(Vector(Point(0.0,0.0,0.0), lightPos));
	DrawLine(x);
	DrawLine(y);
	DrawPoint(center);
	
	if (drawMode == GL_LINES) {
		m.drawGaussianImage();
	} else {
		m.drawElements();
	}
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, MIN_Z_BOUNDING, MAX_Z_BOUNDING);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier
GLvoid window_key(unsigned char key, int x, int y) 
{  
	double step = abs(MAX_X_BOUNDING - MIN_X_BOUNDING) / 200.0;
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;

		case KEY_Z:
			v = (v < 0.01) ? 0.01 : v-0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case KEY_Q:
			u = u+0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case KEY_S:
			v = (v > 0.99) ? 0.99 : v+0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case KEY_D:
			u = u-0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case KEY_A:
			radius = (radius > 0.01) ? radius - 0.01 : 0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_E:
			radius = radius + 0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;
		
		case KEY_T:
			center.translate(Vector(0.0,step,0.0));
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_F:
			center.translate(Vector(-step,0.0,0.0));
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_G:
			center.translate(Vector(0.0,-step,0.0));
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_H:
			center.translate(Vector(step,0.0,0.0));
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_R:
			center.translate(Vector(0.0,0.0,-step));
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_Y:
			center.translate(Vector(0.0,0.0,step));
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		case KEY_X:
			drawMode = (drawMode == GL_LINES) ? GL_TRIANGLES : GL_LINES;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;

		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion