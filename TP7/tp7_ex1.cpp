#include <stdio.h>      
#include <stdlib.h>     
#include <iostream>     
#include <math.h>
#include <fcntl.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/drawings.h"	// Drawing functions
#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/beziers.h"	// Define blocks
#include "../lib/headers/cube.h"	// Define blocks
#include "../lib/headers/model.h"	// Define blocks
#include "../lib/headers/cylinder.h"	// Define blocks

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
float MIN_X_BOUNDING = -15.0;
float MAX_X_BOUNDING =  15.0;
float MIN_Y_BOUNDING = -15.0;
float MAX_Y_BOUNDING =  15.0;
float MIN_Z_BOUNDING = -15.0;
float MAX_Z_BOUNDING =  15.0;

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y);
GLvoid readModel(const char* path, Point*tab);

Vector x,y;
double radius;
float u = 0.5;
float v = 0.5;
Point lightPos;

double* normalArray;
double* vertices;
unsigned int* faceArray;
unsigned int nbFaces;

GLfloat Light0Pos[4];

Cylinder c;

#pragma endregion

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(600, 50);
	glutCreateWindow("OpenGL TP7 : Maillages Avances");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	// Ajoute lumières
	glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);

	// Allume lumières
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	radius = 1.0;
	c = Cylinder(10,10,30);

	float lower = c.getLowerBoundary();
	float largest = c.getHigherBoundary();

	largest = abs(largest);

	float coef = 0.5;

	MIN_X_BOUNDING = /*floor*/ (1.0+coef) * (-largest);
	MAX_X_BOUNDING = /*ceil */ (1.0+coef) * (largest);
	MIN_Y_BOUNDING = /*floor*/ (1.0+coef) * (-largest);
	MAX_Y_BOUNDING = /*ceil */ (1.0+coef) * (largest);
	MIN_Z_BOUNDING = /*floor*/ (1.0+coef) * (-largest) * 2.0;
	MAX_Z_BOUNDING = /*ceil */ (1.0+coef) * (largest)  * 2.0;

	glEnable(GL_DEPTH_TEST);

	Light0Pos[0] = 0.0;
	Light0Pos[1] = 0.0;
	Light0Pos[2] = MIN_Z_BOUNDING;
	Light0Pos[3] = 0.0;
	lightPos = Point(Light0Pos[0], Light0Pos[1], Light0Pos[2]);
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	Vector toCamNorm = Vector( cos(u*2*M_PI) * sin(v*M_PI), cos(v*M_PI), sin(v*M_PI) * sin(u*2*M_PI));
	Vector cam = toCamNorm * radius;
	gluLookAt(
		toCamNorm.getX(), toCamNorm.getY(), toCamNorm.getZ(),
		0.0, 0.0, 0.0,
		0.0, 1.0, 0.0
	);
	DrawPoint(lightPos);

	c.drawElements();
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, MIN_Z_BOUNDING, MAX_Z_BOUNDING);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier
GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		// case 113:
		// 	exit(1);
		// 	break; 
		case 100:
			u = u-0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 113:
			u = u+0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 122:
			v = v-0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 115:
			v = v+0.01;
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
		break;

		case 43:
			c.addFacettes();
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;
		
		case 45:
			c.removeFacettes();
			glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
			glutPostRedisplay();
			break;
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion