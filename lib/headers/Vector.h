/**
 * =====================================================================
 * FILE : Vector.h
 * DESC : Declaration of the Vector class, used to represent a 3D vector
 *        in 3D space. Used in all subsequent OpenGL practicals.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere in early Feb 2019
 * =====================================================================
 */

#ifndef VECTOR_H
#define VECTOR_H
#pragma once

#include <string>

class Point;

class Vector {
	private:
		// Coordinates
		double x, y, z;

	public:
		/**
		 * Basic constructor, constructs a null vector.
		 */
		Vector();
		/**
		 * Copy constructor
		 */
		Vector(const Vector &v);
		/**
		 * Constructor creating a vector from Point p to p2
		 */
		Vector(const Point& p, const Point& p2);
		/**
		 * Constructor creating a vector of coordinates x,y,z :
		 */
		Vector(double xx, double yy, double zz);

		double getX()const;
		double getY()const;
		double getZ()const;
		// Get angle of vector on XY plane
		double getAlpha()const;
		double getAlphaDeg()const;
		// Get angle of vector on XZ plane
		double getTheta()const;
		double getThetaDeg()const;

		void setX(double xx);
		void setY(double yy);
		void setZ(double zz);

		/**
		 * returns the length of the vector
		 */
		double norme() const;

		/**
		 * ensures the vector is resized to have a length of 1
		 */
		Vector&normalize();

		/**
		 * Returns the result of the addition of each one of the
		 * coordinates multiplied together
		 */
		double scalar(Vector v2);

		/**
		 * Returns the vector resulting of the mutiplication of the two vectors
		 */
		Vector vectoriel(Vector v2);

		/**
		 * Returns the angle in-between two vectors.
		 */
		double angle(Vector v2);

		/**
		 * scales a vector by a scalar value given in argument
		 */
		void scale(double n);

		/**
		 * Returns the vector values in a array of double precision floating points
		 */
		double* toArray();

		/**
		 * Applies a vector to a certain point (gives the coordinate of the new point in the vector)
		 */
		void applyToPoint(double px, double py, double pz);

		/**
		 * Element-wise scalar multiplication of a vector.
		 */
		Vector& operator* (double scalar);

		/**
		 * Unary minus operator, reverses the vector's coordinates
		 */
		Vector& operator-();

		/**
		 * Vector addition, following Chasles' principle
		 */
		Vector operator+ (const Vector& v) const;

		/**
		 * Output stream operator for printing to stdout or file
		 */
		friend std::ostream& operator<<(std::ostream& out, const Vector& v);

		/**
		 * Explicit cast operator into a pointer for a Point
		 */
		operator Point*() const;

		/**
		 * Explicit cast operator into a Point
		 */
		operator Point() const;
};

#endif
