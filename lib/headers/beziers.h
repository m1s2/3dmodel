#ifndef BEZIER_H
#define BEZIER_H

#include "Point.h"

#include <vector>

/**
 * Permet de calculer une courbe de Beziers avec la formule des polynômes de Bernstein contenant 
 * `nbU` points, pour une courbe avec `nbPC` points de contrôle, contenus dans `pointsControles`. 
 * Les points de la courbe résultante seront stockés dans `resultat`.
 */
void courbeBezier_Bernstein(Point* pointsControles, long nbPC, long nbU, Point* resultat);

/**
 * Permet de calculer une courbe de Beziers avec la formule des polynômes de Bernstein contenant 
 * `nbU` points, pour une courbe avec `nbPC` points de contrôle, contenus dans `pointsControles`. 
 * Les points de la courbe résultante seront stockés dans `resultat`.
 */
void surfaceBezier_Bernstein(Point** pointsControles, long nbPCx, long nbPCy, long nbU, long nbV, Point** resultat);

/**
 * Permet de calculer tous les `nbU` points d'une courbe de Béziers grâce à l'algorithme de Casteljau,
 * sur une courbe avec `nbPC` points de contrôle, stockés dans `*pointsControles`. Les points de la courbe
 * résultante seront stockés dans `resultat`.
 */
void courbeBeziersCastelnau(Point* pointsControles, long nbPC, long nbU, Point* resultat);

/**
 * Equivalent à la fonction au dessus, avec un retour sur la construction de Casteljau :
 */
void courbeBeziersCastelnau(Point* pointsControles, long nbPC, long nbU, Point* resultat, std::vector<std::vector<Point>> &currentConstruction);

/**
 * Calcule le point `u` d'une courbe de Beziers grâce à l'algorithme récursif de Casteljau. Les points de
 * contrôle sont au nombre de `nbPC` et sont contenus dans `nbPC`.
 */
Point casteljauRecursif(double u, Point* ptsCtrl, long nbPC);

/**
 * Equivalent à la fonction au dessus, avec un retour sur la construction de Casteljau :
 */
Point casteljauRecursif(double u, Point* ptsCtrl, long nbPC, std::vector<std::vector<Point>> &currentConstruction);

/**
 * Permet de calculer le polynôme de Bernstein pour le point `u` avec `n` points de contrôle, pour la valeur `i`.
 */
double coefBernstein(double n, double i, double u);

/**
 * Permets de faire l'opération "i parmi n" (voir définition dans le cours).
 */
double nCm(double n, int i);

/**
 * Permets de faire une factorielle d'un nombre de type double.
 * ATTENTION : Le nombre que vous donnez sera d'abord arrondi avant de calculer sa factorielle. Si vous donnez 49.8,
 * la factorielle de 50 sera retournée (si elle peut rentrer dans 64 bits, taille d'un double)
 */
double fact(double n);

/**
 * Actuel appel à la factorielle avec un nombre rond. à ne pas utiliser seule, voir `fact(double)`
 */
double factorial(double n);

#endif