/**
 * =====================================================================
 * FILE : Point.h
 * DESC : Contains the declaration of the class Point, used in all 
 *        practicals from the dawn of time.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere around early Feb.2019
 * =====================================================================
 * 
 * Note : Includes : 
 *   - Vector
 *     - string
 */

#ifndef POINT_H
#define POINT_H
#pragma once

#include "Vector.h"

// class Vector;

class Point {
	private:
		Vector normal;
	public:
		double x,y,z;
		Point();
		Point(const Point &p);
		Point(double xx, double y, double zz);

		double getX() const;
		double getY() const;
		double getZ() const;

		void setX(double xx);
		void setY(double yy);
		void setZ(double zz);

		void multiply(double k);

		Point ProjectOnLine(Point Point1Line, Point Point2Line);
		Point ProjectOnLine(Vector vecteur, Point Pline);
		Point ProjectOnPlane(Point planePoint, Vector normalVect);

		Point translate(const Vector &v);

		double* toArray() const;

		void doGlVertex3f() const;
		void doGlVertex3f_Alone() const;

		Vector getNormal() const;
		double* getNormalArray() const;
		void setNormal(const Vector& v);

		bool operator==(const Point& p) const;
		bool operator!=(const Point& p) const;

		Point& operator+=(const Point& p);
		Point& operator/ (const double scalar);
		Point& operator/=(const double scalar);

		Point& operator+ (const Point& p) const;

		bool operator< (const Point& p) const;
		bool operator> (const Point& p) const;
		bool operator<=(const Point& p) const;
		bool operator>=(const Point& p) const;

		Vector operator- (const Point& pOrigin) const;
		friend std::ostream& operator<<(std::ostream&out, const Point&p);
};

#endif