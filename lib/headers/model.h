#ifndef MODEL_H
#define MODEL_H

#include "face.h"

class Model {
private:
	std::vector<Point> vertices;
	std::vector<Face> faces;
	float min_x, max_x, min_y, max_y, min_z, max_z;
	Point centroid;
public:
	Model();
	Model(const char*);
	void readFile(const char*);
	void draw() const;
	void printfInfo() const;
	std::vector<float> getModelBoundaries() const;
	int getNbFaces() const {return faces.size();}
	float getLargestBoundary() const;
	float getLowestBoundary() const;
	double* getNormals() const;
	double* getVertexArray() const;
	unsigned int* getFaceArray() const;
	void drawGaussianImage() const;
	void drawElements() const;
	void drawWireframe() const;
	void drawDihedralAngles(double threshold) const;
	Point getCentroid() const;
};

#endif