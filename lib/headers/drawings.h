/**
 * =====================================================================
 * FILE : drawings.h
 * DESC : Contains all the necessary drawing functions for the later
 *        practicals. Uses GL/GLU methods.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere near the beginning of March 2019
 * =====================================================================
 */

#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "Point.h"

#include <vector>
#include <GL/glut.h>

/**
 * Draws vector `v` from the origin.
 */
GLvoid DrawLine(Vector v);

/**
 * Draws vector `v` from point `p`.
 */
GLvoid DrawLine(Vector v, Point p);

/**
 * Draws vector `v` from point `p` using the colors `r,g,b` with width `size`.
 */
GLvoid DrawLine(Vector v, Point p, GLdouble r, GLdouble g, GLdouble b, GLdouble size);

/**
 * Draws point `p`
 */
GLvoid DrawPoint(Point p);

/**
 * Draws point `p` with colors `a,b,c`
 */
GLvoid DrawPoint(Point p, double a, double b, double c);

/**
 * Draws a line from the `nbPoints` points in `pointsOfCurve`.
 */
GLvoid DrawCurve(const Point* pointsOfCurve, long nbPoints);

/**
 * Draws a line from the `nbPoints` points in `pointsOfCurve`.
 */
GLvoid DrawCurve(const Point* pointsOfCurve, const Point endPoint, long nbPoints);

/**
 * Draws a line from the `nbPoints` points in `pointsOfCurve`, 
 * assuring the last part of the curve is drawn.
 */
GLvoid DrawCurve(const Point* pointsOfCurve, const long nbPoints, double a, double b, double c);

/**
 * Draws a line from the `nbPoints` points in `pointsOfCurve`.
 */
GLvoid DrawCurve(std::vector<Point> pointsOfCurve, long nbPoints, int index);

/**
 * Draws a 2D grid from `lowerX, lowerY`, to `higherX, higherY`
 */
GLvoid DrawGrid(double lowerX, double lowerY, double higherX, double higherY);

/**
 * Draws the points of a 2D matrix of points individually.
 */
GLvoid DrawPointFrame(Point**, double, double);

/**
 * Draws a cylinder (highly specific)
 */
GLvoid DrawCylinder(Point**, double);

/**
 * Draws a cone (highly specific)
 */
GLvoid DrawCone(Point,Point*,double);

/**
 * Draws a sphere (highly specific)
 */
GLvoid DrawSphere(Point poleNord, Point poleSud, Point** surface, int nbParalleles, int nbMeridiens);

/**
 * Draws the top of a sphere.
 */
GLvoid DrawSphereTop(Point poleNord, Point poleSud, Point** surface, int nbParalleles, int nbMeridiens);

/**
 * Draws the wireframe of the loaded model
 */
GLvoid DrawSphereWireFrame(Point poleNord, Point poleSud, Point** points, double x, double y);

#endif