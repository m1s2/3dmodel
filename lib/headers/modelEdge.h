/**
 * =====================================================================
 * FILE : modelEdge.h
 * DESC : Contains the declaration of the class modelEdge, which is used
 *        to load a 3d mesh using an edge topology.
 * AUTH : Thibault de Villèle
 * DATE : March 20th, 2019
 * =====================================================================
 */

#ifndef MODELEDGE_H
#define MODELEDGE_H

#include "edges.h"

#include <algorithm>

class modelEdge {
private:
	std::vector<Point> vertices;
	std::vector<edge> edges;
	std::vector<faceEdge> faces;
	double min_x, min_y, min_z, max_x, max_y, max_z;

	/**
	 * Gets the array representing each vertex's normal.
	 */
	double* getNormalArray() const;

	/**
	 * Gets the array representing each vertex
	 */
	double* getVertexArray() const;

	/**
	 * Returns the right way to iterate through the vertices
	 * as to form the original model.
	 */
	unsigned int* getFaceArray() const;

	unsigned int* getEdgesArray() const;

public:
	/**
	 * Simple constructor, doesn't actually constructs anything
	 */
	modelEdge();

	/**
	 * Constructor that loads a mesh from a file.
	 */
	modelEdge(const char*);

	/**
	 * Reads a file to load a model.
	 */
	void readFile(const char*);

	/**
	 * Draws the model using `glDrawElements()`.
	 */
	void drawElements() const;

	/**
	 * Draw wireframe usin!g `glDrawElements()`
	 */
	void drawElementsWireframe() const;

	/**
	 * Gets the values to pass to `glOrtho()` so the model is visible
	 */
	float* getBoundaries() const;

	/**
	 * Prints info about the model and its mesh.
	 */
	void printInfo(bool full_vect = true) const;

	/**
	 * Moves the endpoint of an edge
	 */
	void moveEdgeForward(const ui, const ui);

	/**
	 * Moves the startpoint of an edge
	 */
	void moveEdgeBackward(const ui, const ui);

	/**
	 * Removes an edge from the model.
	 */
	void removeEdge(ui);

	/**
	 * same as removeEdge(), but choosing a random edge
	 */
	void removeRandomEdge();

	/**
	 * Draw only the points of the model
	 */
	void drawPoints() const;

	/**
	 * Draw only the edges of the model, for debug purposes.
	 */
	void drawEdges() const;

	/**
	 * Simplify the model using a grid of l x w x d
	 */
	void simplify(const int l, const int w, const int d);

	/**
	 * Returns the indices of the three vertices composing the face.
	 */
	std::vector<unsigned int> getVertexIndices(const ui) const;

	/**
	 * Returns faces.size()
	 */
	std::size_t getNbFaces() const {return faces.size();}

	/**
	 * returns a copy of the vertices.
	 */
	std::vector<Point> getVertices() const {return vertices;}

	/**
	 * returns the faces
	 */
	std::vector<faceEdge> getFaces() const {return faces;}

	/**
	 * return edges
	 */
	std::vector<edge> getEdges() const {return edges;}

	void setEdges(const std::vector<edge>& vectEdge) {
		this->edges = vectEdge;
	}

	void setPoints(const std::vector<Point>& vectPoint) {
		this->vertices = vectPoint;
	}

	void setFaces(const std::vector<faceEdge>& vectFaces) {
		this->faces = vectFaces;
	}

	/**
	 * subdivises a face of a model :
	 */
	void subdiviseFace(const ui);
};

#endif //define MODELEDGE_H