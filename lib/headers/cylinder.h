/**
 * =====================================================================
 * FILE : cylinder.h
 * DESC : Contains the declaration of the Cylinder class, used in some
 *        later practicals.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere near end-feb 2019
 * =====================================================================
 */

#ifndef CYLINDER_H
#define CYLINDER_H

#include "Point.h"

#include <vector>

class Cylinder {
private:
	unsigned int m_nbFacettes;
	unsigned int m_height;
	unsigned int m_rayon;
	std::vector<Point> m_faceUp;
	std::vector<Point> m_faceDown;
public:
	Cylinder();
	Cylinder(unsigned int nbFacettes);
	Cylinder(unsigned int, int, int);
	void draw() const;
	void drawAlone() const;
	void generateFacettes();
	void addFacettes();
	void removeFacettes();
	unsigned int getNbVertexForMesh() const;
	// getters :
	double* getTriangleMesh() const;
	double* getNormals() const;
	double getLowerBoundary() const;
	double getHigherBoundary() const;
	unsigned int* getFaceIndex() const;
	void toOffFile(const std::string&) const;

	/**
	 * Draws the cylinder using the `glDrawElements()` method.
	 */
	void drawElements() const;

	void drawDihedralAngles(double threshold) const;
};

#endif