/**
 * =====================================================================
 * FILE : options.h
 * DESC : Contains a struct used to carry all the parameters of an OpenGL
 *        program. Is used as a signleton in most practicals.
 * AUTH : Thibault de Villèle
 * DATE : March 15th, 2019
 * =====================================================================
 */

#ifndef OPTIONS_H
#define OPTIONS_H

#include "definitions.h"
#include "Point.h"

#include <GL/glut.h>

struct camera {
	// Position of the camera inside the space :
	double eyeX;
	double eyeY;
	double eyeZ;

	// Position of the viewed item :
	double centerX;
	double centerY;
	double centerZ;

	// Up vector of the camera :
	double upX;
	double upY;
	double upZ;

	// Orbit ratios given by the parent options struct
	double horiOff;
	double vertOff;

	// General zoom level :
	double zoomLevel;

	// Constructors :
	camera();
	camera(const camera&);
	camera(double,double,double);

	// Get parameters from another camera object
	void setParameters(const camera&);

	// Calls `gluLookat()`.
	void update();
	// Compute new orbit positions of the camera
	void computeOrbit();

	// Move the point the camera aims at :
	void setCenter(double,double,double);
	// Move the point the camera aims at :
	void setCenter(const Point&);

	// Move the camera
	void setPosition(double,double,double);
	// Move the camera
	void setPosition(const Point&);

	// Get closer to the object
	void zoom(const double);
	// Get further from the object
	void unzoom(const double);

	/**
	 * Sets the camera in orbit around the center point
	 * by the factors given in argument
	 */
	void setOrbitAt(double,double);

	/**
	 * debug
	 */
	void printparams();
};

struct options {
	// Light parameters :
	GLfloat* Light0Pos;
	GLfloat* Light0Amb;
	GLfloat* Light0Dif;
	GLfloat* Light0Spe;
	// Light position :
	Point lightPos;

	// Bounding box parameters for the viewport : 
	double MIN_X_BOUNDING;
	double MAX_X_BOUNDING;
	double MIN_Y_BOUNDING;
	double MAX_Y_BOUNDING;
	double MIN_Z_BOUNDING;
	double MAX_Z_BOUNDING;

	// Vectors allowing for easy differenciation of X and
	// Y axes, zhen degugging
	Vector x;
	Vector y;

	// Camera parameters :
	camera cameraOptions;
	double horizontalOffset;
	double verticalOffset;
	double stepHorizontal;
	double stepVertical;

	// For some cases, we have multiple draw modes :
	GLenum drawMode;
	bool drawAll;

	// Constructor :
	options();
	// Camera position setter :
	void setCamera(const camera&);

	void moveCameraUp();
	void moveCameraDown();
	void moveCameraLeft();
	void moveCameraRight();

	void setCameraCenter(const Point&);
	void setCameraCenter(double,double,double);

	void updateLight() const;

	void zoom(const double);

	void unzoom(const double);

	void updateCamera();

	void updateOrtho() const;
	
	void printOrtho() const;

	// Set the ortho values from a float array
	void setOrthoArray(const double*);

	/**
	 * debug
	 */
	void printparams();
};

#endif	// OPTIONS_H