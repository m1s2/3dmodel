/**
 * =====================================================================
 * FILE : cube.h
 * DESC : Contains the declaration of the Cube class, used in one TP.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere near end-feb 2019
 * =====================================================================
 */

#ifndef CUBE_H
#define CUBE_H

#include <array>

#include "./Point.h"

class Cube {
private:
	std::array<std::array<Point, 4>, 2> _points;
public:	
	Cube();
	// Cube(Point);
	// Cube(int, int, int);
	// Cube(Point, int, int, int);
	void draw() const;
	void drawCube(bool) const;
	void translate(const Vector& v);
};

#endif