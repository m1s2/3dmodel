#ifndef HALFEDGES_H
#define HALFEDGES_H

#include "Vector.h"
#include "Point.h"

#include <vector>

struct halfEdge {
	unsigned int endVertex;
	unsigned int faceRelated;
	unsigned int otherEdge;
	unsigned int nextHalfEdge;

	/**
	 * Basic constructor. Initialises everything at 0, not to 
	 * cause segfaults when accessing vectors of faces or vertices
	 */
	halfEdge();

	/**
	 * Constructor providing the details :
	 *    - of the end vertex of this edge (known when parsing the file)
	 *    - of the face related to it (since we are parsing the file face by face)
	 */
	halfEdge(const unsigned int end, const unsigned int face) : 
		endVertex(end), 
		faceRelated(face) {}

	/**
	 * Copy constructor, in case we need it (?)
	 */
	halfEdge(const halfEdge& he) : 
		endVertex(he.endVertex), 
		faceRelated(he.faceRelated), 
		otherEdge(he.otherEdge),
		nextHalfEdge(he.nextHalfEdge) {}

	/**
	 * Sets the 'otherEdge' field in the object.
	 */
	void setOtherEdge(const unsigned int oe);

	/**
	 * Sets the next edge in the model
	 */
	void setNextEdge(const unsigned int);

	/**
	 * checks if a given edge is the same
	 */
	bool operator==(const halfEdge& e) const;

	/**
	 * checks if a given edge is NOT the same
	 */
	bool operator!=(const halfEdge& e) const;
};

struct faceHalfEdge {
	/**
	 * Do we really need a face struct ? Don't think so. 
	 * All we need is a few indices of edges to start the
	 * faces from, and we're golden.
	 */
};

class modelHalfEdge {
private:
	std::vector<Point> vertices;
	std::vector<halfEdge> edges;
	std::vector<unsigned int> faces;

	double min_x, min_y, max_x, max_y, min_z, max_z;

	/**
	 * Returns an array of the X,Y,Z coordinates
	 * of the vertices in this model
	 */
	double* getVertexArray() const;

	/**
	 * Returns an array of the X,Y,Z coordinates
	 * of the normal to the vertices of this model.
	 */
	double* getNormalArray() const;

	/**
	 * Returns an array of the indexes to use by
	 * `glDrawElements()` to trace a model in 3D space
	 */
	unsigned int* getFaceArray() const;

public:
	/**
	 * Basic constructor.
	 */
	modelHalfEdge();

	/**
	 * Constructor that reads a given OFF file
	 */
	modelHalfEdge(const std::string& pathName);

	/**
	 * Reads the data stored in an OFF file
	 */
	void readFile(const std::string& pathName);

	/**
	 * Draw the model using ` glDrawElements()`
	 */
	void drawElements() const;

	/**
	 * Draw the model using ` glDrawElements()` with wireframe
	 */
	void drawElementsWireframe() const;

	/**
	 * Draw the model using ` glDrawElements()` only points
	 */
	void drawPoints() const;

	/**
	 * Returns the boundaries to be used by `glOrtho` later
	 */
	double* getBoundaries() const;

	/**
	 * Prints some info about the model.
	 */
	void printInfo() const;

	/**
	 * print a bounding box around the model
	 */
	void drawBoundingBox() const;

	/**
	 * refines the model by adding 3 vertices to each face (triforce-style)
	 */
	void refine();

	/**
	 * subdivise the model using the butterfly model
	 */
	void subdiviseButterfly();

	/**
	 * computes the position of a point according to the 
	 * butterfly subdivision pattern in the currently loaded mesh.
	 */
	Point& computeButterflyPosition(const unsigned int);
};

#endif