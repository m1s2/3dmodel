/**
 * =====================================================================
 * FILE : face.h
 * DESC : Contains the necessary structs for the class model.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere in march 2019
 * =====================================================================
 */

#ifndef FACE_H
#define FACE_H

#include "drawings.h"

struct meshFace;

class Face {
private:
	int north;
	int left;
	int right;
	Vector normal;
public:
	Face(int,int,int);
	// void draw() const;
	int getNorth() const {return north;}
	int getLeft() const {return left;}
	int getRight() const {return right;}
	Vector getNormal() const {return normal;}
	void setNormal(const Vector&v) {this->normal = Vector(v).normalize();}
	unsigned int* getArray() const;
	meshFace toMeshFace(const std::vector<Point>) const;
	meshFace toMeshFace(const double*) const;
};

struct meshFace {
	Point pnorth;
	Point pleft;
	Point pright;
	Vector normal;

	meshFace(const Face& f, const std::vector<Point> source);
	meshFace(const Face& f, const double* source);

	double getDihedralAngle(const meshFace& mf) const;
	std::vector<Point> getCommonPoints(const meshFace& mf) const;
};

#endif