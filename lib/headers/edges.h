/**
 * =====================================================================
 * FILE : edges.h
 * DESC : Contains the necessary structures for a modelEdge class to be
 *        possible. Thus, contains `struct edge` and `struct faceEdge`.
 * AUTH : Thibault de Villèle
 * DATE : March 20th, 2019
 * =====================================================================
 */

#ifndef EDGES_H
#define EDGES_H

#include "face.h"

#include <limits>

#define ui unsigned int

/**
 * This structure will be inside a vector for the modelEdge class. As such, it needs
 * the information for the start and end vertices, as well as the connected faces.
 */
struct edge {
	// Data inside the edge :
	unsigned int startPoint;
	unsigned int endPoint;
	unsigned int firstFace;
	unsigned int secondFace;

	edge() : startPoint(0), endPoint(0), firstFace(0), secondFace(0) {}

	/**
	 * Edge constructor. The user must provide all necessary
	 * information about the start and end vertex, since they
	 * are known immediately when parsing the file.
	 */
	edge(const ui start, const ui end, const ui f_face);

	/**
	 * Sets the second face
	 */
	void setSecondFace(const ui secFace);

	/**
	 * Checks if the edge already has a second
	 * face attached to it or not
	 */
	bool hasSecondFace() const;

	/**
	 * returns true if this edge has the face asked for in its arguments
	 */
	ui hasFace(const ui) const;

	/**
	 * Allows to get the other face of an edge
	 */
	unsigned int getOtherFace(const ui) const;

	/**
	 * Checks if the edge is the same
	 */
	bool operator==(const edge& e) const;

	/**
	 * Checks if the edge is not the same
	 */
	bool operator!=(const edge& e) const;
};

/**
 * Each one of the unsigned ints inside the class are in fact
 * pointers to a single vector of edges inside the modelEdge class, of which
 * we can decide the element it relates to.
 */
struct faceEdge {
	// Data inside the face :
	unsigned int northEdge;
	unsigned int leftEdge;
	unsigned int rightEdge;
	Vector normal;

	faceEdge();

	faceEdge(ui n, ui l, ui r);

	void setNormal(const Vector& v);

	void swapEdges(const ui, const ui);
	void swapEdges_INT(const int, const int);

	void alignWith(const ui);
	void alignWith_INT(const int);

	unsigned int getRemainingEdge(const ui, const ui) const;

	bool operator==(const faceEdge& f) const;
};

#endif