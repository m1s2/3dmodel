/**
 * =====================================================================
 * FILE : sphere.h
 * DESC : Contains the declaration of the class sphere, used in some TPs
 * AUTH : Thibault de Villèle
 * DATE : Somewhere near end-Feb. 2019
 * =====================================================================
 */

#ifndef SPHERE_H
#define SPHERE_H

#include "./beziers.h"

class Sphere {
public:
	/**
	 * Creation basique.
	 */
	Sphere();

	/**
	 * Creation de sphère avec rayon et centre defini
	 */
	Sphere(Point, int);

	/**
	 * Creation de sphère avec rayon, centre, parallèles, méridiens.
	 */
	Sphere(Point, int, int, int);

	/**
	 * Calcul des points de la surface de la sphère
	 */
	void calculate();

	/**
	 * Ajoute un parallèle à la sphère. Recalcule aussi les nouveaux points.
	 */
	void addParallel();

	/**
	 * Ajoute un méridien à la sphère. Recalcule aussi les nouveaux points.
	 */
	void addMeridian();

	/**
	 * Enlève un parallèle à la sphère. Recalcule aussi les nouveaux points.
	 */
	void removeParallel();

	/**
	 * Enlève un méridien à la sphère. Recalcule aussi les nouveaux points.
	 */
	void removeMeridian();

	/**
	 * Change le point d'origine de la sphère. Recalcule aussi les nouveaux points.
	 */
	void setOrigin(Point);

	/**
	 * Change le rayon de la sphère. Recalcule aussi les nouveaux points.
	 */
	void setRadius(int);

	/** 
	 * Dessin de sphère
	 */
	void draw();

	/**
	 * Dessin des polygones de surface
	 */
	void drawWireFrame();

	/**
	 * Dessin utilisant glDrawElements();
	 */
	void drawElements() const;

	/**
	 * Dessin du wireframe utilisant glDrawElements();
	 */
	void drawElements_Wireframe() const;

	/**
	 * Gets the low coordinate of the bounding box surrounding the sphere.
	 */
	double getLowerBoundary() const;

	/**
	 * Gets the high coordinate of the bounding box surrounding the sphere.
	 */
	double getHigherBoundary() const;

	/**
	 * Draws all dihedral angles of the model.
	 */
	void drawDihedralAngles(double threshold) const;
	
private:
	Point center;
	Point northPole;
	Point southPole;
	std::vector<std::vector<Point>> surface;
	int nbParallels;
	int nbMeridians;
	int radius;

	/**
	 * Get an array of the vertices composing the sphere. 
	 * Private, since we only need it inside the 
	 * drawElements() and drawElements_Wireframe() functions.
	 */
	double* getTriangleMesh() const;
	/**
	 * Get an array of the normals of the vertices composing the sphere. 
	 * Private, since we only need it inside the drawElements() and
	 * drawElements_Wireframe() functions.
	 */
	double* getNormals() const;
	/**
	 * Get an array of the indices of the vertices composing the sphere. 
	 * Private, since we only need it inside the drawElements() and
	 * drawElements_Wireframe() functions.
	 */
	unsigned int * getFaces() const;

};

#endif