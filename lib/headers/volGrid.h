/**
 * =====================================================================
 * FILE : volGrid.h
 * DESC : Contains the class `volumetricGrid`. This class is used to
 *        simplify a 3d mesh using a cell-based simplification method.
 * AUTH : Thibault de Villèle
 * DATE : March, 31st 2019
 * =====================================================================
 */

#ifndef VOLUMETRICGRID_H
#define VOLUMETRICGRID_H

#include "modelEdge.h"
#include "definitions.h"

struct voxel {
	Point min, max;
	std::vector<unsigned int> neighbors;
	unsigned int currentPoint;

	voxel(const Point& p, const Point& q) : min(p), max(q) {
		currentPoint = std::numeric_limits<unsigned int>::infinity();
	}

	bool hasPoint() const {
		return currentPoint != std::numeric_limits<unsigned int>::infinity();
	}

	bool isInside(const Point& p) const {
		if (p.x < max.x && p.x >= min.x) {
			if (p.y < max.y && p.y >= min.y) {
				if (p.z < max.z && p.z >= max.z) {
					return true;
				}
			}
		}
		return false;
	}
};

class volumetricGrid {
public:
	/**
	 * Coordinates of the lower-left point of 
	 * the model's bounding box in 3D space.
	 */
	Point min;
	/**
	 * Coordinates of the top-right point of 
	 * the model's bounding box in 3D space.
	 */
	Point max;
	/**
	 * Contains all the voxels of the grid, which
	 * are used to re-sample a given model 
	 */
	std::vector<voxel> voxels;

	/**
	 * Indicates how many voxels are in this grid 
	 * height-wise, width-wise and depth-wise.
	 */
	int h, w, d;

	/**
	 * Constructor
	 */
	volumetricGrid(const Point&, const Point&, const int, const int, const int);

	/**
	 * Allows to generate as many voxels as needed 
	 * for the volumetric grid to be filled.
	 */
	void generateVoxels();

	/**
	 * Allows for the model `e` to be re-sampled using
	 * the volumetric grid `v` given in argument.
	 */
	static modelEdge& simplify(const modelEdge& e, volumetricGrid& v);

	/**
	 * returns an appoximation of the voxel a point is in :
	 */
	unsigned int getApproxVoxel(const Point& p) const;
};

#endif