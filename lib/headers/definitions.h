/**
 * =====================================================================
 * FILE : definitions.h
 * DESC : Contains all the definitions of macros used in the OpenGL
 *        practicals.
 * AUTH : Thibault de Villèle
 * DATE : Somewhere near mid-February 2019
 * =====================================================================
 */

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

// Définition de la taille de la fenêtre
#define WIDTH  900
#define HEIGHT 900

// Définition de la couleur de la fenêtre
#define RED   0.03125
#define GREEN 0.42578125
#define BLUE  0.625
#define ALPHA 1

// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27
#define KEY_TAB 9

/**
 * Usual key mappings : [[ can be changed to fit a certain need ]]
 * 
 * Keys ZQSD are used to move around the model.
 * 
 * Keys IJKL are used to :
 *   - add and remove meridians/parallels on a sphere
 * 
 * Keys TFGH are used to move a model in space
 * 
 * Key R is used to change the render mode of a model/solid
 * 
 */
#define KEY_NUMBER_ROW_0 48
#define KEY_NUMBER_ROW_1 49
#define KEY_NUMBER_ROW_2 50
#define KEY_NUMBER_ROW_3 51
#define KEY_NUMBER_ROW_4 52
#define KEY_NUMBER_ROW_5 53
#define KEY_NUMBER_ROW_6 54
#define KEY_NUMBER_ROW_7 55
#define KEY_NUMBER_ROW_8 56
#define KEY_NUMBER_ROW_9 57
#define KEY_A  97
#define KEY_B  98
#define KEY_C  99
#define KEY_D 100
#define KEY_E 101
#define KEY_F 102
#define KEY_G 103
#define KEY_H 104
#define KEY_I 105
#define KEY_J 106
#define KEY_K 107
#define KEY_L 108
#define KEY_M 109
#define KEY_N 110
#define KEY_O 111
#define KEY_P 112
#define KEY_Q 113
#define KEY_R 114
#define KEY_S 115
#define KEY_T 116
#define KEY_U 117
#define KEY_V 118
#define KEY_W 119
#define KEY_X 120
#define KEY_Y 121
#define KEY_Z 122

#define CASTELJAU_CONSTRUCTION_ENABLED true

// Allows for verbose indication of fusion of vertices.
#define FUSION_DEBUG 1

// Allows for some more edges to be detected during simplification :
#define EDGE_DETECT

#endif
