#include <cmath>
#include <stdio.h>
#include <GL/glut.h>
#include <ostream>
#include "../headers/Point.h"
#include "../headers/Vector.h"

class Vector;

Point::Point() {
	x = 0.0;
	y = 0.0;
	z = 0.0;
	normal = Vector(0.0,0.0,0.0);
}

Point::Point(const Point &p) {
	x = p.getX();
	y = p.getY();
	z = p.getZ();
	normal = p.getNormal();
}

Point::Point(double xx, double yy, double zz) {
	x = xx;
	y = yy;
	z = zz;
	normal = Vector(0.0,0.0,0.0);
}

double Point::getX() const {
	return x;
}

double Point::getY() const {
	return y;
}

double Point::getZ() const {
	return z ;
}

void Point::setX(double xx) {
	this->x = xx;
}

void Point::setY(double yy) {
	this->y = yy;
}

void Point::setZ(double zz) {
	this->z = zz;
}

Point Point::ProjectOnLine(Point Point1Line, Point Point2Line) {
	// Point actuel = A
	// Point point1line = debut droite
	// Point point2line = support droite
	Vector ba = Vector(	this->x-Point1Line.getX(),
				this->y-Point1Line.getY(),
				this->z-Point1Line.getZ());
	Vector bc = Vector(	Point2Line.getX()-Point1Line.getX(),
				Point2Line.getY()-Point1Line.getY(),	
				Point2Line.getZ()-Point1Line.getZ());
	bc.normalize();
	double bap= ba.scalar(bc)/bc.norme();
	return Point(Point1Line.getX()+bc.getX()*bap, Point1Line.getY()+bc.getY()*bap, Point1Line.getZ()+bc.getZ()*bap);
}

Point Point::ProjectOnLine(Vector vecteur, Point Pline) {
	Point C = Point(Pline.getX()+vecteur.getX(),
			Pline.getY()+vecteur.getY(),
			Pline.getZ()+vecteur.getZ());
	return ProjectOnLine(Pline, C); 
}

Point Point::ProjectOnPlane(Point planePoint, Vector normalVect) {
	Vector ma = Vector(	this->x-planePoint.getX(), 
				this->y-planePoint.getY(),
				this->z-planePoint.getZ());
	double lengthToPlane = ma.scalar(normalVect)/normalVect.norme();
	return Point(this->x-normalVect.getX()*lengthToPlane, this->y-normalVect.getY()*lengthToPlane, this->z-normalVect.getZ()*lengthToPlane);
}

void Point::multiply(double k) {
	this->x *= k;
	this->y *= k;
	this->z *= k;
}

Point Point::translate(const Vector &v) {
	this->x = this->x + v.getX();
	this->y = this->y + v.getY();
	this->z = this->z + v.getZ();
	return *this;
}

double* Point::toArray() const {
	double* res = new double[3];
	res[0] = x;
	res[1] = y;
	res[2] = z;
	return res;
}

void Point::doGlVertex3f() const {
	glVertex3d(x,y,z);
}

void Point::doGlVertex3f_Alone() const {
	glPointSize(5.0);
	glBegin(GL_POINTS);
	glVertex3d(x,y,z);
	glEnd();
}

Vector Point::getNormal() const {
	return normal;
}

double* Point::getNormalArray() const {
	Vector v(normal);
	return v.normalize().toArray();
}

void Point::setNormal(const Vector& v) {
	this->normal = Vector(v);
}

bool Point::operator==(const Point& p) const {
	return this->x == p.getX() && this->y == p.getY() && this->z == p.getZ();
}

bool Point::operator!=(const Point& p) const {
	return !(*this == p);
}

Point& Point::operator+=(const Point& p) {
	this->x += p.getX();
	this->y += p.getY();
	this->z += p.getZ();
	return *this;
}

Point& Point::operator/ (const double scalar) {
	this->x = this->x / scalar;
	this->y = this->y / scalar;
	this->z = this->z / scalar;
	return *this;
}

Point& Point::operator/=(const double scalar) {
	return (*this / scalar);
}

Vector Point::operator- (const Point& pOrigin) const {
	return Vector(pOrigin, *this);
}

std::ostream& operator<<(std::ostream& out, const Point& p) {
	return out << "Point : (" << p.getX() << "," << p.getY() << "," << p.getZ() << ")";
}

bool Point::operator< (const Point& p) const {
	if (this->x < p.x) {
		if (this->y < p.y) {
			if (this->z < p.z) {
				return true;
			}
		}
	}
	return false;
}

bool Point::operator> (const Point& p) const {
	return p < *this;
}

bool Point::operator<=(const Point& p) const {
	return !(*this > p);
}

bool Point::operator>=(const Point& p) const {
	return !(*this < p);
}

Point& Point::operator+ (const Point& p) const {
	Point n = Point(*this);
	return n += p;
}