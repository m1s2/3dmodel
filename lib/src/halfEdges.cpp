#include "../headers/halfedges.h"
#include "../headers/drawings.h"

#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include <cstdlib>
#include <limits>
#include <algorithm>
#include <iostream>
#include <GL/glut.h>

#define DEBUG

halfEdge::halfEdge() {
	this->endVertex = 0;
	this->faceRelated = 0;
	this->otherEdge = std::numeric_limits<unsigned int>::max();
	this->nextHalfEdge = 0;
}

void halfEdge::setOtherEdge(const unsigned int i) {
	this->otherEdge = i;
}

void halfEdge::setNextEdge(const unsigned int i) {
	this->nextHalfEdge = i;
}

bool halfEdge::operator==(const halfEdge& he) const {
	if (this->endVertex == he.endVertex) {
		if (this->faceRelated == he.faceRelated) {
			if (this->otherEdge == he.otherEdge) {
				if (this->nextHalfEdge == he.nextHalfEdge) {
					return true;
				}
			}
		}
	}
	return false;
}

bool halfEdge::operator!=(const halfEdge& he) const {
	return !(*this==he);
}

modelHalfEdge::modelHalfEdge() {
	this->min_x = std::numeric_limits<float>::infinity();
	this->min_y = std::numeric_limits<float>::infinity();
	this->min_z = std::numeric_limits<float>::infinity();
	this->max_x = std::numeric_limits<float>::infinity();
	this->max_y = std::numeric_limits<float>::infinity();
	this->max_z = std::numeric_limits<float>::infinity();
}

modelHalfEdge::modelHalfEdge(const std::string& pathName) {
	this->readFile(pathName);
}

void modelHalfEdge::readFile(const std::string& pathName) {

	FILE* modelFile = fopen(pathName.c_str(), "r");
	if (modelFile == NULL) {
		printf("Could not instanciate model, error reading file.\n");
		exit(EXIT_FAILURE);
	}
	#ifdef DEBUG
	FILE* errFile = fopen("read_log.txt", "w");
	if (errFile == NULL) {
		fprintf(stderr, "Couldn't open log file. All hell can break \nloose and you won't even know what caused it.\n");
	}
	#endif
	fscanf(modelFile, "OFF ");

	#ifdef DEBUG
	if (errFile != NULL) {
		fprintf(errFile, "\n\n");
	}
	#endif

	int nbPoints, nbTriangles, nbAretes;
	fscanf(modelFile, "%i %i %i", &nbPoints, &nbTriangles, &nbAretes);

	double px,py,pz;
	for (unsigned int i = 0; i < nbPoints; ++i) {
		fscanf(modelFile, "%la %la %la",&px,&py,&pz);
		this->vertices.push_back(Point(px,py,pz));

		// Definition boundaries X
		if (px < this->min_x) {
			this->min_x = px;
		} else if (px > max_x) {
			this->max_x = px;
		}
		// Definition boundaries Y
		if (py < this->min_y) {
			this->min_y = py;
		} else if (py > max_y) {
			this->max_y = py;
		}
		// Definition boundaries Z
		if (pz < this->min_z) {
			this->min_z = pz;
		} else if (pz > max_z) {
			this->max_z = pz;
		}
	}

	std::vector<std::vector<unsigned int>> neighbors;
	std::vector<std::vector<unsigned int>> neighborsEdges;
	neighbors.resize(nbPoints);
	neighborsEdges.resize(nbPoints);

	// read the triangle values :
	unsigned int k,n,l,r;
	unsigned int edgeVal = 0;
	int pos;
	for (unsigned int i = 0; i < nbTriangles; ++i) {
		// yo this is the shit :
		fscanf(modelFile, "%u %u %u %u", &k, &n, &l, &r);

		// Create half edges (orientation defined by end point)

		std::vector<unsigned int>::iterator pos;

		/**
		 * We are gonna try to create the edges :
		 *   - R -> N
		 *   - N -> L
		 *   - L -> R
		 */

		int nextEdgeCheck = -1;

		/**
		 * North edge : From Right to North
		 */
		{
			// here, the edge is not found
			// push new edge onto the neighbor index
			neighbors[r].push_back(n);
			neighborsEdges[r].push_back(this->edges.size());

			#ifdef DEBUG
			if (errFile) {
				fprintf(errFile, "modelHalfEdge::readFile() : Creating edge (%u,%u) at index %u\n", r, n, this->edges.size());
			}
			#endif

			// check if opposite face has been done :
			// if (neighbors[n].size() != 0 && (pos = std::find(neighbors[n].begin(), neighbors[n].end(), r)) != neighbors[n].end()) {
			// 	nextEdgeCheck = abs(std::distance(pos,neighbors[n].begin()));
			// 	#ifdef DEBUG
			// 	if (errFile) {
			// 		fprintf(errFile, "\t\tOther edge has been found, index %i\n", nextEdgeCheck);
			// 	}
			// 	#endif
			// }

			// create the edge and set params
			halfEdge he_north = halfEdge(n, i);		// Create the edge
			he_north.setNextEdge(this->edges.size()+1);	// Set next edge in face
			// if (nextEdgeCheck >= 0) {
			// 	he_north.setOtherEdge(neighborsEdges[n][nextEdgeCheck]);
			// }
			this->edges.push_back(he_north);		// Push it onto the vector

			nextEdgeCheck = -1;

			// Push the face onto the stack, to make the union official
			this->faces.push_back(this->edges.size());
		}
		/**
		 * Left edge : From North to Left
		 */
		{
			// check if edge already exists ! 
			if (neighbors[n].size() != 0 && (pos = std::find(neighbors[n].begin(), neighbors[n].end(), l)) != neighbors[n].end()) {
				#ifdef DEBUG
				if (errFile != NULL) {
					fprintf(errFile, "modelHalfEdge::readFile() : DUPLICATA : Face %u with items (%u,%u,%u) has left edge (%u,%u) already done ! \n", i, n, l, r, n, l);
				}
				#endif
			}

			neighbors[n].push_back(l);
			neighborsEdges[n].push_back(this->edges.size());

			#ifdef DEBUG
			if (errFile) {
				fprintf(errFile, "modelHalfEdge::readFile() : Creating edge (%u,%u) at index %u\n", n, l, this->edges.size());
			}
			#endif
			
			// if (neighbors[l].size() != 0 && (pos = std::find(neighbors[l].begin(), neighbors[l].end(), n)) != neighbors[l].end()) {
			// 	nextEdgeCheck = abs(std::distance(pos,neighbors[l].begin()));
			// 	#ifdef DEBUG
			// 	if (errFile) {
			// 		fprintf(errFile, "\t\t\tOther edge has been found, index %i\n", nextEdgeCheck);
			// 	}
			// 	#endif
			// }

			halfEdge he_left  = halfEdge(l, i);		// Create the edge
			he_left.setNextEdge(this->edges.size()+1);	// Set next edge in face
			// if (nextEdgeCheck >= 0) {
			// 	he_left.setOtherEdge(neighborsEdges[l][nextEdgeCheck]);
			// }
			this->edges.push_back(he_left);			// Push it onto vector

			nextEdgeCheck = -1;
		}
		/**
		 * Right edge : From left to right
		 */
		{
			
			if (neighbors[l].size() != 0 && (pos = std::find(neighbors[l].begin(), neighbors[l].end(), r)) != neighbors[l].end()) {
				#ifdef DEBUG
				if (errFile != NULL) {
					fprintf(errFile, "modelHalfEdge::readFile() : DUPLICATA : Face %u with items (%u,%u,%u) has left edge (%u,%u) already done ! \n", i, n, l, r, l, r);
				}
				#endif
			}

			neighbors[l].push_back(r);
			neighborsEdges[l].push_back(this->edges.size());

			#ifdef DEBUG
			if (errFile) {
				fprintf(errFile, "modelHalfEdge::readFile() : Creating edge (%u,%u) at index %u\n", l, r, this->edges.size());
			}
			#endif
			
			// if (neighbors[r].size() != 0 && (pos = std::find(neighbors[r].begin(), neighbors[r].end(), l)) != neighbors[r].end()) {
			// 	nextEdgeCheck = abs(std::distance(pos,neighbors[r].begin()));
			// 	#ifdef DEBUG
			// 	if (errFile) {
			// 		fprintf(errFile, "\t\t\tOther edge has been found, index %i\n", nextEdgeCheck);
			// 	}
			// 	#endif
			// }

			halfEdge he_right  = halfEdge(r, i);		// Create the edge
			he_right.setNextEdge(this->edges.size()-2);	// Set next edge in face
			// if (nextEdgeCheck >= 0) {
			// 	he_right.setOtherEdge(neighborsEdges[r][nextEdgeCheck]);
			// }
			this->edges.push_back(he_right);			// Push it onto vector

			nextEdgeCheck = -1;
		}

		Vector left = Vector(this->vertices[n], this->vertices[l]);
		Vector right = Vector(this->vertices[n], this->vertices[r]);
		Vector normal = (left.vectoriel(right)).normalize();

		this->vertices[n].setNormal(this->vertices[n].getNormal() + normal);
		this->vertices[l].setNormal(this->vertices[l].getNormal() + normal);
		this->vertices[r].setNormal(this->vertices[r].getNormal() + normal);

		#ifdef DEBUG
		if (errFile) {
			fprintf(errFile,"\n\n");
		}
		#endif
	}

	std::cerr << "FINISHED PARSING FACES" << std::endl;

	// for all edges :
	for (int i = 0; i < neighbors.size(); ++i) {
		// if they have neighbors :
		if (neighbors[i].size() > 0) {
			for (int j = 0; j < neighbors[i].size(); ++j) {
				// for all their neighbors : 
				// get the end edge :
				unsigned int reverseHost = neighbors[i][j];
				std::vector<unsigned int>::iterator pos;
				// if they are also connected the other way :
				// if (neighbors[i].size() != neighborsEdges[i].size()) {
				// 	std::cerr << "COULD SEGFAULT -- ";
				// }
				bool found = false;
				// if ((pos = std::find(neighbors[reverseHost].begin(), neighbors[reverseHost].end(), i)) != neighbors[reverseHost].end()) {
				for (int k = 0; k < neighbors[reverseHost].size(); ++k) {
					if (neighbors[reverseHost][k] == i) {
						unsigned int forwardEdge = neighborsEdges[i][j];
						unsigned int backwardEdge= neighborsEdges[reverseHost][k];
						this->edges[forwardEdge].otherEdge = backwardEdge;
						this->edges[backwardEdge].otherEdge = forwardEdge;
						found = true;
						break;
					}
				}
				if (!found && errFile != NULL) {
					#ifdef DEBUG
					if (errFile) {
						fprintf(errFile, "For edge %u between points %u and %u no edge was found.\n", neighborsEdges[i][j], i, j);
					}
					#endif
				}
			}
		}
	}

	fclose(modelFile);
	if (errFile)
		fclose(errFile);

	for (Point p : this->vertices) {
		p.setNormal(p.getNormal().normalize());
	}

	for (int i = 0; i < this->edges.size(); ++i) {
		if (this->edges[i].nextHalfEdge > this->edges.size()) {
			std::cout << "Edge " << i << " doesn't have next edges." << std::endl;
		}
		if (this->edges[i].otherEdge > this->edges.size()) {
			std::cout << "Edge " << i << " doesn't have opposite edges." << std::endl;
		}
	}

	return;
}

void modelHalfEdge::drawElements() const {
	double* vertexArray = this->getVertexArray();
	double* normalArray = this->getNormalArray();
	unsigned int* faceArray = this->getFaceArray();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertexArray);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// this->printInfo();

	glDrawElements(GL_TRIANGLES, faces.size()*3, GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}



void modelHalfEdge::drawElementsWireframe() const {
	double* vertexArray = this->getVertexArray();
	double* normalArray = this->getNormalArray();
	unsigned int* faceArray = this->getFaceArray();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertexArray);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// this->printInfo();

	glDrawElements(GL_TRIANGLES, faces.size()*3, GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void modelHalfEdge::drawPoints() const {
	double* vertexArray = this->getVertexArray();
	double* normalArray = this->getNormalArray();
	unsigned int* faceArray = new unsigned int[vertices.size()];
	for (unsigned int i = 0; i < vertices.size(); ++i) {
		faceArray[i] = i;
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertexArray);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	// this->printInfo();

	glDrawElements(GL_POINTS, this->vertices.size(), GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

double* modelHalfEdge::getBoundaries() const {
	double* bounds = new double[6];
	bounds[0] = min_x;
	bounds[1] = max_x;
	bounds[2] = min_y;
	bounds[3] = max_y;
	bounds[4] = min_z;
	bounds[5] = max_z;
	return bounds;
}

void modelHalfEdge::printInfo() const {
	printf("Model contains  %u points\n", this->vertices.size());
	printf("\t\t%u edges\n", this->edges.size());
	printf("\t\t%u faces\n", this->faces.size());
	return;
}

double* modelHalfEdge::getVertexArray() const {
	double* vertexArray = new double[3*this->vertices.size()];
	for (int i = 0; i < this->vertices.size(); ++i) {
		double* coords = this->vertices[i].toArray();
		vertexArray[3*i + 0] = coords[0];
		vertexArray[3*i + 1] = coords[1];
		vertexArray[3*i + 2] = coords[2];
	}
	return vertexArray;
}

double* modelHalfEdge::getNormalArray() const {
	double* vertexArray = new double[3*this->vertices.size()];
	for (int i = 0; i < this->vertices.size(); ++i) {
		double* coords = this->vertices[i].getNormal().toArray();
		vertexArray[3*i + 0] = coords[0];
		vertexArray[3*i + 1] = coords[1];
		vertexArray[3*i + 2] = coords[2];
	}
	return vertexArray;
}

unsigned int* modelHalfEdge::getFaceArray() const {
	unsigned int* faceArray = new unsigned int[3*this->faces.size()];
	for (int i = 0; i < this->faces.size(); ++i) {
		faceArray[3*i + 0] = this->edges[this->faces[i]].endVertex;
		faceArray[3*i + 1] = this->edges[this->edges[this->faces[i]].nextHalfEdge].endVertex;
		faceArray[3*i + 2] = this->edges[this->edges[this->edges[this->faces[i]].nextHalfEdge].nextHalfEdge].endVertex;
	}
	return faceArray;
}

void modelHalfEdge::drawBoundingBox() const {
	Point p1 = Point(min_x, min_y, min_z);
	Point p2 = Point(min_x, min_y, max_z);
	Point p3 = Point(min_x, max_y, min_z);
	Point p4 = Point(min_x, max_y, max_z);
	Point p5 = Point(max_x, min_y, min_z);
	Point p6 = Point(max_x, min_y, max_z);
	Point p7 = Point(max_x, max_y, min_z);
	Point p8 = Point(max_x, max_y, max_z);

	// Draw back face
	DrawLine(p2 - p1, p1);
	DrawLine(p3 - p1, p1);
	DrawLine(p4 - p2, p2);
	DrawLine(p4 - p3, p3);

	// Draw front face
	DrawLine(p6 - p5, p5);
	DrawLine(p7 - p5, p5);
	DrawLine(p8 - p6, p6);
	DrawLine(p8 - p7, p7);

	// draw links :
	DrawLine(p5 - p1, p1);
	DrawLine(p6 - p2, p2);
	DrawLine(p7 - p3, p3);
	DrawLine(p8 - p4, p4);
	return;
}

void modelHalfEdge::refine() {
	/**
	 * Start by copying the vertices onto a new vector
	 */

	std::vector<unsigned int> diffPoints;
	diffPoints.resize(this->edges.size());
	std::vector<bool> assigned;
	assigned.resize(this->edges.size());
	for (bool b : assigned) {
		b = false;
	}

	std::vector<std::pair<unsigned int, unsigned int>> diffEdges;
	diffEdges.resize(this->edges.size());
	std::vector<bool> edgeSplit;
	edgeSplit.resize(this->edges.size());
	for (bool b : edgeSplit) {
		b = false;
	}

	unsigned int maxSize = this->faces.size();

	for (unsigned int i = 0; i < maxSize; ++i) {
		/**
		 * For each face, we will double each edge, push it onto
		 */
		unsigned int northEdge = this->faces[i];
		unsigned int leftEdge = this->edges[northEdge].nextHalfEdge;
		unsigned int rightEdge= this->edges[leftEdge].nextHalfEdge;

		/**
		 * Reminders : 
		 * North edge goes from R to N
		 * Left  edge goes from N to L
		 * Right edge goes from L to R
		 */

		unsigned int neEnd = this->edges[northEdge].endVertex;
		unsigned int leEnd = this->edges[leftEdge ].endVertex;
		unsigned int reEnd = this->edges[rightEdge].endVertex;

		unsigned int neMiddle;	// middle point of North Edge
		unsigned int leMiddle;	// middle point of left edge
		unsigned int reMiddle;	// middle point of right edge

		/**
		 * If the edges have been split previously, we just
		 * connect them to the points they need to connect.
		 */
		#pragma region Point creation
		if (assigned[northEdge]) {
			neMiddle = diffPoints[northEdge];
		} else {
			Vector v = this->vertices[neEnd] - this->vertices[reEnd];
			Point nm = Point(this->vertices[reEnd]);
			nm.translate( (v * 0.5) );
			nm.setNormal((this->vertices[neEnd].getNormal() + this->vertices[reEnd].getNormal()).normalize());
			neMiddle = this->vertices.size();
			diffPoints[northEdge] = this->vertices.size();
			assigned[northEdge] = true;
			this->vertices.push_back(nm);
		}
		if (assigned[leftEdge]) {
			leMiddle = diffPoints[leftEdge];
		} else {
			Vector v = this->vertices[leEnd] - this->vertices[neEnd];
			Point lm = Point(this->vertices[neEnd]);
			lm.translate( (v * 0.5) );
			lm.setNormal((this->vertices[leEnd].getNormal() + this->vertices[neEnd].getNormal()).normalize());
			leMiddle = this->vertices.size();
			diffPoints[leftEdge] = this->vertices.size();
			assigned[leftEdge] = true;
			this->vertices.push_back(lm);
		}
		if (assigned[rightEdge]) {
			reMiddle = diffPoints[rightEdge];
		} else {
			Vector v = this->vertices[reEnd] - this->vertices[leEnd];
			Point rm = Point(this->vertices[leEnd]);
			rm.translate( (v * 0.5) );
			rm.setNormal((this->vertices[reEnd].getNormal() + this->vertices[leEnd].getNormal()).normalize());
			reMiddle = this->vertices.size();
			diffPoints[rightEdge] = this->vertices.size();
			assigned[rightEdge] = true;
			this->vertices.push_back(rm);
		}
		#pragma endregion

		/**
		 * We now have all the points we need. 
		 * Let's build the top face.
		 */
		{
			// We don't touch the existing first edge's end point, just its next edge.
			this->edges[northEdge].setNextEdge(this->edges.size());
			// new edge
			halfEdge northLeft  = halfEdge(leMiddle, this->edges[northEdge].faceRelated);
			northLeft.setNextEdge(this->edges.size()+1);
			// new edge
			halfEdge northRight = halfEdge(neMiddle, this->edges[northEdge].faceRelated);
			northRight.setNextEdge(northEdge);
			if (edgeSplit[northEdge]) {
				auto oldedges = diffEdges[northEdge];
				northLeft.setOtherEdge(oldedges.second);
				northRight.setOtherEdge(oldedges.first);
				this->edges[oldedges.second].setOtherEdge(this->edges.size());
				this->edges[oldedges.first].setOtherEdge(this->edges.size()+1);
			} else {
				diffEdges[northEdge] = std::make_pair(this->edges.size(), this->edges.size()+1);
				edgeSplit[northEdge] = true;
			}
			// push edge changes
			this->edges.push_back(northLeft);
			this->edges.push_back(northRight);
		}
		/**
		 * Let's build the left face
		 */
		{
			// We don't touch the existing edge's end point, just its successor
			this->edges[leftEdge].setNextEdge(this->edges.size());
			// new edge
			halfEdge leftNorth  = halfEdge(leMiddle, this->faces.size()+1);
			leftNorth.setNextEdge(leftEdge);
			// new edge
			halfEdge leftRight  = halfEdge(reMiddle, this->faces.size()+1);
			leftRight.setNextEdge(this->edges.size()+1);
			if (edgeSplit[leftEdge]) {
				auto oldedges = diffEdges[northEdge];
				leftRight.setOtherEdge(oldedges.second);
				leftNorth.setOtherEdge(oldedges.first);
				this->edges[oldedges.second].setOtherEdge(this->edges.size());
				this->edges[oldedges.first].setOtherEdge(this->edges.size()+1);
			} else {
				diffEdges[leftEdge] = std::make_pair(this->edges.size(), this->edges.size()+1);
				edgeSplit[leftEdge] = true;
			}
			// add new face
			this->faces.push_back(leftEdge);
			// push edge changes
			this->edges.push_back(leftRight);
			this->edges.push_back(leftNorth);
		}
		/**
		 * Let's build the right face
		 */
		{
			// We don't touch the existing edge's end point, just its successor
			this->edges[rightEdge].nextHalfEdge = this->edges.size();
			// new edge
			halfEdge rightNorth = halfEdge(neMiddle, this->faces.size()+1);
			rightNorth.setNextEdge(this->edges.size()+1);
			// new edge
			halfEdge rightLeft  = halfEdge(reMiddle, this->faces.size()+1);
			rightLeft.setNextEdge(rightEdge);
			if (edgeSplit[rightEdge]) {
				auto oldedges = diffEdges[northEdge];
				rightNorth.setOtherEdge(oldedges.second);
				rightLeft.setOtherEdge(oldedges.first);
				this->edges[oldedges.second].setOtherEdge(this->edges.size());
				this->edges[oldedges.first].setOtherEdge(this->edges.size()+1);
			} else {
				diffEdges[rightEdge] = std::make_pair(this->edges.size(), this->edges.size()+1);
				edgeSplit[rightEdge] = true;
			}
			// add new face
			this->faces.push_back(rightEdge);
			// push edge changes
			this->edges.push_back(rightNorth);
			this->edges.push_back(rightLeft);
		}
		/**
		 * Let's build the middle face
		 */
		{
			// new edge
			halfEdge middleNorth = halfEdge(neMiddle, this->faces.size()+1);
			middleNorth.setNextEdge(this->edges.size()+1);
			middleNorth.setOtherEdge(this->edges.size()-1);
			// new edge
			halfEdge middleLeft  = halfEdge(leMiddle, this->faces.size()+1);
			middleLeft.setNextEdge(this->edges.size()+2);
			middleNorth.setOtherEdge(this->edges.size()-5);
			// new edge
			halfEdge middleRight = halfEdge(reMiddle, this->faces.size()+1);
			middleRight.setNextEdge(this->edges.size());
			middleNorth.setOtherEdge(this->edges.size()-3);
			// add face
			this->faces.push_back(this->edges.size());
			// push edge changes
			this->edges.push_back(middleNorth);
			this->edges.push_back(middleLeft);
			this->edges.push_back(middleRight);
		}
	}
}

void modelHalfEdge::subdiviseButterfly() {

	std::vector<unsigned int> diffPoints;
	diffPoints.resize(this->edges.size());
	std::vector<bool> assigned;
	assigned.resize(this->edges.size());
	for (bool b : assigned) {
		b = false;
	}

	std::vector<std::pair<unsigned int, unsigned int>> diffEdges;
	diffEdges.resize(this->edges.size());
	std::vector<bool> edgeSplit;
	edgeSplit.resize(this->edges.size());
	for (bool b : edgeSplit) {
		b = false;
	}

	unsigned int maxSize = this->faces.size();

	for (unsigned int i = 0; i < this->faces.size(); ++i) {
		/**
		 * For each face, we will double each edge, push it onto
		 */
		unsigned int northEdge = this->faces[i];
		unsigned int leftEdge = this->edges[northEdge].nextHalfEdge;
		unsigned int rightEdge= this->edges[leftEdge].nextHalfEdge;

		unsigned int neMiddle;	// middle point of North Edge
		unsigned int leMiddle;	// middle point of left edge
		unsigned int reMiddle;	// middle point of right edge

		/**
		 * If the edges have been split previously, we just
		 * connect them to the points they need to connect.
		 */
		#pragma region Point creation
		if (assigned[northEdge]) {
			neMiddle = diffPoints[northEdge];
		} else {
			Point nm = this->computeButterflyPosition(northEdge);
			neMiddle = this->vertices.size();
			diffPoints[northEdge] = this->vertices.size();
			assigned[northEdge] = true;
			this->vertices.push_back(nm);
		}
		if (assigned[leftEdge]) {
			leMiddle = diffPoints[leftEdge];
		} else {
			Point lm = this->computeButterflyPosition(leftEdge);
			leMiddle = this->vertices.size();
			diffPoints[leftEdge] = this->vertices.size();
			assigned[leftEdge] = true;
			this->vertices.push_back(lm);
		}
		if (assigned[rightEdge]) {
			reMiddle = diffPoints[rightEdge];
		} else {
			Point rm = this->computeButterflyPosition(rightEdge);
			reMiddle = this->vertices.size();
			diffPoints[rightEdge] = this->vertices.size();
			assigned[rightEdge] = true;
			this->vertices.push_back(rm);
		}
		#pragma endregion
	}

	for (unsigned int i = 0; i < maxSize; ++i) {
		/**
		 * For each face, we will double each edge, push it onto
		 */
		unsigned int northEdge = this->faces[i];
		unsigned int leftEdge = this->edges[northEdge].nextHalfEdge;
		unsigned int rightEdge= this->edges[leftEdge].nextHalfEdge;

		/**
		 * Reminders : 
		 * North edge goes from R to N
		 * Left  edge goes from N to L
		 * Right edge goes from L to R
		 */

		unsigned int neEnd = this->edges[northEdge].endVertex;
		unsigned int leEnd = this->edges[leftEdge ].endVertex;
		unsigned int reEnd = this->edges[rightEdge].endVertex;

		unsigned int neMiddle = diffPoints[northEdge];	// middle point of North Edge
		unsigned int leMiddle = diffPoints[leftEdge];	// middle point of left edge
		unsigned int reMiddle = diffPoints[rightEdge];	// middle point of right edge

		halfEdge northLeft, northRight, leftRight, leftNorth, rightNorth, rightLeft;
		halfEdge middleNorth, middleLeft, middleRight;

		/**
		 * We now have all the points we need. 
		 * Let's build the top face.
		 */
			// We don't touch the existing first edge's end point, just its next edge.
			this->edges[northEdge].setNextEdge(this->edges.size());
			// new edge
			northLeft  = halfEdge(leMiddle, this->edges[northEdge].faceRelated);
			northLeft.setNextEdge(this->edges.size()+1);
			// new edge
			northRight = halfEdge(neMiddle, this->edges[northEdge].faceRelated);
			northRight.setNextEdge(northEdge);
		/**
		 * Let's build the left face
		 */
			// We don't touch the existing edge's end point, just its successor
			this->edges[leftEdge].setNextEdge(this->edges.size()+3);
			// new edge
			leftNorth  = halfEdge(leMiddle, this->faces.size()+1);
			leftNorth.setNextEdge(leftEdge);
			// new edge
			leftRight  = halfEdge(reMiddle, this->faces.size()+1);
			leftRight.setNextEdge(this->edges.size()+2);
			// add new face
			this->faces.push_back(leftEdge);
			// We'll insert leftNorth AND THEN leftRight
		/**
		 * Let's build the right face
		 */
			// We don't touch the existing edge's end point, just its successor
			this->edges[rightEdge].nextHalfEdge = this->edges.size()+5;
			// new edge
			rightNorth = halfEdge(neMiddle, this->faces.size()+1);
			rightNorth.setNextEdge(this->edges.size()+4);
			// new edge
			rightLeft  = halfEdge(reMiddle, this->faces.size()+1);
			rightLeft.setNextEdge(rightEdge);
			// add new face
			this->faces.push_back(rightEdge);
		/**
		 * Let's connect the faces !
		 */
			if (edgeSplit[northEdge]) {
				auto oldedges = diffEdges[northEdge];
				this->edges[northEdge].setOtherEdge(oldedges.second);
				rightNorth.setOtherEdge(oldedges.first);
				this->edges[oldedges.second].setOtherEdge(northEdge);
				this->edges[oldedges.first].setOtherEdge(this->edges.size()+4);
			} else {
				diffEdges[northEdge] = std::make_pair(northEdge, this->edges.size()+4);
				edgeSplit[northEdge] = true;
			}
			if (edgeSplit[leftEdge]) {
				auto oldedges = diffEdges[northEdge];
				this->edges[leftEdge].setOtherEdge(oldedges.second);
				northLeft.setOtherEdge(oldedges.first);
				this->edges[oldedges.second].setOtherEdge(leftEdge);
				this->edges[oldedges.first].setOtherEdge(this->edges.size());
			} else {
				diffEdges[leftEdge] = std::make_pair(leftEdge, this->edges.size());
				edgeSplit[leftEdge] = true;
			}
			if (edgeSplit[rightEdge]) {
				auto oldedges = diffEdges[northEdge];
				this->edges[rightEdge].setOtherEdge(oldedges.second);
				leftRight.setOtherEdge(oldedges.first);
				this->edges[oldedges.second].setOtherEdge(rightEdge);
				this->edges[oldedges.first].setOtherEdge(this->edges.size()+3);
			} else {
				diffEdges[rightEdge] = std::make_pair(rightEdge, this->edges.size()+3);
				edgeSplit[rightEdge] = true;
				}
			// push edge changes
			this->edges.push_back(northLeft);
			this->edges.push_back(northRight);
			// push edge changes
			this->edges.push_back(leftNorth);
			this->edges.push_back(leftRight);
			// push edge changes
			this->edges.push_back(rightNorth);
			this->edges.push_back(rightLeft);
		/**
		 * Let's build the middle face
		 */
			// new edge
			middleNorth = halfEdge(neMiddle, this->faces.size()+1);
			middleNorth.setNextEdge(this->edges.size()+1);
			middleNorth.setOtherEdge(this->edges.size()-1);
			// new edge
			middleLeft  = halfEdge(leMiddle, this->faces.size()+1);
			middleLeft.setNextEdge(this->edges.size()+2);
			middleNorth.setOtherEdge(this->edges.size()-5);
			// new edge
			middleRight = halfEdge(reMiddle, this->faces.size()+1);
			middleRight.setNextEdge(this->edges.size());
			middleNorth.setOtherEdge(this->edges.size()-4);
			// add face
			this->faces.push_back(this->edges.size());
			// push edge changes
			this->edges.push_back(middleNorth);
			this->edges.push_back(middleLeft);
			this->edges.push_back(middleRight);
	}
}

Point& modelHalfEdge::computeButterflyPosition(const unsigned int e) {
	// left neighbor
	unsigned int lnI = this->edges[e].endVertex;
	// top neighbor (north)
	unsigned int tnI = this->edges[this->edges[e].nextHalfEdge].endVertex;
	// bottom neighbor (south)
	unsigned int bnI = this->edges[this->edges[this->edges[e].otherEdge].nextHalfEdge].endVertex;
	// neighbor on top left
	unsigned int tlnI = this->edges[this->edges[this->edges[this->edges[e].nextHalfEdge].otherEdge].nextHalfEdge].endVertex;
	// neighbor on  top right
	unsigned int trnI = this->edges[this->edges[this->edges[this->edges[this->edges[e].nextHalfEdge].nextHalfEdge].otherEdge].nextHalfEdge].endVertex;
	// neighbor on lower right 
	unsigned int lrnI = this->edges[this->edges[this->edges[this->edges[this->edges[e].otherEdge].nextHalfEdge].otherEdge].nextHalfEdge].endVertex;
	// neighbor on lower left
	unsigned int llnI = this->edges[this->edges[this->edges[this->edges[this->edges[this->edges[e].otherEdge].nextHalfEdge].nextHalfEdge].otherEdge].nextHalfEdge].endVertex;
	// right neighbor
	unsigned int rnI = this->edges[this->edges[this->edges[e].nextHalfEdge].nextHalfEdge].endVertex;

	Point lN = Point(this->vertices[lnI]);
	Point rN = Point(this->vertices[rnI]);
	Point nN = Point(this->vertices[tnI]);
	Point sN = Point(this->vertices[bnI]);
	Point tLN = Point(this->vertices[tlnI]);
	Point tRN = Point(this->vertices[trnI]);
	Point bLN = Point(this->vertices[llnI]);
	Point bRN = Point(this->vertices[lrnI]);

	Vector lN_n = lN.getNormal();
	Vector rN_n = rN.getNormal();
	Vector sN_n = sN.getNormal();
	Vector nN_n = nN.getNormal();

	double x = 0.5 * lN.x + 0.5 * rN.x + 0.125 * nN.x + 0.125 * sN.x - 0.0625 * tLN.x - 0.0625 * tRN.x - 0.0625 * bLN.x - 0.0625 * bRN.x;
	double y = 0.5 * lN.y + 0.5 * rN.y + 0.125 * nN.y + 0.125 * sN.y - 0.0625 * tLN.y - 0.0625 * tRN.y - 0.0625 * bLN.y - 0.0625 * bRN.y;
	double z = 0.5 * lN.z + 0.5 * rN.z + 0.125 * nN.z + 0.125 * sN.z - 0.0625 * tLN.z - 0.0625 * tRN.z - 0.0625 * bLN.z - 0.0625 * bRN.z;

	Vector normal = (lN_n + rN_n + sN_n + nN_n).normalize();

	Point* butterfly = new Point(x,y,z);
	butterfly->setNormal(normal);

	return *butterfly;
}