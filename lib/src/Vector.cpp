#include <cmath>
#include <tgmath.h>
#include <ostream>
#include "../headers/Vector.h"
#include "../headers/Point.h"

class Point;

Vector::Vector() {
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

Vector::Vector(const Vector &v) {
	x = v.getX();
	y = v.getY();
	z = v.getZ();
}

Vector::Vector(const Point &p, const Point& p2) {
	x = p2.getX() - p.getX();
	y = p2.getY() - p.getY();
	z = p2.getZ() - p.getZ();
}

Vector::Vector(double xx, double yy, double zz) {
	x = xx;
	y = yy;
	z = zz;
}

double Vector::getX() const{
	return x;
}

double Vector::getY() const{
	return y;
}

double Vector::getZ() const{
	return z;
}

double Vector::getAlpha() const {
	return atan(this->y / this->x);
}

double Vector::getAlphaDeg() const {
	return ( atan(this->y / this->x) * 180.0 ) / M_PI;
}

double Vector::getTheta() const {
	return atan(this->z / this->x);
}

double Vector::getThetaDeg() const {
	return ( atan(this->z / this->x) * 180.0 ) / M_PI;
}

void Vector::setX(double xx) {
	this->x = xx;
}

void Vector::setY(double yy) {
	this->y = yy;
}

void Vector::setZ(double zz) {
	this->z = zz;
}

double Vector::norme() const {
	return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
}

Vector& Vector::normalize() {
	double n = this->norme();
	this->x = this->x/n;
	this->y = this->y/n;
	this->z = this->z/n;
	return *this;
}

double Vector::scalar(Vector v2) {
	return x*v2.getX() + y*v2.getY() + z*v2.getZ();
}

Vector  Vector::vectoriel(Vector v2) {
	double rx = this->getY()*v2.getZ() - this->getZ()*v2.getY();
	double ry = this->getZ()*v2.getX() - this->getX()*v2.getZ();
	double rz = this->getX()*v2.getY() - this->getY()*v2.getX();
	return Vector(rx, ry, rz);
}

void Vector::scale(double n) {
	this->x = this->x * n;
	this->y = this->y * n;
	this->z = this->z * n;
}

double Vector::angle(Vector v2) {
	return abs(scalar(v2)/(this->norme()*v2.norme())) * 180.0 / M_PI;
}

double* Vector::toArray() {
	double* res = (double*) malloc(3*sizeof(double));
	res[0] = x;
	res[1] = y;
	res[2] = z;
	return res;
}

void Vector::applyToPoint(double px, double py, double pz) {
	this->x = this->x+px;
	this->y = this->y+py;
	this->z = this->z+pz;
}

Vector& Vector::operator* (double scalar) {
	scale(scalar);
	return *this;
}

Vector& Vector::operator- () {
	x = -x;
	y = -y;
	z = -z;
	return *this;
}

Vector Vector::operator+ (const Vector& v) const {
	return Vector(this->x + v.getX(), this->y + v.getY(), this->z + v.getZ());
}

std::ostream& operator<<(std::ostream& out, const Vector& v) {
	return out << "Vector : (" << v.getX() << "," << v.getY() << "," << v.getZ() << ")";
}

Vector::operator Point *() const {
	return new Point(x,y,z);
}

Vector::operator Point() const {
	return Point(x,y,z);
}
