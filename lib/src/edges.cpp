#include "../headers/edges.h"

#include <iostream>

edge::edge(const ui start, const ui end, const ui f_face) {
	this->startPoint = start;
	this->endPoint = end;
	this->firstFace = f_face;
	this->secondFace = std::numeric_limits<unsigned int>::infinity();
}

void edge::setSecondFace(const ui secFace) {
	this->secondFace = secFace;
}

bool edge::hasSecondFace() const {
	return this->secondFace != std::numeric_limits<unsigned int>::infinity();
}

ui edge::hasFace(const ui f) const {
	if (this->firstFace == f) return 1;
	if (this->secondFace == f) return 2;
	return 0;
}

unsigned int edge::getOtherFace(const ui f) const {
	if (this->firstFace == f) return this->secondFace;
	return this->firstFace;
}

bool edge::operator==(const edge& e) const {
	// edges are the same whether they're from a to b or b to a
	return  (this->startPoint == e.startPoint && this->endPoint == e.endPoint) || 
		(this->startPoint == e.endPoint && this->endPoint == e.startPoint);
}

bool edge::operator!=(const edge& e) const {
	return !(*this == e);
}

faceEdge::faceEdge() {
	northEdge = 0;
	leftEdge = 0;
	rightEdge = 0;
	normal = Vector(0.0,0.0,0.0);
}

faceEdge::faceEdge(ui n, ui l, ui r) {
	northEdge = n;
	leftEdge = l;
	rightEdge = r;
	normal = Vector(0.0,0.0,0.0);
}

void faceEdge::setNormal(const Vector& v) {
	this->normal = Vector(v);
}

void faceEdge::swapEdges(const ui _old, const ui _new) {
	if (this->leftEdge == _old) {
		#ifdef FUSION_DEBUG
		std::cout << "L - Old edges : " << this->leftEdge << "," << this->rightEdge << "," << this->northEdge << "...";
		#endif
		this->leftEdge = _new;
		#ifdef FUSION_DEBUG
		std::cout << " now are : " << this->leftEdge << "," << this->rightEdge << "," << this->northEdge << std::endl;
		#endif
	}
	if (this->rightEdge == _old) {
		#ifdef FUSION_DEBUG
		std::cout << "R - Old edges : " << this->leftEdge << "," << this->rightEdge << "," << this->northEdge << "...";
		#endif
		this->rightEdge = _new;
		#ifdef FUSION_DEBUG
		std::cout << " now are : " << this->leftEdge << "," << this->rightEdge << "," << this->northEdge << std::endl;
		#endif
	}
	if (this->northEdge == _old) {
		#ifdef FUSION_DEBUG
		std::cout << "N - Old edges : " << this->leftEdge << "," << this->rightEdge << "," << this->northEdge << "...";
		#endif
		this->northEdge = _new;
		#ifdef FUSION_DEBUG
		std::cout << " now are : " << this->leftEdge << "," << this->rightEdge << "," << this->northEdge << std::endl;
		#endif
	}
	return;
}

void faceEdge::alignWith(const ui index) {
	if (this->leftEdge >= index) {
		this->leftEdge--;
	}
	if (this->rightEdge >= index) {
		this->rightEdge--;
	}
	if (this->northEdge >= index) {
		this->northEdge--;
	}
	return;
}

void faceEdge::alignWith_INT(const int i) {
	const ui index = (const ui) i;
	this->alignWith(index);
}

void faceEdge::swapEdges_INT(const int _o, const int _n) {
	const ui _old = (const ui) _o;
	const ui _new = (const ui) _n;
	this->swapEdges(_old, _new);
}

unsigned int faceEdge::getRemainingEdge(const ui e1, const ui e2) const {
	if (northEdge != e1 && northEdge != e2) return northEdge;
	if (leftEdge != e1 && leftEdge != e2) return leftEdge;
	if (rightEdge != e1 && rightEdge != e2) return rightEdge;
}

bool faceEdge::operator==(const faceEdge& f) const {
	// if perfectly equal
	if (this->northEdge == f.northEdge && this->leftEdge == f.leftEdge && this->rightEdge == f.rightEdge) {return true;}
	// if left-north inverted
	if (this->northEdge == f.leftEdge && this->leftEdge == f.northEdge && this->rightEdge == f.rightEdge) {return true;}
	// if right-north inverted
	if (this->northEdge == f.rightEdge && this->leftEdge == f.leftEdge && this->rightEdge == f.northEdge) {return true;}
	// if left-right inverted
	if (this->northEdge == f.northEdge && this->leftEdge == f.rightEdge && this->rightEdge == f.leftEdge) {return true;}
	// if all shifted one up (two down)
	if (this->northEdge == f.leftEdge && this->leftEdge == f.rightEdge && this->rightEdge == f.northEdge) {return true;}
	// if all shifted two up (one down)
	if (this->northEdge == f.rightEdge && this->leftEdge == f.northEdge && this->rightEdge == f.leftEdge) {return true;}
	// if not :
	return false;
}
