#include <stdio.h>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include <GL/glut.h>

#include "../headers/face.h"

Face::Face(int n, int l, int r) {
	this->north = (n);
	this->left = (l);
	this->right = (r);
}

unsigned int* Face::getArray() const {
	unsigned int * indices = new unsigned int[3];
	indices[0] = left;
	indices[1] = north;
	indices[2] = right;
	return indices;
}

meshFace Face::toMeshFace(const std::vector<Point> src) const {
	return meshFace(*this, src);
}

meshFace Face::toMeshFace(const double* src) const {
	return meshFace(*this, src);
}

meshFace::meshFace(const Face& f, const std::vector<Point> source) {
	pnorth = source.at(f.getNorth());
	pleft = source.at(f.getLeft());
	pright = source.at(f.getRight());

	normal = f.getNormal().normalize();
}

meshFace::meshFace(const Face& f, const double* source) {
	unsigned int i = f.getNorth();
	pnorth = Point(source[3*i+0], source[3*i+1], source[3*i+2]);
	i = f.getLeft();
	pleft = Point(source[3*i+0], source[3*i+1], source[3*i+2]);
	i = f.getRight();
	pright = Point(source[3*i+0], source[3*i+1], source[3*i+2]);
	

	normal = Vector(pnorth, pleft).vectoriel(Vector(pnorth, pright)).normalize();
}

double meshFace::getDihedralAngle(const meshFace& mf) const {
	// check if they are not the same :
	bool northSame = this->pnorth == mf.pnorth || this->pnorth == mf.pleft || this->pnorth == mf.pright;
	bool leftSame = this->pleft == mf.pnorth || this->pleft == mf.pleft || this->pleft == mf.pright;
	bool rightSame = this->pright == mf.pnorth || this->pright == mf.pleft || this->pright == mf.pright;

	bool allSame = northSame && leftSame && rightSame;

	if (allSame) {
		return NAN;
	} else if (northSame && leftSame || northSame && rightSame || leftSame && rightSame){
		Vector currentNorm = -Vector(normal);
		Vector otherNorm = -Vector(mf.normal);

		// both vectors sould be of norm 1.0 (since normalized at creation)
		double currentAngle = std::acos(currentNorm.scalar(otherNorm));

		if (isnan(currentAngle)) {
			Vector current2 = -Vector(mf.normal);
			Vector other2 = -Vector(this->normal);

			double currentAngle = std::acos(current2.scalar(other2));
			// std::cout << "V : " << current2 << " // " << other2 << " // Angle calculated is : " << currentAngle << std::endl;
			return currentAngle;
		}

		// std::cout << "V : " << currentNorm << " // " << otherNorm << " // Angle calculated is : " << currentAngle << std::endl;
		return ( currentAngle * 180 ) / M_PI;
	} else {
		return NAN;
	}
}

std::vector<Point> meshFace::getCommonPoints(const meshFace& mf) const {
	std::vector<Point> commonKnowledge;

	if (this->pnorth == mf.pnorth || this->pnorth == mf.pleft || this->pnorth == mf.pright) {
		commonKnowledge.push_back(this->pnorth);
	}
	if (this->pleft == mf.pnorth || this->pleft == mf.pleft || this->pleft == mf.pright) {
		commonKnowledge.push_back(this->pleft);
	}
	if (this->pright == mf.pnorth || this->pright == mf.pleft || this->pright == mf.pright) {
		commonKnowledge.push_back(this->pright);
	}

	return commonKnowledge;
}
