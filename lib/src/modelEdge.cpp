#include "../headers/modelEdge.h"

#include <cfloat>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <unordered_map>
#include <utility>

/**
 * Function for checking if an edge is already inserted into
 * a vertex's neighboring edges.
 */
int isAlreadyHere(const std::vector<edge>&, const edge&);

#define ERR(x, i) if (x != NULL) { fprintf(x, "Face %i already had a second face. ABORT\n", i); }
#define ERR2(x, i) if (x != NULL) { fprintf(x, "Edges for face %i don't match up. ABORT\n", i); }
#define ERR3(x, i) if (x != NULL) { fprintf(x, "Some edges for face %i are same. ABORT\n", i); }

modelEdge::modelEdge() {
	min_x = FLT_MAX;
	min_y = FLT_MAX;
	min_z = FLT_MAX;
	max_x = FLT_MIN;
	max_y = FLT_MIN;
	max_z = FLT_MIN;
}

modelEdge::modelEdge(const char* pathname) {
	min_x = DBL_MAX;
	min_y = DBL_MAX;
	min_z = DBL_MAX;
	max_x = DBL_MIN;
	max_y = DBL_MIN;
	max_z = DBL_MIN;
	this->readFile(pathname);
}

void modelEdge::readFile(const char* pathToModel) {
	FILE* modelFile = fopen(pathToModel, "r");
	if (modelFile == NULL) {
		printf("Could not read model. Model not instanciated.\n");
		exit(1);
	}

	FILE* errFile = fopen("log.txt", "w");
	if (modelFile == NULL) {
		fprintf(stderr, "couldn't open log file. incidents will not be reported\n");
	}

	// Verifier que le fichier est "OFF"
	fscanf(modelFile, "OFF ");

	unsigned int nbPoints, nbTriangles, nbAretes;
	fscanf(modelFile, "%i %i %i", &nbPoints, &nbTriangles, &nbAretes);

	double px,py,pz;
	for (unsigned int i = 0; i < nbPoints; ++i) {
		fscanf(modelFile, "%la %la %la",&px,&py,&pz);
		vertices.push_back(Point(px,py,pz));
		
		// Definition boundaries X
		if (px < min_x) {
			min_x = px;
		} else if (px > max_x) {
			max_x = px;
		}
		// Definition boundaries Y
		if (py < min_y) {
			min_y = py;
		} else if (py > max_y) {
			max_y = py;
		}
		// Definition boundaries Z
		if (pz < min_z) {
			min_z = pz;
		} else if (pz > max_z) {
			max_z = pz;
		}
	}

	// read the triangle values :
	unsigned int k,n,l,r;
	unsigned int edgeVal = 0;
	int pos;
	for (unsigned int i = 0; i < nbTriangles; ++i) {
		// yo this is the shit :
		fscanf(modelFile, "%u %u %u %u", &k, &n, &l, &r);

		// Create face for edge storage :
		faceEdge f;

		// Create the first edge :
		edge north(n, l, i);
		if ((pos = isAlreadyHere(edges, north)) != -1) {
			if (edges[pos].hasSecondFace()) ERR(errFile, i);
			edges[pos].setSecondFace(i);
			f.northEdge = pos;
		} else {
			edges.push_back(north);
			f.northEdge = edges.size()-1;
		}

		// Create the second edge :
		edge left(l, r, i);
		if ((pos = isAlreadyHere(edges, left)) != -1) {
			if (edges[pos].hasSecondFace()) ERR(errFile, i);
			edges[pos].setSecondFace(i);
			f.leftEdge = pos;
		} else {
			edges.push_back(left);
			f.leftEdge = edges.size()-1;
		}

		// Create the third/last edge :
		edge right(r, n, i);
		if ((pos = isAlreadyHere(edges, right)) != -1) {
			if (edges[pos].hasSecondFace()) ERR(errFile, i);
			edges[pos].setSecondFace(i);
			f.rightEdge = pos;
		} else {
			edges.push_back(right);
			f.rightEdge = edges.size()-1;
		}

		Vector fNormal = Vector(vertices[n], vertices[l]).vectoriel(Vector(vertices[n], vertices[r]));
		vertices[n].setNormal(vertices[n].getNormal() + fNormal);
		vertices[r].setNormal(vertices[r].getNormal() + fNormal);
		vertices[l].setNormal(vertices[l].getNormal() + fNormal);

		f.setNormal(fNormal);
		faces.push_back(f);
	}

	for (unsigned int i = 0; i < nbPoints; ++i) {
		vertices[i].setNormal(vertices[i].getNormal().normalize());
	}

	fclose(errFile);
	fclose(modelFile);

	return;
}

double* modelEdge::getVertexArray() const {
	double* v = new double[vertices.size() * 3];
	for (unsigned int i = 0; i < vertices.size(); ++i) {
		double* p = vertices[i].toArray();
		v[3*i + 0] = p[0];
		v[3*i + 1] = p[1];
		v[3*i + 2] = p[2];
	}
	return v;
}

double* modelEdge::getNormalArray() const {
	double* n = new double[vertices.size() * 3];
	for (unsigned int i = 0; i < vertices.size(); ++i) {
		double* p = vertices[i].getNormal().toArray();
		n[3*i + 0] = p[0];
		n[3*i + 1] = p[1];
		n[3*i + 2] = p[2];
	}
	return n;
}

unsigned int* modelEdge::getFaceArray() const {
	FILE* errFile = fopen("log2.txt", "w");
	unsigned int a,b,c,d,e,g,p1,p2,p3;
	unsigned int* f = new unsigned int[faces.size()*3];
	for (unsigned int i = 0; i < faces.size(); ++i) {
		a = edges[faces[i].northEdge].startPoint;
		b = edges[faces[i].northEdge].endPoint;
		c = edges[faces[i].leftEdge].startPoint;
		d = edges[faces[i].leftEdge].endPoint;
		e = edges[faces[i].rightEdge].startPoint;
		g = edges[faces[i].rightEdge].endPoint;
		if (a == g) {
			// end of last and start of the first are same :
			p1 = a;
			p3 = e;
			if (c == e) {
				p2 = d;
			} else {
				p2 = c;
			}
		} else {
			if (a == e) {
				// begin of first and last are the same :
				p1 = a;
				p3 = g;
				if (c == g) {
					p2 = d;
				} else {
					p2 = c;
				}
			} else {
				if (b == g) {
					// end of first and last are the same :
					p1 = b;
					p3 = e;
					if (c == e) {
						p2 = d;
					} else {
						p2 = c;
					}
				} else {
					if (b == e) {
						// end of first and begin of last are the same :
						p1 = b; 
						p3 = g;
						if (c == g) {
							p2 = d;
						} else {
							p2 = c;
						}
					}
				}
			}
		}

		if (p1 == p2 || p1 == p3 || p2 == p3) ERR3(errFile, i);
		f[3*i + 0] = p1;
		f[3*i + 1] = p2;
		f[3*i + 2] = p3;
	}
	return f;
}

unsigned int* modelEdge::getEdgesArray() const {
	unsigned int* edgeArray = new unsigned int[edges.size()*2];

	for (int i = 0; i < edges.size(); ++i ) {
		edgeArray[2*i + 0] = edges[i].startPoint;
		edgeArray[2*i + 1] = edges[i].endPoint;
	}

	return edgeArray;
}

void modelEdge::drawElements() const {
	double* vertexArray = getVertexArray();
	double* normalArray = getNormalArray();
	unsigned int* faceArray = getFaceArray();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertexArray);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// this->printInfo();

	glDrawElements(GL_TRIANGLES, faces.size()*3, GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	return;
}

void modelEdge::drawElementsWireframe() const {
	double* vertexArray = getVertexArray();
	double* normalArray = getNormalArray();
	unsigned int* faceArray = getFaceArray();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertexArray);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glDrawElements(GL_TRIANGLES, faces.size()*3, GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	return;
}

void modelEdge::drawPoints() const {
	double* vertexArray = getVertexArray();
	double* normalArray = getNormalArray();
	unsigned int* faceArray = new unsigned int[vertices.size()];
	for (int i = 0; i < vertices.size(); ++i) {
		faceArray[i] = i;
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertexArray);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glDrawElements(GL_POINTS, vertices.size(), GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	return;
}

void modelEdge::drawEdges() const {

	for (int i = 0; i < edges.size(); ++i) {
		glBegin(GL_LINES);
			vertices[edges[i].startPoint].doGlVertex3f();
			vertices[edges[i].endPoint].doGlVertex3f();
		glEnd();
	}

	return;
}

float* modelEdge::getBoundaries() const {
	float* bbb = new float[6];
	bbb[0] = min_x;
	bbb[1] = max_x;
	bbb[2] = min_y;
	bbb[3] = max_y;
	bbb[4] = min_z;
	bbb[5] = max_z;
	return bbb;
}

int isAlreadyHere(const std::vector<edge>& v, const edge& e) {
	for (unsigned int i = 0; i < v.size(); ++i) {
		if (v[i] == e) {
			return (int)i;
		}
	}

	return -1;
}

void modelEdge::moveEdgeForward(const ui s, const ui e) {
	// S : index of edge
	// E : index of new vertex
	if (s >= vertices.size() || e >= vertices.size()) {
		return;
	}

	edges[s].endPoint = e;
	return;
}

void modelEdge::removeEdge(const ui v) {
	if (v > edges.size()) {
		std::cout << "couldn't remove edge, didn't exist" << std::endl;
		return;
	}
	if (edges.size() < 3) {
		std::cout << "cant remove any more edges" << std::endl;
	}

	std::cout << "BEFORE ===============================================================================================================================" << std::endl;
	this->printInfo();
	std::cout << "GO     ===============================================================================================================================" << std::endl;

	unsigned int pointToRemove = edges[v].endPoint;
	unsigned int replacementPoint = edges[v].startPoint;

	std::cout << std::endl;

	// Replace the vertex removed with its counterpart in all edges
	for (int i = 0; i < edges.size(); ++i) {
		if (i != v) {
			// this will remove the END point of the edge from the triangulation.
			if (edges[i].startPoint == pointToRemove) {
				#ifdef FUSION_DEBUG
				std::cout << "Edge " << i << " : replacing start vertex " << edges[i].startPoint << " with " << replacementPoint << std::endl;
				#endif
				edges[i].startPoint = replacementPoint;
			}
			if (edges[i].endPoint == pointToRemove) {
				#ifdef FUSION_DEBUG
				std::cout << "Edge " << i << " : replacing end vertex " << edges[i].startPoint << " with " << replacementPoint << std::endl;
				#endif
				edges[i].endPoint = replacementPoint;
			}
		}
	}

	std::cout << std::endl;

	std::vector<std::pair<int,int>> removedEdges;

	for (int i = 0; i < edges.size(); ++i) {
		for (int j = i+1; j < edges.size(); ++j) {
			// what() ??()
			if (edges[i] == edges[j]) {
				#ifdef FUSION_DEBUG
				std::cout << "Edges " << i << " and " << j << " are the same ! Removing one ...";
				#endif
				// They should have a face in common !
				removedEdges.push_back(std::make_pair(i,j));
				#ifdef FUSION_DEBUG
				std::cout << " pushed edge " << j << " to be removed.";
				#endif
				if (edges[i].firstFace == edges[j].firstFace ) {
					#ifdef FUSION_DEBUG
					std::cout << " Starts the same (" << edges[i].firstFace << "), so swapped the faces connected : ";
					#endif
					edges[i].firstFace = edges[j].secondFace;
					#ifdef FUSION_DEBUG
					std::cout << edges[i].firstFace << "," << edges[i].secondFace << std::endl;
					#endif
				} else if (edges[i].firstFace == edges[j].secondFace) {
					#ifdef FUSION_DEBUG
					std::cout << " Start-End the same (" << edges[i].firstFace << "), so swapped the faces connected : ";
					#endif
					edges[i].firstFace = edges[j].firstFace;
					#ifdef FUSION_DEBUG
					std::cout << edges[i].firstFace << "," << edges[i].secondFace << std::endl;
					#endif
				} else if (edges[i].secondFace == edges[j].firstFace) {
					#ifdef FUSION_DEBUG
					std::cout << " End-Start the same (" << edges[i].firstFace << "), so swapped the faces connected : ";
					#endif
					edges[i].secondFace = edges[j].secondFace;
					#ifdef FUSION_DEBUG
					std::cout << edges[i].firstFace << "," << edges[i].secondFace << std::endl;
					#endif
				} else if (edges[i].secondFace == edges[i].secondFace) {
					#ifdef FUSION_DEBUG
					std::cout << " Ends the same (" << edges[i].firstFace << "), so swapped the faces connected : ";
					#endif
					edges[i].secondFace = edges[j].firstFace;
					#ifdef FUSION_DEBUG
					std::cout << edges[i].firstFace << "," << edges[i].secondFace << std::endl;
					#endif
				} else {
					#ifdef FUSION_DEBUG
					std::cout << "NOTHING. SHOULDN'T BE HERE." << std::endl;
					#endif
				}
			}
		}
	}

	#ifdef FUSION_DEBUG
	std::cout << "To be removed : <" ;
	for ( auto i : removedEdges ) {
		std::cout << " (" << i.first << "," << i.second << "),";
	}
	std::cout << ">" << std::endl << std::endl;
	#endif

	unsigned int f1 = edges[v].firstFace;
	unsigned int f2 = edges[v].secondFace;

	for (int i = 0; i < faces.size(); ++i) {
		if (i != f1 && i != f2) {
			// Swap faces
			for (int j = 0; j < removedEdges.size(); ++j) {
				faces[i].swapEdges_INT(removedEdges[j].second, removedEdges[j].first);
			}
			faces[i].alignWith(v);
			for (int j = 0; j < removedEdges.size(); ++j) {
				faces[i].alignWith(removedEdges[j].second);
			}
		}
	}

	edges.erase(edges.begin() + v);
	int nbRemoved = 1;
	for (int i = 0; i < removedEdges.size(); ++i) {
		edges.erase(edges.begin() + removedEdges[i].second - nbRemoved);
		nbRemoved++;
	}
	faces.erase(faces.begin() + f1);
	if (f1 < f2) {
		faces.erase(faces.begin() + f2 -1);
	} else {
		faces.erase(faces.begin() + f2 );
	}

	// Re-align the vertices in the edges : 
	vertices.erase(vertices.begin()+pointToRemove);
	for(int i = 0; i < edges.size(); ++i) {
		if (edges[i].startPoint >= pointToRemove) {
			edges[i].startPoint--;
		}
		if (edges[i].endPoint >= pointToRemove) {
			edges[i].endPoint--;
		}
	}

	std::cout << "AFTER  ===============================================================================================================================" << std::endl;
	this->printInfo();
	std::cout << "END    ===============================================================================================================================" << std::endl;
}

void modelEdge::removeRandomEdge() {
	unsigned int edgeToRemove = std::rand() % edges.size();
	std::cout << "Removing edge n°" << edgeToRemove << std::endl;
	this->removeEdge(edgeToRemove);
	return ;
}

void modelEdge::printInfo( bool full_vect) const {
	std::cout << "Currently has " << vertices.size() << " vertices, " << edges.size() << " edges, and " << faces.size() << " faces." << std::endl;
	if (full_vect) {
		std::cout << "Here are the contents of the vectors : " << std::endl;

		std::cout << "Vertices : " << std::endl;
		for (int i = 0; i < vertices.size(); ++i) {std::cout << i << ":" << vertices[i];}
		std::cout << std::endl;

		std::cout << "Edges : <" << std::endl;
		for (int i = 0; i < edges.size(); ++i) {
			std::cout << i << " : (" << edges[i].startPoint << "," << edges[i].endPoint << "," << edges[i].firstFace << "," << edges[i].secondFace << "), " << std::endl; 
		}
		std::cout << ">" << std::endl;

		std::cout << "Faces : <" << std::endl;
		for (int i = 0; i < faces.size(); ++i) {
			std::cout << i << " : (" << faces[i].northEdge << "," << faces[i].leftEdge << "," << faces[i].rightEdge << "), " << std::endl; 
		}
		std::cout << ">" << std::endl;
	}
}

std::vector<unsigned int> modelEdge::getVertexIndices(const ui f) const {
	if (f >= faces.size()) {
		std::cout << "couldn't get the indices of face " << f << " because the index of that face is too high" << std::endl;
		return std::vector<unsigned int>(0);
	}

	std::vector<unsigned int> vert;

	unsigned int a,b,c,d,e,g,p1,p2,p3;
	int i = f;
	a = edges[faces[i].northEdge].startPoint;
	b = edges[faces[i].northEdge].endPoint;
	c = edges[faces[i].leftEdge].startPoint;
	d = edges[faces[i].leftEdge].endPoint;
	e = edges[faces[i].rightEdge].startPoint;
	g = edges[faces[i].rightEdge].endPoint;
	if (a == g) {
		// end of last and start of the first are same :
		p1 = a;
		p3 = e;
		if (c == e) {
			p2 = d;
		} else {
			p2 = c;
		}
	} else {
		if (a == e) {
			// begin of first and last are the same :
			p1 = a;
			p3 = g;
			if (c == g) {
				p2 = d;
			} else {
				p2 = c;
			}
		} else {
			if (b == g) {
				// end of first and last are the same :
				p1 = b;
				p3 = e;
				if (c == e) {
					p2 = d;
				} else {
					p2 = c;
				}
			} else {
				if (b == e) {
					// end of first and begin of last are the same :
					p1 = b; 
					p3 = g;
					if (c == g) {
						p2 = d;
					} else {
						p2 = c;
					}
				}
			}
		}
	}

	vert.push_back(p1);
	vert.push_back(p2);
	vert.push_back(p3);

	return vert;
}

void modelEdge::subdiviseFace(const ui f) {
	std::vector<Point> new_vert;
	std::vector<edge> new_edges;
	std::vector<faceEdge> new_faces;

	unsigned int maxx = std::numeric_limits<unsigned int>::infinity();

	// for edge north, get the center point :
	Point centroid1 = Point(vertices[edges[faces[f].northEdge].startPoint]);
	centroid1.x += vertices[edges[faces[f].northEdge].endPoint].x;
	centroid1.y += vertices[edges[faces[f].northEdge].endPoint].y;
	centroid1.z += vertices[edges[faces[f].northEdge].endPoint].z;
	centroid1 = centroid1 / 2.0;
	// create two edges for it :
	edge e1 = edge(new_vert.size(), new_vert.size()+1, maxx);
	edge e2 = edge(new_vert.size()+1, new_vert.size()+2, maxx);
	// push the points onto the vector
	new_vert.push_back(vertices[edges[faces[f].northEdge].startPoint]);
	new_vert.push_back(centroid1);
	new_vert.push_back(vertices[edges[faces[f].northEdge].endPoint]);
	// push the edges :
	new_edges.push_back(e1);
	new_edges.push_back(e2);

	// do the same for the 2 remaining edges, and push the faces created
	// onto a new stack, and at the end change the vectors of the model for those ones

	// for edge north, get the center point :
	Point centroid2 = Point(vertices[edges[faces[f].leftEdge].startPoint]);
	centroid2.x += vertices[edges[faces[f].leftEdge].endPoint].x;
	centroid2.y += vertices[edges[faces[f].leftEdge].endPoint].y;
	centroid2.z += vertices[edges[faces[f].leftEdge].endPoint].z;
	centroid2 = centroid2 / 2.0;
	// create two edges for it :
	edge e3 = edge(new_vert.size(), new_vert.size()+1, maxx);
	edge e4 = edge(new_vert.size()+1, new_vert.size()+2, maxx);
	// push the points onto the vector
	new_vert.push_back(vertices[edges[faces[f].leftEdge].startPoint]);
	new_vert.push_back(centroid2);
	new_vert.push_back(vertices[edges[faces[f].leftEdge].endPoint]);
	// push the edges :
	new_edges.push_back(e3);
	new_edges.push_back(e4);

	// for edge north, get the center point :
	Point centroid3 = Point(vertices[edges[faces[f].rightEdge].startPoint]);
	centroid3.x += vertices[edges[faces[f].rightEdge].endPoint].x;
	centroid3.y += vertices[edges[faces[f].rightEdge].endPoint].y;
	centroid3.z += vertices[edges[faces[f].rightEdge].endPoint].z;
	centroid3 = centroid3 / 2.0;
	// create two edges for it :
	edge e5 = edge(new_vert.size(), new_vert.size()+1, maxx);
	edge e6 = edge(new_vert.size()+1, new_vert.size()+2, maxx);
	// push the points onto the vector
	new_vert.push_back(vertices[edges[faces[f].rightEdge].startPoint]);
	new_vert.push_back(centroid3);
	new_vert.push_back(vertices[edges[faces[f].rightEdge].endPoint]);
	// push the edges :
	new_edges.push_back(e5);
	new_edges.push_back(e6);

	edge e7 = edge(new_vert.size()-2,new_vert.size()-5,new_faces.size());
	edge e8 = edge(new_vert.size()-5,new_vert.size()-8,new_faces.size()+1);
	edge e9 = edge(new_vert.size()-8,new_vert.size()-2,new_faces.size()+2);

	new_edges.push_back(e7);
	new_edges.push_back(e8);
	new_edges.push_back(e9);

	// inside face :
	faceEdge f1 = faceEdge(new_vert.size()-1, new_vert.size()-2, new_vert.size()-3);
	// faceEdge f2 = faceEdge(new_vert.size()-1);

	this->vertices = new_vert;
	this->edges = new_edges;
	this->faces = new_faces;
}