#include "../headers/options.h"

#include <math.h>
#include <iostream>

options::options() {
	// Default constructor :
	Light0Pos = (GLfloat*)malloc(4*sizeof(GLfloat));
	Light0Amb = (GLfloat*)malloc(4*sizeof(GLfloat));
	Light0Dif = (GLfloat*)malloc(4*sizeof(GLfloat));
	Light0Spe = (GLfloat*)malloc(4*sizeof(GLfloat));

	MIN_X_BOUNDING = -15.0;
	MAX_X_BOUNDING =  15.0;
	MIN_Y_BOUNDING = -15.0;
	MAX_Y_BOUNDING =  15.0;
	MIN_Z_BOUNDING = -15.0;
	MAX_Z_BOUNDING =  15.0;

	Light0Pos[0] = 0.0;
	Light0Pos[1] = 0.0;
	Light0Pos[2] = MAX_Z_BOUNDING;
	Light0Pos[3] = 0.0;
	lightPos = Point(Light0Pos[0], Light0Pos[1], Light0Pos[2]);

	x = Vector(1.0,0.0,0.0);
	y = Vector(0.0,2.0,0.0);

	cameraOptions = camera();
	horizontalOffset = 0.0;
	verticalOffset = 0.5;
	stepHorizontal = 0.01;
	stepVertical = 0.01;
	cameraOptions.setOrbitAt(horizontalOffset, verticalOffset);

	/**
	 * Draw mode by default is GL_TRIANGLES. Can be 
	 * switched to GL_LINES, GL_POINTS ... depending
	 * on the program, and the need for it.
	 */
	drawMode = GL_TRIANGLES;
	drawAll = true;
}

void options::setCamera(const camera& c) {
	cameraOptions.setParameters(c);
}

void options::moveCameraUp() {
	verticalOffset = (verticalOffset > 0.99) ? 0.99 : verticalOffset + stepVertical;
	cameraOptions.setOrbitAt(horizontalOffset, verticalOffset);
}

void options::moveCameraDown() {
	verticalOffset = (verticalOffset < 0.01) ? 0.01 : verticalOffset - stepVertical;
	cameraOptions.setOrbitAt(horizontalOffset, verticalOffset);
}

void options::moveCameraLeft() {
	horizontalOffset += stepHorizontal;
	cameraOptions.setOrbitAt(horizontalOffset, verticalOffset);
}

void options::moveCameraRight() {
	horizontalOffset -= stepHorizontal;
	cameraOptions.setOrbitAt(horizontalOffset, verticalOffset);
}

void options::setCameraCenter(const Point& c) {
	cameraOptions.setCenter(c);
}

void options::setCameraCenter(double x, double y, double z) {
	cameraOptions.setCenter(x,y,z);
}

void options::updateLight() const {
	glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
}

void options::updateCamera() {
	cameraOptions.update();
	updateOrtho();
}

void options::setOrthoArray(const double* bbb) {
	double deltaX = abs(bbb[1] - bbb[0]);
	double deltaY = abs(bbb[3] - bbb[2]);
	double deltaZ = abs(bbb[5] - bbb[4]);
	double dist = sqrt(pow(deltaX/2.0, 2.0) + pow(deltaY/2.0, 2.0) + pow(deltaZ/2.0, 2.0));

	this->cameraOptions.centerX = bbb[0] + deltaX/2.0f;
	this->cameraOptions.centerY = bbb[2] + deltaY/2.0f;
	this->cameraOptions.centerZ = bbb[4] + deltaZ/2.0f;

	MIN_X_BOUNDING = this->cameraOptions.centerX - dist;
	MAX_X_BOUNDING = this->cameraOptions.centerX + dist;
	MIN_Y_BOUNDING = this->cameraOptions.centerY - dist;
	MAX_Y_BOUNDING = this->cameraOptions.centerY + dist;
	MIN_Z_BOUNDING = this->cameraOptions.centerZ - dist;
	MAX_Z_BOUNDING = this->cameraOptions.centerZ + dist;

	std::cout << "new bounds : " <<std::endl;
	std::cout << MIN_X_BOUNDING << ";" << MAX_X_BOUNDING << std::endl;
	std::cout << MIN_Y_BOUNDING << ";" << MAX_Y_BOUNDING << std::endl;
	std::cout << MIN_Z_BOUNDING << ";" << MAX_Z_BOUNDING << std::endl;
}

void options::updateOrtho() const {
	float delta = abs(MAX_X_BOUNDING - MIN_X_BOUNDING) / 2.0;

	// std::cout << "gonna give to glOrtho : " << std::endl;
	// std::cout << "\t" << cameraOptions.centerX - ( delta * cameraOptions.zoomLevel ) <<std::endl;
	// std::cout << "\t" << cameraOptions.centerX + ( delta * cameraOptions.zoomLevel ) <<std::endl;
	// std::cout << "\t" << cameraOptions.centerY - ( delta * cameraOptions.zoomLevel ) <<std::endl;
	// std::cout << "\t" << cameraOptions.centerY + ( delta * cameraOptions.zoomLevel ) <<std::endl;
	// std::cout << "\t" << cameraOptions.centerZ - ( delta * cameraOptions.zoomLevel ) <<std::endl;
	// std::cout << "\t" << cameraOptions.centerZ + ( delta * cameraOptions.zoomLevel ) <<std::endl;

	glOrtho((cameraOptions.centerX - ( delta * cameraOptions.zoomLevel ) ),	// * cameraOptions.zoomLevel
		(cameraOptions.centerX + ( delta * cameraOptions.zoomLevel ) ),	// * cameraOptions.zoomLevel
		(cameraOptions.centerY - ( delta * cameraOptions.zoomLevel ) ),	// * cameraOptions.zoomLevel
		(cameraOptions.centerY + ( delta * cameraOptions.zoomLevel ) ),	// * cameraOptions.zoomLevel
		(cameraOptions.centerZ - ( delta * cameraOptions.zoomLevel ) ),	// * cameraOptions.zoomLevel
		(cameraOptions.centerZ + ( delta * cameraOptions.zoomLevel ) ));	// * cameraOptions.zoomLevel
}

void options::printOrtho() const {
	float delta = abs(MAX_X_BOUNDING - MIN_X_BOUNDING) / 2.0;

	std::cout << "gonna give to glOrtho : " << std::endl;
	std::cout << "\t" << cameraOptions.centerX - ( delta * cameraOptions.zoomLevel ) ;
	std::cout << "//" << cameraOptions.centerX + ( delta * cameraOptions.zoomLevel ) <<std::endl;
	std::cout << "\t" << cameraOptions.centerY - ( delta * cameraOptions.zoomLevel ) ;
	std::cout << "//" << cameraOptions.centerY + ( delta * cameraOptions.zoomLevel ) <<std::endl;
	std::cout << "\t" << cameraOptions.centerZ - ( delta * cameraOptions.zoomLevel ) ;
	std::cout << "//" << cameraOptions.centerZ + ( delta * cameraOptions.zoomLevel ) <<std::endl;

	std::cout << "Camera is at : " << std::endl;
	std::cout << "\t" << this->cameraOptions.eyeX << std::endl;
	std::cout << "\t" << this->cameraOptions.eyeY << std::endl;
	std::cout << "\t" << this->cameraOptions.eyeZ << std::endl;
}

void options::zoom(const double scalar) {
	cameraOptions.zoom(scalar);
}

void options::unzoom(const double scalar) {
	cameraOptions.unzoom(scalar);
}

void options::printparams() {
	std::cout << "CAMERA PARAMS : " << std::endl;
	cameraOptions.printparams();
	std::cout << "==================" << std::endl;
	std::cout << "orbit params : " << horizontalOffset << "," << verticalOffset << std::endl;
	std::cout << "step orbits : " << stepHorizontal << "," << stepVertical << std::endl;

	std::cout << "boundaries : " << std::endl;
	std::cout << MIN_X_BOUNDING << ";" << MAX_X_BOUNDING << std::endl;
	std::cout << MIN_Y_BOUNDING << ";" << MAX_Y_BOUNDING << std::endl;
	std::cout << MIN_Z_BOUNDING << ";" << MAX_Z_BOUNDING << std::endl;
}

camera::camera() {
	eyeX = 0.0;
	eyeY = 0.0;
	eyeZ = 1.0;

	centerX = 0.0;
	centerY = 0.0;
	centerZ = 0.0;

	upX = 0.0;
	upY = 1.0;
	upZ = 0.0;

	zoomLevel = 1.0;
}

camera::camera(const camera& c) {
	eyeX = c.eyeX;
	eyeY = c.eyeY;
	eyeZ = c.eyeZ;

	centerX = c.centerX;
	centerY = c.centerY;
	centerZ = c.centerZ;

	upX = c.upX;
	upY = c.upY;
	upZ = c.upZ;

	horiOff = c.horiOff;
	vertOff = c.vertOff;

	zoomLevel = c.zoomLevel;
}

camera::camera(double x, double y, double z) {
	eyeX = x;
	eyeY = y;
	eyeZ = z;

	centerX = 0.0;
	centerY = 0.0;
	centerZ = 0.0;

	upX = 0.0;
	upY = 1.0;
	upZ = 0.0;

	horiOff = 0.0;
	vertOff = 0.5;

	zoomLevel = 1.0;
}

void camera::setParameters(const camera& c) {
	eyeX = c.eyeX;
	eyeY = c.eyeY;
	eyeZ = c.eyeZ;

	centerX = c.centerX;
	centerY = c.centerY;
	centerZ = c.centerZ;

	upX = c.upX;
	upY = c.upY;
	upZ = c.upZ;

	horiOff = c.horiOff;
	vertOff = c.vertOff;

	zoomLevel = c.zoomLevel;
}

void camera::update() {
	this->computeOrbit();
	gluLookAt(
		eyeX, eyeY, eyeZ,
		centerX, centerY, centerZ,
		upX, upY, upZ
	);
}

void camera::computeOrbit() {
	eyeX = this->centerX + cos(horiOff * 2 * M_PI) * sin(vertOff * M_PI);
	eyeY = this->centerY + cos(vertOff * M_PI);
	eyeZ = this->centerZ + sin(vertOff * M_PI) * sin(horiOff * 2 * M_PI);
}

void camera::setCenter(double x, double y, double z) {
	centerX = x;
	centerY = y;
	centerZ = z;
}

void camera::setCenter(const Point& c) {
	centerX = c.getX();
	centerY = c.getY();
	centerZ = c.getZ();
}

void camera::setPosition(double x, double y, double z) {
	eyeX = x;
	eyeY = y;
	eyeZ = z;
}

void camera::setPosition(const Point& p) {
	eyeX = p.getX();
	eyeY = p.getY();
	eyeZ = p.getZ();
}

void camera::setOrbitAt(double h, double v) {
	horiOff = h;
	vertOff = v;
}

void camera::zoom(const double scalar) {
	zoomLevel -= scalar;
}

void camera::unzoom(const double scalar) {
	zoomLevel += scalar;
}

void camera::printparams() {
	std::cout << "Center at : " << centerX << "," << centerY << "," << centerZ << std::endl;
	std::cout << "Eye at : " << eyeX << "," << eyeY << "," << eyeZ << std::endl;
	std::cout << "Up is : " << upX << "," << upY << "," << upZ << std::endl;

	std::cout << "orbit parameters : " << horiOff << "," << vertOff << std::endl;
	std::cout << "zoomed : " << zoomLevel << std::endl;
}