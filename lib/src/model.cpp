#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <GL/glut.h>
#include <fcntl.h>
#include <cfloat>
#include <algorithm>

#include "../headers/model.h"
#include "../headers/drawings.h"

Model::Model() {}

Model::Model(const char* pathToModel) {
	this->readFile(pathToModel);
	printf("Model contains %i points and %i triangles\n", vertices.size(), faces.size());
}

void Model::readFile(const char* pathToModel) {
	FILE* modelFile = fopen(pathToModel, "r");
	if (modelFile == NULL) {
		printf("Could not read model. Model not instanciated.\n");
		exit(1);
	}

	min_x = FLT_MAX;
	min_y = FLT_MAX;
	min_z = FLT_MAX;
	max_x = FLT_MIN;
	max_y = FLT_MIN;
	max_z = FLT_MIN;

	// Verifier que le fichier est "OFF"
	fscanf(modelFile, "OFF ");

	int nbPoints, nbTriangles, nbAretes;
	fscanf(modelFile, "%i %i %i", &nbPoints, &nbTriangles, &nbAretes);

	centroid = Point(0.0,0.0,0.0);

	double px,py,pz;
	for (long int i = 0; i < nbPoints; ++i) {
		// prends les coordonnées du point
		fscanf(modelFile, "%lf %lf %lf",&px,&py,&pz);
		vertices.push_back(Point(px,py,pz));
		centroid += Point(px,py,pz);
		// Definition boundaries X
		if (px < min_x) {
			min_x = px;
		} else if (px > max_x) {
			max_x = px;
		}
		// Definition boundaries Y
		if (py < min_y) {
			min_y = py;
		} else if (py > max_y) {
			max_y = py;
		}
		// Definition boundaries Z
		if (pz < min_z) {
			min_z = pz;
		} else if (pz > max_z) {
			max_z = pz;
		}
	}

	centroid /= (const double)nbPoints;

	int indiceN, indiceL, indiceR, nbLol;
	for (long int i = 0; i < nbTriangles; ++i) {
		fscanf(modelFile, "%i %i %i %i", &nbLol, &indiceN, &indiceL, &indiceR);
		Face f(indiceN, indiceL, indiceR);
		f.setNormal( Vector(vertices[indiceN], vertices[indiceL]).vectoriel(Vector(vertices[indiceN], vertices[indiceR])).normalize() );
		faces.push_back(f);
	}

	for (int i = 0; i < faces.size(); ++i) {
		vertices[faces[i].getNorth()].setNormal(vertices[faces[i].getNorth()].getNormal() + faces[i].getNormal());
		vertices[faces[i].getLeft()].setNormal(vertices[faces[i].getLeft()].getNormal() + faces[i].getNormal());
		vertices[faces[i].getRight()].setNormal(vertices[faces[i].getRight()].getNormal() + faces[i].getNormal());
	}

	for (int i = 0; i < vertices.size(); ++i) {
		vertices[i].setNormal(vertices[i].getNormal().normalize());
	}

	fclose(modelFile);
	return;
}

void Model::draw() const {

	for(unsigned int i = 0; i < faces.size(); i++){
		Vector v1 = Vector(vertices[faces[i].getNorth()], vertices[faces[i].getLeft()]);
		Vector v2 = Vector(vertices[faces[i].getNorth()], vertices[faces[i].getRight()]);
		Vector normal = v1.vectoriel(v2);
		// normal.scale(-1.0);
		glNormal3f(normal.getX(), normal.getY(), normal.getZ());
		glBegin(GL_TRIANGLES);
			glVertex3f( vertices[faces[i].getLeft()].getX(), vertices[faces[i].getLeft()].getY(), vertices[faces[i].getLeft()].getZ());
			glVertex3f( vertices[faces[i].getRight()].getX(), vertices[faces[i].getRight()].getY(), vertices[faces[i].getRight()].getZ());
			glVertex3f( vertices[faces[i].getNorth()].getX(), vertices[faces[i].getNorth()].getY(), vertices[faces[i].getNorth()].getZ());
		glEnd();
	}

}

void Model::printfInfo() const {
	printf("Contains %i vert %i face\n", vertices.size(), faces.size());
	return;
}

std::vector<float> Model::getModelBoundaries() const {
	std::vector<float> bounds;
	bounds.push_back(min_x * 1.2);
	bounds.push_back(max_x * 1.2);
	bounds.push_back(min_y * 1.2);
	bounds.push_back(max_y * 1.2);
	bounds.push_back(min_z * 1.2);
	bounds.push_back(max_z * 1.2);
	return bounds;
}

float Model::getLargestBoundary() const {
	float delta_x = (float)max_x - (float)min_x;
	float delta_y = (float)max_y - (float)min_y;
	float delta_z = (float)max_z - (float)min_z;

	return (max_x > max_y) ? (max_x > max_z) ? max_x : max_z : (max_z > max_y) ? max_z : max_y;
}

float Model::getLowestBoundary() const {
	float delta_x = (float)max_x - (float)min_x;
	float delta_y = (float)max_y - (float)min_y;
	float delta_z = (float)max_z - (float)min_z;

	return (min_x < min_y) ? (min_x < min_z) ? min_x : min_z : (min_z < min_y) ? min_z : min_y;
}

double* Model::getNormals() const {
	double* normalArray = new double[vertices.size()*3];
	for (int i = 0; i < vertices.size(); ++i) {
		double* currentNormal = vertices[i].getNormalArray();
		normalArray[3*i+0] = currentNormal[0];
		normalArray[3*i+1] = currentNormal[1];
		normalArray[3*i+2] = currentNormal[2];
	}
	return normalArray;
}

double* Model::getVertexArray() const {
	double* vertexArray = new double[vertices.size()*3];
	for (int i = 0; i < vertices.size(); ++i) {
		double* currentVertex = Point(vertices[i]).toArray();
		vertexArray[3*i+0] = currentVertex[0];
		vertexArray[3*i+1] = currentVertex[1];
		vertexArray[3*i+2] = currentVertex[2];
	}
	return vertexArray;
}

unsigned int* Model::getFaceArray() const {
	unsigned int* faceArray = new unsigned int[faces.size()*3];
	for (int i = 0; i < faces.size(); ++i) {
		unsigned int* currentVertex = faces[i].getArray();
		faceArray[3*i+0] = currentVertex[0];
		faceArray[3*i+1] = currentVertex[1];
		faceArray[3*i+2] = currentVertex[2];
	}
	return faceArray;
}

void Model::drawGaussianImage() const {
	// get point normals as points :
	double* p = getNormals();

	unsigned int* f = new unsigned int[vertices.size()];
	for (unsigned int i = 0; i < vertices.size(); ++i)
		f[i] = i;

	// enable draw mode :
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, p);

	glDrawElements(GL_POINTS, vertices.size(), GL_UNSIGNED_INT, f);

	glDisableClientState(GL_VERTEX_ARRAY);
}

void Model::drawElements() const {
	double* v = this->getVertexArray();
	double* n = this->getNormals();
	unsigned int* f = this->getFaceArray();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, v);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, n);

	glDrawElements(GL_TRIANGLES, this->getNbFaces()*3, GL_UNSIGNED_INT, f);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void Model::drawWireframe() const {
	double* v = this->getVertexArray();
	double* n = this->getNormals();
	unsigned int* f = this->getFaceArray();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, v);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, n);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glDrawElements(GL_TRIANGLES, this->getNbFaces()*3, GL_UNSIGNED_INT, f);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void Model::drawDihedralAngles(double threshold) const {
	std::vector<meshFace> meshfaces;
	for (int i = 0; i < faces.size(); ++i) 
		meshfaces.push_back(faces.at(i).toMeshFace(vertices));
	
	// now for evey one of them see if they are below threshold
	for (int i = 0; i < meshfaces.size(); ++i) {
		for (int j = i; j < meshfaces.size(); ++j) {
			double guessedAngle = meshfaces.at(i).getDihedralAngle(meshfaces.at(j));
			if (!isnan(guessedAngle) && guessedAngle > 0.0 && guessedAngle < 180.0 && guessedAngle > threshold) {
				std::vector<Point> common = meshfaces.at(i).getCommonPoints(meshfaces.at(j));
				if (common.size() >= 2) {
					DrawLine(common[1]-common[0], common[0], 1.0, 0.0, 0.0, 5.0);
				}
			}
		}
	}
	return;
}

Point Model::getCentroid() const {
	return Point(centroid);
}
