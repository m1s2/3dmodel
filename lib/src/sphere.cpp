#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <GL/glut.h>

#include "../headers/Point.h"
#include "../headers/sphere.h"
#include "../headers/Vector.h"
#include "../headers/beziers.h"
#include "../headers/definitions.h"
#include "../headers/face.h"

Sphere::Sphere() {
	this->center = Point(0.0,0.0,0.0);
	this->radius = 10;
	this->nbParallels = 8;
	this->nbMeridians = 8;
	this->surface = std::vector<std::vector<Point>>();
}

Sphere::Sphere(Point p, int r) {
	this->center = Point(p.getX(), p.getY(), p.getZ());
	this->radius = r;
	this->nbParallels = 8;
	this->nbMeridians = 8;
	this->surface = std::vector<std::vector<Point>>();
}

Sphere::Sphere(Point p, int r, int par, int mer) {
	this->center = Point(p.getX(), p.getY(), p.getZ());
	this->radius = r;
	this->nbParallels = par;
	this->nbMeridians = mer;
	this->surface = std::vector<std::vector<Point>>();
}

void Sphere::calculate() {
	this->northPole = Point(center.getX(), center.getY()+radius, center.getZ());
	this->southPole = Point(center.getX(),center.getY()-radius, center.getZ());

	this->surface.clear();

	for (int i = 0; i < nbParallels; ++i)
		surface.push_back(std::vector<Point>());

	// Ici : calcul des angles de tous les points d'intersection
	// des parallèles aux méridiens 
	for (int j = 0; j < nbParallels; ++j) {
		double phi = (double)(j+1) * (M_PI/(nbParallels+1.0));
		// Ici : calcul des angles de tous les points des
		// intersections de méridiens et des parallèles
		for (int i = 0; i < nbMeridians; ++i) {
			double theta = (double)(i) * ((double)(2.0 * M_PI)/(nbMeridians));
			surface.at(j).push_back(Point(center.getX() + radius * std::sin(phi) * std::cos(theta),
						center.getY() + radius * std::cos(phi), 
						center.getZ() + radius * std::sin(phi) * std::sin(theta)  ));
		}
	}

	// Generate normals :
	northPole.setNormal(Vector(center, northPole).normalize());
	southPole.setNormal(Vector(center, southPole).normalize());
	//then, for each point :
	for (int i = 0; i < nbParallels; ++i) {
		for (int j = 0; j < nbMeridians; ++j) {
			// Each point has at least 5, at most 6 neighboring faces 
			// (can have 4 in the extreme case sphere only has one parallel)
			Vector top_left, top_middle, top_right;
			Vector bot_left, bot_middle, bot_right;
			Vector all_top, all_bot;
			int last = (j == 0) ? nbMeridians-1 : j-1;
			int next = (j == nbMeridians-1) ? 0 : j+1;
			if (i == 0) { // point is connected to north pole
				top_middle = Vector(0.0,0.0,0.0);
				top_left = Vector(surface[i][j], northPole).vectoriel(Vector(surface[i][last], surface[i][j]));
				top_right = Vector(surface[i][j], northPole).vectoriel(Vector(surface[i][j], surface[i][next]));
			} else {
				int top = i-1; // we sure it cannot be negative, since above is i == 0
				top_left = Vector(surface[i][last], surface[top][last]).vectoriel(Vector(surface[i][last], surface[i][j]));
				top_middle = Vector(surface[i][j], surface[top][j]).vectoriel(Vector(surface[top][j], surface[top][next]));
				top_right = Vector(surface[i][j], surface[top][j]).vectoriel(Vector(surface[i][j], surface[i][next]));
			}
			all_top = top_left + top_middle + top_right;
			if (i == nbParallels-1) { // point is connected to south pole
				bot_middle = Vector(0.0,0.0,0.0);
				bot_left = Vector(southPole, surface[i][j]).vectoriel(Vector(surface[i][last], surface[i][j]));
				bot_right = Vector(southPole, surface[i][j]).vectoriel(Vector(surface[i][j], surface[i][next]));
			} else {
				int bot = i+1;
				bot_left = Vector(surface[bot][j], surface[i][j]).vectoriel(Vector(surface[i][last], surface[i][j]));
				bot_middle = Vector(surface[bot][j], surface[i][j]).vectoriel(Vector(surface[bot][j], surface[bot][next]));
				bot_right = Vector(surface[bot][next], surface[i][next]).vectoriel(Vector(surface[i][j], surface[i][next]));
			}
			all_bot = bot_left + bot_middle + bot_right;
			surface[i][j].setNormal( (all_top+all_bot).normalize() );
		} 
	}
}

#pragma region Getters/Setters

void Sphere::addParallel() {
	this->nbParallels++;
	this->calculate();
}

void Sphere::addMeridian() {
	this->nbMeridians++;
	this->calculate();
}

void Sphere::removeParallel() {
	if (this->nbParallels > 2) {
		this->nbParallels--;
	}
}

void Sphere::removeMeridian() {
	if (this->nbMeridians > 1) {
		this->nbMeridians--;
	}
}

void Sphere::setOrigin(Point p) {
	this->center = Point(p.getX(), p.getY(), p.getZ());
}

void Sphere::setRadius(int r) {
	this->radius = r;
}

#pragma endregion

#pragma region Drawing functions (basic openGL) 

void Sphere::draw() {
	// Calcul uniquement lorsqu'on doit 
	// dessiner la sphere :
	this->calculate();

	glPolygonMode( GL_FRONT, GL_LINE );
	glColor3f(0.8,0.8,0.8);

	// Dessine les liens au pole nord
	glBegin(GL_TRIANGLE_FAN);
		glVertex3d(northPole.getX(), northPole.getY(), northPole.getZ());
		for (int i = 0; i < nbMeridians; ++i) {
			glVertex3d(surface.at(0).at(i).getX(), surface.at(0).at(i).getY(), surface.at(0).at(i).getZ());
		}
		glVertex3d(surface.at(0).at(nbMeridians-1).getX(), surface.at(0).at(nbMeridians-1).getY(), surface.at(0).at(nbMeridians-1).getZ());
	glEnd();

	// Dessine le 'corps' de la sphère
	glBegin(GL_QUADS);
		for (int i = 0; i < nbParallels-1; ++i) {
			for (int j = 0; j < nbMeridians-1; ++j) {
				glVertex3d(surface.at(i+0).at(j+0).getX(), surface.at(i+0).at(j+0).getY(), surface.at(i+0).at(j+0).getZ());
				glVertex3d(surface.at(i+1).at(j+0).getX(), surface.at(i+1).at(j+0).getY(), surface.at(i+1).at(j+0).getZ());
				glVertex3d(surface.at(i+1).at(j+1).getX(), surface.at(i+1).at(j+1).getY(), surface.at(i+1).at(j+1).getZ());
				glVertex3d(surface.at(i+0).at(j+1).getX(), surface.at(i+0).at(j+1).getY(), surface.at(i+0).at(j+1).getZ());
			}
		}
	glEnd();

	// Dessine les liens au pole sud
	glBegin(GL_TRIANGLE_FAN);
		glVertex3d(southPole.getX(), southPole.getY(), southPole.getZ());
		for (int i = 0; i < nbMeridians; ++i) {
			glVertex3d(surface.at(nbParallels-1).at(i).getX(), surface.at(nbParallels-1).at(i).getY(), surface.at(nbParallels-1).at(i).getZ());
		}
		glVertex3d(surface.at(nbParallels-1).at(0).getX(), surface.at(nbParallels-1).at(0).getY(), surface.at(nbParallels-1).at(0).getZ());
	glEnd();
}

void Sphere::drawWireFrame() {
	glColor3f(0.9,0.4,0.1);

	glLineWidth(2.0);

	// Lignes horizontales :
	for (int i = 0; i < nbParallels; ++i) {
		glBegin(GL_LINE_STRIP);
			for (int j = 0; j < nbMeridians; ++j)
				glVertex3d(this->surface.at(i).at(j).getX(), this->surface.at(i).at(j).getY(), this->surface.at(i).at(j).getZ());
		glEnd();
	}
	// Lignes verticales :
	for (int j = 0; j < nbMeridians; ++j) {
		glBegin(GL_LINE_STRIP);
			glVertex3f(northPole.getX(),northPole.getY(),northPole.getZ());
			for (int i = 0; i < nbParallels; ++i) 
				glVertex3d(this->surface.at(i).at(j).getX(), this->surface.at(i).at(j).getY(), this->surface.at(i).at(j).getZ());
			glVertex3f(southPole.getX(),southPole.getY(),southPole.getZ());
		glEnd();
	}
}

#pragma endregion

double* Sphere::getTriangleMesh() const {
	// number of points :
	long int nbPoints = nbMeridians * nbParallels * 3 /* all points in the sphere's 'core' */ \
				+ 3 /* north pole */ \
				+ 3 /* south pole */ ;
	// Allocate new array :
	double* vertexArray = new double[nbPoints];
	// start by the north pole :
	double* northPoleCoordinates = northPole.toArray();
	vertexArray[0] = northPoleCoordinates[0];
	vertexArray[1] = northPoleCoordinates[1];
	vertexArray[2] = northPoleCoordinates[2];
	// follow up by every point on the sphere, parallel by parallel :
	// for every parallel :
	for (int i = 0; i < nbParallels; ++i) {
		int offset = i * nbMeridians * 3;
		// for every point on this parallel : 
		for (int j = 0; j < nbMeridians; ++j) {
			double* currentVertex = surface[i][j].toArray();
			vertexArray[3 + offset + 3*j + 0] = currentVertex[0];
			vertexArray[3 + offset + 3*j + 1] = currentVertex[1];
			vertexArray[3 + offset + 3*j + 2] = currentVertex[2];
		}
	}
	// finish with the south pole :
	double* southPoleCoordinates = southPole.toArray();
	vertexArray[nbPoints-3] = southPoleCoordinates[0];
	vertexArray[nbPoints-2] = southPoleCoordinates[1];
	vertexArray[nbPoints-1] = southPoleCoordinates[2];
	return vertexArray;
}

double* Sphere::getNormals() const {
	// allocate array for normals :
	double* normalArray = new double[(nbMeridians*nbParallels+2)*3];
	// add normal of north pole first :
	double* northPoleNormal = northPole.getNormal().toArray();
	normalArray[0] = northPoleNormal[0];
	normalArray[1] = northPoleNormal[1];
	normalArray[2] = northPoleNormal[2];
	for (int i = 0; i < nbParallels; ++i) {
		for (int j = 0; j < nbMeridians; ++j) {
			double* currentNormal = surface[i][j].getNormal().toArray();
			normalArray[3+i*nbMeridians*3+3*j+0] = currentNormal[0];
			normalArray[3+i*nbMeridians*3+3*j+1] = currentNormal[1];
			normalArray[3+i*nbMeridians*3+3*j+2] = currentNormal[2];
		}
	}
	double* southPoleNormal = southPole.getNormal().toArray();
	normalArray[(nbMeridians*nbParallels+2)*3-3] = southPoleNormal[0];
	normalArray[(nbMeridians*nbParallels+2)*3-2] = southPoleNormal[1];
	normalArray[(nbMeridians*nbParallels+2)*3-1] = southPoleNormal[2];
	return normalArray;
}

unsigned int* Sphere::getFaces() const {
	// calculate the number of points necessary :
	/**
	 * for each face (para * meri), it has to have one face on top of him, and
	 * one face on the bottom (so * 2), and each face is 3 points (so * 3)
	 */
	int nbPoints = nbParallels * nbMeridians * 3 * 4;

	/**
	 * 
	 * Here are the faces this function is drawing, for each point P :
	 * 				____________________
	 * 				|                 /|
	 * 				|               /  |
	 * 				|   Face 1    /    |
	 * 				|           /      |
	 * 				|         /        |
	 * 				|       /          |
	 * 				|     /            |
	 * 				|   /     Face 2   |
	 * 				| /                |
	 * 				--------------------
	 * 	   ____________________(P)
	 * 	   |                 /|
	 * 	   |               /  |
	 * 	   |   Face 3    /    |
	 * 	   |           /      |
	 * 	   |         /        |
	 * 	   |       /          |
	 * 	   |     /            |
	 * 	   |   /     Face 4   |
	 * 	   | /                |
	 * 	   --------------------
	 * 
	 * As such, for each point, you need 12 vertex indices for the 4 faces it draws.
	 * Here are some other configurations (still needs 12 points, whetever they are) :
	 * 
	 * If the point P is directly connected to a pole, the faces become :
	 * 
	 * 			Pole ->	X 
	 * 	N.B. : 			|\
	 * 	For the south pole,	|  \
	 * 	The faces are just	|    \
	 * 	reversed compared to	|Faces \
	 * 	this configuration	|1 and 2 \
	 * 				| ( this   \
	 * 				|  face is   \
	 * 				|  drawn       \
	 * 				|  twice )       \
	 * 				-------------------
	 * 	   ____________________(P)
	 * 	   |                 /|
	 * 	   |               /  |
	 * 	   |   Face 3    /    |
	 * 	   |           /      |
	 * 	   |         /        |
	 * 	   |       /          |
	 * 	   |     /            |
	 * 	   |   /     Face 4   |
	 * 	   | /                |
	 * 	   --------------------
	 */

	unsigned int* faceArray = new unsigned int[nbPoints];

	unsigned int index_current;
	unsigned int index_top;
	unsigned int index_lower;
	unsigned int index_next;
	unsigned int index_last;
	unsigned int index_next_top;
	unsigned int index_last_lower;

	for (int i = 0; i < nbParallels; ++i) {
		for (int j = 0; j < nbMeridians; ++j) {
			if (i == 0) {
				/**
				 * Then its the top row, connected to the north pole
				 * Next top becomes next vertex, since top is the north pole
				 */
				index_top = 0;
				index_next_top = ( (j == nbMeridians-1) ? 0 : j+1 ) + 1;
			} else {
				index_top = j + (i-1) * nbMeridians + 1; 
				index_next_top = ( (j == nbMeridians-1) ? 0 : j+1 ) + nbMeridians * i + 1;
			}
			if (i == nbParallels-1) {
				/**
				 * The face is then connected to the south pole
				 * The last bottom vertex is then the last vertex, since it is connected to the south pole
				 */
				index_lower = nbParallels * nbMeridians + 1;
				index_last_lower = ( (j == 0) ? nbMeridians-1 : j-1 ) + i * nbMeridians + 1;
			} else {
				index_lower = j + (i+1) * nbMeridians + 1;
				index_last_lower = ( (j == 0) ? nbMeridians-1 : j-1 ) + (i+1) * nbMeridians + 1;
			}
			index_current = j + nbMeridians * i + 1;
			index_next = ( (j == nbMeridians-1) ? 0 : j+1 ) + nbMeridians * i + 1;
			index_last = ( (j == 0) ? nbMeridians-1 : j-1 ) + nbMeridians * i + 1;
			/**
			 * Top faces
			 */
			faceArray[i*nbMeridians*12 + 12*j+00] = index_current;
			faceArray[i*nbMeridians*12 + 12*j+01] = index_next;
			faceArray[i*nbMeridians*12 + 12*j+02] = index_next_top;
			faceArray[i*nbMeridians*12 + 12*j+03] = index_current;
			faceArray[i*nbMeridians*12 + 12*j+04] = index_next_top;
			faceArray[i*nbMeridians*12 + 12*j+05] = index_top;
			
			/**
			 * Bottom faces
			 */
			faceArray[i*nbMeridians*12 + 12*j+ 6] = index_last ;
			faceArray[i*nbMeridians*12 + 12*j+ 7] = index_last_lower ;
			faceArray[i*nbMeridians*12 + 12*j+ 8] = index_current ;
			faceArray[i*nbMeridians*12 + 12*j+ 9] = index_current ;
			faceArray[i*nbMeridians*12 + 12*j+10] = index_last_lower ;
			faceArray[i*nbMeridians*12 + 12*j+11] = index_lower ;
		}
	}

	return faceArray;
}

double Sphere::getLowerBoundary() const {
	// TODO : implement a check of the lower coordinate.
	// For now, sends back the X coord of south pole
	return southPole.getY();
}

double Sphere::getHigherBoundary() const {
	// TODO : see getLowerBoundary, same
	return northPole.getY();
}

void Sphere::drawElements() const {
	double* vertices = this->getTriangleMesh();
	double* normalArray = this->getNormals();
	unsigned int* faceArray = this->getFaces();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertices);
	
	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);
	
	glDrawElements(GL_TRIANGLES, nbParallels * nbMeridians * 12, GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	free(vertices);
	free(normalArray);
	free(faceArray);
}

void Sphere::drawElements_Wireframe() const {
	double* vertices = this->getTriangleMesh();
	double* normalArray = this->getNormals();
	unsigned int* faceArray = this->getFaces();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, vertices);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, normalArray);

	glDrawElements(GL_LINES, nbParallels * nbMeridians * 12, GL_UNSIGNED_INT, faceArray);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	free(vertices);
	free(normalArray);
	free(faceArray);
}

void Sphere::drawDihedralAngles(double threshold) const {
	double* v = getTriangleMesh();
	unsigned int* f = getFaces();

	std::vector<Face> allFaces;
	for (int i = 0; i < nbMeridians*nbParallels*4; ++i) {
		allFaces.push_back(Face(f[3*i+0], f[3*i+1], f[3*i+2]));
	}
	std::vector<meshFace> allmesh;
	for (int i = 0; i < allFaces.size(); ++i) {
		allmesh.push_back(allFaces.at(i).toMeshFace(v));
	}

	// now for evey one of them see if they are below threshold
	for (int i = 0; i < allmesh.size(); ++i) {
		for (int j = i; j < allmesh.size(); ++j) {
			double guessedAngle = allmesh.at(i).getDihedralAngle(allmesh.at(j));
			if (guessedAngle < 0) {
				guessedAngle += 180.0;
			}
			if (!isnan(guessedAngle) && guessedAngle > 0.0 && guessedAngle < 180.0 && guessedAngle > threshold) {
				std::vector<Point> common = allmesh.at(i).getCommonPoints(allmesh.at(j));
				if (common.size() >= 2) {
					DrawLine(common[1]-common[0], common[0], 1.0, 0.0, 0.0, 5.0);
				}
			}
		}
	}
}
