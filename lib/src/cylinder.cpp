#include "../headers/cylinder.h"

#include "../headers/face.h"

#include <cmath>
#include <math.h>
#include <iostream>
#include <fstream>
#include <GL/glut.h>

Cylinder::Cylinder() {}

Cylinder::Cylinder(unsigned int nbFac) {
	m_nbFacettes = nbFac;
	m_height = 10;
	m_rayon = 5;
	generateFacettes();
}

Cylinder::Cylinder(unsigned int nbFac, int r, int h) {
	m_nbFacettes = nbFac;
	m_height = h;
	m_rayon = r;
	generateFacettes();
}

void Cylinder::generateFacettes() {
	m_faceDown.clear();
	m_faceUp.clear();

	double baseline = (2.0 * M_PI) / (double)m_nbFacettes;
	// generation des points
	for (unsigned int i = 0; i < m_nbFacettes; ++i) {
		m_faceUp.push_back(Point(std::cos(i*baseline) * m_rayon, m_height, std::sin(i*baseline) * m_rayon));
		m_faceDown.push_back(Point(std::cos(i*baseline) * m_rayon, 0.0, std::sin(i*baseline) * m_rayon));
	}

	// calcul des normales des points :
	for (int i = 0; i < m_nbFacettes; ++i) {
		/** Pour chaque point i , il possède 3 faces connectées :
		 *  - la face (i, i+1, i_down)
		 *  - la face (i-1_down, i, i_down)
		 *  - la face (i-1, i, i-1_down)
		 */
		int lastIndex = (i == 0) ? m_nbFacettes - 1 : i - 1;
		int nextIndex = (i == m_nbFacettes - 1) ? 0 : i + 1;
		Vector next_face_up_normal = Vector(m_faceUp[i], m_faceUp[nextIndex]).vectoriel(Vector(m_faceDown[i], m_faceUp[i]));
		Vector last_face_up_normal = Vector(m_faceUp[lastIndex], m_faceUp[i]).vectoriel(Vector(m_faceDown[lastIndex], m_faceUp[lastIndex]));
		Vector last_face_down_normal = Vector(m_faceDown[lastIndex], m_faceDown[i]).vectoriel(Vector(m_faceDown[i],m_faceUp[i]));
		m_faceUp[i].setNormal((next_face_up_normal + last_face_down_normal + last_face_up_normal + Vector(0.0,1.0,0.0)).normalize());
		m_faceDown[i].setNormal((next_face_up_normal + last_face_down_normal + last_face_up_normal + Vector(0.0,-1.0,0.0)).normalize());
	}
}

void Cylinder::draw() const {
	for (int i = 0; i < m_nbFacettes-1; ++i) {
		glVertex3d(m_faceUp[i+0].getX(), m_faceUp[i+0].getY(), m_faceUp[i+0].getZ());
		glVertex3d(m_faceDown[i+0].getX(), m_faceDown[i+0].getY(), m_faceDown[i+0].getZ());
		glVertex3d(m_faceDown[i+1].getX(), m_faceDown[i+1].getY(), m_faceDown[i+1].getZ());
		glVertex3d(m_faceUp[i+1].getX(), m_faceUp[i+1].getY(), m_faceUp[i+1].getZ());
	}
	int lastIndex = m_faceUp.size()-1;
	glVertex3d(m_faceUp[lastIndex].getX(), m_faceUp[lastIndex].getY(), m_faceUp[lastIndex].getZ());
	glVertex3d(m_faceDown[lastIndex].getX(), m_faceDown[lastIndex].getY(), m_faceDown[lastIndex].getZ());
	glVertex3d(m_faceDown[0].getX(), m_faceDown[0].getY(), m_faceDown[0].getZ());
	glVertex3d(m_faceUp[0].getX(), m_faceUp[0].getY(), m_faceUp[0].getZ());
}

void Cylinder::drawAlone() const {
	glBegin(GL_QUADS);
		this->draw();
	glEnd();
}

void Cylinder::addFacettes() {
	this->m_nbFacettes++;
	generateFacettes();
}

void Cylinder::removeFacettes() {
	this->m_nbFacettes--;
	generateFacettes();
}

double* Cylinder::getTriangleMesh() const {
	// Parcourir tous les points :
	double * vertexArray = new double[m_nbFacettes * 3 * 2 + 6];
	for(int i = 0; i < m_nbFacettes; ++i) {
		double* currentVertex = Point(m_faceUp[i]).toArray();
		vertexArray[3*i+0] = currentVertex[0];
		vertexArray[3*i+1] = currentVertex[1];
		vertexArray[3*i+2] = currentVertex[2];
	}
	for(int i = 0; i < m_nbFacettes; ++i) {
		double* currentVertex = Point(m_faceDown[i]).toArray();
		vertexArray[m_nbFacettes*3 + 3*i+0] = currentVertex[0];
		vertexArray[m_nbFacettes*3 + 3*i+1] = currentVertex[1];
		vertexArray[m_nbFacettes*3 + 3*i+2] = currentVertex[2];
	}

	vertexArray[m_nbFacettes * 3 * 2 + 0] = 0.0;
	vertexArray[m_nbFacettes * 3 * 2 + 1] = m_height;
	vertexArray[m_nbFacettes * 3 * 2 + 2] = 0.0;
	vertexArray[m_nbFacettes * 3 * 2 + 3] = 0.0;
	vertexArray[m_nbFacettes * 3 * 2 + 4] = 0.0;
	vertexArray[m_nbFacettes * 3 * 2 + 5] = 0.0;
	return vertexArray;
}

unsigned int Cylinder::getNbVertexForMesh() const {
	return m_nbFacettes * 3 * 2 * 2;
}

double* Cylinder::getNormals() const {
	double* normalArray = new double[m_nbFacettes * 3 * 2 + 6];
	for(int i = 0; i < m_nbFacettes; ++i) {
		int j = i+1;
		if (i == m_nbFacettes-1) j = 0;
		double* currentNormal = Point(m_faceUp[i]).toArray();
		normalArray[3*i+0] = currentNormal[0];
		normalArray[3*i+1] = currentNormal[1];
		normalArray[3*i+2] = currentNormal[2];
	}
	for(int i = 0; i < m_nbFacettes; ++i) {
		int j = i+1;
		if (i == 0) j = m_nbFacettes-1;
		double* currentNormal = Point(m_faceDown[i]).toArray();
		normalArray[m_nbFacettes*3 + 3*i+0] = currentNormal[0];
		normalArray[m_nbFacettes*3 + 3*i+1] = currentNormal[1];
		normalArray[m_nbFacettes*3 + 3*i+2] = currentNormal[2];
	}

	normalArray[m_nbFacettes * 3 * 2 + 0] = 0.0;
	normalArray[m_nbFacettes * 3 * 2 + 1] = 0.0;
	normalArray[m_nbFacettes * 3 * 2 + 2] = 1.0;
	normalArray[m_nbFacettes * 3 * 2 + 3] = 0.0;
	normalArray[m_nbFacettes * 3 * 2 + 4] = 0.0;
	normalArray[m_nbFacettes * 3 * 2 + 5] =-1.0;
	return normalArray;
}

double Cylinder::getLowerBoundary() const {
	return 0.0;
}

double Cylinder::getHigherBoundary() const {
	return (m_height > m_rayon*2) ? m_height : m_rayon*2;
}

unsigned int* Cylinder::getFaceIndex() const {
	// number of faces : m_nbFacettes * 2
	// times 3 for three point coordinates : m_nbFacettes * 2 * 3
	unsigned int * vertexArray = new unsigned int[m_nbFacettes * 3 * 2 * 2];
	// ------------------
	// Meshes the outside
	// ------------------
	for (int i = 0; i < m_nbFacettes; ++i) {
		int j = (i == m_nbFacettes-1) ? 0 : i+1;
		vertexArray[3*i+0] = j;
		vertexArray[3*i+1] = m_nbFacettes+i;
		vertexArray[3*i+2] = i;
	}
	for (int i = 0; i < m_nbFacettes; ++i) {
		int j = (i == 0) ? m_nbFacettes-1 : i-1;
		vertexArray[3*m_nbFacettes + 3*i+0] = m_nbFacettes + j;
		vertexArray[3*m_nbFacettes + 3*i+1] = i;
		vertexArray[3*m_nbFacettes + 3*i+2] = m_nbFacettes + i;
	}
	// ---------------------
	// Meshes the top/bottom
	// ---------------------
	unsigned int topPoint = m_nbFacettes * 2;
	unsigned int bottomPoint = m_nbFacettes * 2+1;
	// top first :
	for (int i = 0; i < m_nbFacettes; ++i) {
		unsigned int j = (i == m_nbFacettes-1) ? 0 : i+1;
		vertexArray[m_nbFacettes*6 + 3*i + 0] = i;
		vertexArray[m_nbFacettes*6 + 3*i + 1] = topPoint;
		vertexArray[m_nbFacettes*6 + 3*i + 2] = j;
	}
	for(int i = 0; i < m_nbFacettes; ++i) {
		unsigned int k = m_nbFacettes + i;
		unsigned int j = (i == m_nbFacettes-1) ? m_nbFacettes : k+1;
		vertexArray[m_nbFacettes*6 + m_nbFacettes*3 + 3*i + 0] = j;
		vertexArray[m_nbFacettes*6 + m_nbFacettes*3 + 3*i + 1] = bottomPoint;
		vertexArray[m_nbFacettes*6 + m_nbFacettes*3 + 3*i + 2] = k;
	}
	return vertexArray;
}

void Cylinder::drawElements() const {
	double* v = getTriangleMesh();
	double* n = getNormals();
	unsigned int* f= getFaceIndex();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_DOUBLE, 0, v);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_DOUBLE, 0, n);

	glColor3d(0.0,0.0,0.5);

	glDrawElements(GL_TRIANGLES, getNbVertexForMesh(), GL_UNSIGNED_INT, f);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void Cylinder::drawDihedralAngles(double threshold) const {
	// get mesh :
	unsigned int* f = getFaceIndex();
	// create vector of al points as needed by getFaceIndex()
	double* allpoints = getTriangleMesh();
	// convert all that into triangles for meshing :
	std::vector<Face> allfaces;
	for (int i = 0; i < m_nbFacettes * 4; ++i) {
		allfaces.push_back(Face(f[3*i+0], f[3*i+1], f[3*i+2]));
	}
	std::vector<meshFace> allmesh;
	for (int i = 0; i < allfaces.size(); ++i) {
		allmesh.push_back(allfaces.at(i).toMeshFace(allpoints));
	}

	// now for evey one of them see if they are below threshold
	for (int i = 0; i < allmesh.size(); ++i) {
		for (int j = i; j < allmesh.size(); ++j) {
			double guessedAngle = allmesh.at(i).getDihedralAngle(allmesh.at(j));
			if (guessedAngle < 0) {
				guessedAngle += 180.0;
			}
			if (!isnan(guessedAngle) && guessedAngle > 0.0 && guessedAngle < 180.0 && guessedAngle > threshold) {
				std::vector<Point> common = allmesh.at(i).getCommonPoints(allmesh.at(j));
				if (common.size() >= 2) {
					DrawLine(common[1]-common[0], common[0], 1.0, 0.0, 0.0, 5.0);
				}
			}
		}
	}
}

void Cylinder::toOffFile(const std::string& pathName) const {
	std::ofstream out(pathName);
	out << "OFF" << std::endl;
	out << this->m_faceUp.size() + this->m_faceDown.size() << " " << this->m_nbFacettes*3 << " 0" << std::endl;
	for (unsigned int i = 0; i < this->m_faceUp.size(); ++i) {
		out << this->m_faceUp[i].getX() << " " << this->m_faceUp[i].getY() << " " << this->m_faceUp[i].getZ() << std::endl;
	}
	for (unsigned int i = 0; i < this->m_faceDown.size(); ++i) {
		out << this->m_faceDown[i].getX() << " " << this->m_faceDown[i].getY() << " " << this->m_faceDown[i].getZ() << std::endl;
	}
	unsigned int* faces = getFaceIndex();
	for (unsigned int i = 0; i < this->m_nbFacettes * 4; ++i) {
		out << faces[3*i + 0] << " " << faces[3*i + 1] << " " << faces[3*i + 2] << std::endl;
	}
	out.close();
}
