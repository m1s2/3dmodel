#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <GL/glut.h>
#include "../headers/drawings.h"

GLvoid DrawLine(Vector v) {
	glColor3f(0,0,0);	// Donne couleur noire
	glLineWidth(2);		// Donne largeur de crayon
	glBegin(GL_LINES);	// dessine
		glVertex3f(0.0,0.0,0.0);				// debut ligne
		glVertex3f(v.getX(), v.getY(), v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawLine(Vector v, Point p) {
	glColor3f(0,0,0);	// Donne couleur noire
	glLineWidth(2);		// Donne largeur de crayon

	glBegin(GL_LINES);	// dessine
		glVertex3f(p.getX(), p.getY(), p.getZ());	// debut ligne
		glVertex3f(p.getX()+v.getX(), p.getY()+v.getY(), p.getZ()+v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawLine(Vector v, Point p, GLdouble r, GLdouble g, GLdouble b, GLdouble size) {
	glColor3d(r,g,b);	// Donne couleur noire
	glLineWidth(size);		// Donne largeur de crayon

	glBegin(GL_LINES);	// dessine
		glVertex3f(p.getX(), p.getY(), p.getZ());	// debut ligne
		glVertex3f(p.getX()+v.getX(), p.getY()+v.getY(), p.getZ()+v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawPoint(Point p) {
	glPointSize(10);
	glColor3f(1,0,0.5);
	glBegin(GL_POINTS);
		glVertex3f(p.getX(), p.getY(), p.getZ());
	glEnd();
}

GLvoid DrawPoint(Point p, double a, double b, double c) {
	glPointSize(2);
	glColor3f(a,b,c);
	glBegin(GL_POINTS);
		glVertex3f(p.getX(), p.getY(), p.getZ());
	glEnd();
}

GLvoid DrawCurve(const Point* pointsOfCurve, long nbPoints) {
	// Stores current X,Y,Z point information
	float px, py, pz;

	// OpenGL drawing parameters
	glPointSize(2);
	glLineWidth(1);
	glColor3f(0.8f,0.f,0.f);
	// OpenGL render loop
	glBegin(GL_LINE_STRIP);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve[i].getX();
		py = pointsOfCurve[i].getY();
		pz = pointsOfCurve[i].getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
}

GLvoid DrawCurve(const Point* pointsOfCurve, const long nbPoints, double a, double b, double c) {
	// Stores current X,Y,Z point information
	float px, py, pz;

	// OpenGL drawing parameters
	glLineWidth(1);
	glColor3f(a,b,c);
	// OpenGL render loop
	glBegin(GL_LINE_STRIP);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve[i].getX();
		py = pointsOfCurve[i].getY();
		pz = pointsOfCurve[i].getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
	glPointSize(3);
	glBegin(GL_POINTS);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve[i].getX();
		py = pointsOfCurve[i].getY();
		pz = pointsOfCurve[i].getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
}

GLvoid DrawCurve(const Point* pointsOfCurve, const Point endPoint, long nbPoints) {
	// Stores current X,Y,Z point information
	float px, py, pz;

	// OpenGL drawing parameters
	glPointSize(2);
	glLineWidth(1);
	glColor3f(0.8f,0.f,0.f);
	// OpenGL render loop
	glBegin(GL_LINE_STRIP);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve[i].getX();
		py = pointsOfCurve[i].getY();
		pz = pointsOfCurve[i].getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
	glColor3f(1.f, 0.f, 1.f);
	glBegin(GL_LINES);
		glVertex3f(pointsOfCurve[nbPoints-1].getX(), pointsOfCurve[nbPoints-1].getY(), pointsOfCurve[nbPoints-1].getZ());
		glVertex3f(endPoint.getX(), endPoint.getY(), endPoint.getZ());
	glEnd();
}

GLvoid DrawCurve(std::vector<Point> pointsOfCurve, long nbPoints, int index) {
	// Stores current X,Y,Z point information
	float px, py, pz;

	// OpenGL drawing parameters
	glPointSize(2);
	glLineWidth(1);
	glColor3f(1.f,0.f,1.f);
	// OpenGL render loop
	glBegin(GL_LINE_STRIP);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve.at(i).getX();
		py = pointsOfCurve.at(i).getY();
		pz = pointsOfCurve.at(i).getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
}

GLvoid DrawGrid(double lowerX, double lowerY, double higherX, double higherY) {
	// Gets the nearest values to draw the grid from :
	double lx = ceil(lowerX);
	double ly = ceil(lowerY);
	double hx = floor(higherX);
	double hy = floor(higherY);

	// Determines how many points to draw and allocates memory:
	int nbPoints = ((int)hx - (int)lx) * ((int)hy - (int)ly);
	Point* gridPoints = new Point[nbPoints];

	int height = (int)hx - (int)lx;
	int width = (int)hy - (int)ly;
	// Creates points to draw :
	for (int x = 0; x < height; ++x) {
		for (int y = 0; y < width; ++y) {
			gridPoints[x*width+y] = Point(x+(int)lx,y+(int)ly,0);
		}
	}

	// Draws the points :
	glColor3f(1.0,1.0,1.0);
	glPointSize(1.0);

	GLdouble x,y,z;

	glBegin(GL_POINTS);
		for(int i = 0; i < nbPoints; ++i) {
			x = gridPoints[i].getX();
			y = gridPoints[i].getY();
			z = gridPoints[i].getZ();
			glVertex3f(x,y,z);
		}
	glEnd();
}

GLvoid DrawPointFrame(Point** points, double x, double y) {
	glPointSize(2);
	glLineWidth(0.5);
	// Color : blue
	glColor3f(0.0,0.0,1.0);
	glBegin(GL_POINTS);
		for (int i = 0; i < x; ++i)
		for (int j = 0; j < y; ++j)
		glVertex3d(points[i][j].getX(),points[i][j].getY(),points[i][j].getZ());
	glEnd();
}

GLvoid DrawCylinder(Point** surface, double x) {
	// Sets the drawing model to wireframe
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);
	// glColor3f(0.8,0.8,0.8);
	glBegin(GL_QUADS);
		for (int i = 0; i < x-1; ++i) {
			// printf();
			glColor3f((double)i/(double)(x-1), 0.5, 0.5);
			glVertex3d(surface[i][0].getX(), surface[i][0].getY(), surface[i][0].getZ());
			glVertex3d(surface[i+1][0].getX(), surface[i+1][0].getY(), surface[i+1][0].getZ());
			glVertex3d(surface[i+1][1].getX(), surface[i+1][1].getY(), surface[i+1][1].getZ());
			glVertex3d(surface[i][1].getX(), surface[i][1].getY(), surface[i][1].getZ());
		}
	glEnd();
	glPointSize(5);
	glBegin(GL_POINTS);
		glColor3f(1.0,0.0,0.0);
		for (int i = 0; i < x; ++i) {
			glVertex3d(surface[i][0].getX(), surface[i][0].getY(), surface[i][0].getZ());
			glVertex3d(surface[i][1].getX(), surface[i][1].getY(), surface[i][1].getZ());
		}
	glEnd();
}

GLvoid DrawCone(Point sommet, Point* base, double x) {
	// Sets the drawing model to wireframe
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);
	glColor3f(0.8,0.8,0.8);
	glBegin(GL_TRIANGLE_FAN);
		glVertex3d(sommet.getX(), sommet.getY(), sommet.getZ());
		for (int i = 0; i < x; ++i) {
			glVertex3d(base[i].getX(), base[i].getY(), base[i].getZ());
		}
	glEnd();
	glPointSize(5);
	glBegin(GL_POINTS);
		glColor3f(1.0,0.0,0.0);
		glVertex3d(sommet.getX(), sommet.getY(), sommet.getZ());
		for (int i = 0; i < x; ++i) {
			glVertex3d(base[i].getX(), base[i].getY(), base[i].getZ());			
		}
	glEnd();
}

GLvoid DrawSphere(Point poleNord, Point poleSud, Point** surface, int nbMeridiens, int nbParalleles) {
	glPolygonMode( GL_FRONT, GL_LINE );
	glColor3f(0.8,0.8,0.8);

	// Dessine les liens au pole nord
	glBegin(GL_TRIANGLE_FAN);
		glVertex3d(poleNord.getX(), poleNord.getY(), poleNord.getZ());
		for (int i = 0; i < nbMeridiens; ++i) {
			glVertex3d(surface[0][i].getX(), surface[0][i].getY(), surface[0][i].getZ());
		}
		glVertex3d(surface[0][nbMeridiens-1].getX(), surface[0][nbParalleles-1].getY(), surface[0][nbParalleles-1].getZ());
	glEnd();

	// Dessine le 'corps' de la sphère
	glBegin(GL_QUADS);
		for (int i = 0; i < nbParalleles-1; ++i) {
			for (int j = 0; j < nbMeridiens-1; ++j) {
				glVertex3d(surface[i+0][j+0].getX(), surface[i+0][j+0].getY(), surface[i+0][j+0].getZ());
				glVertex3d(surface[i+1][j+0].getX(), surface[i+1][j+0].getY(), surface[i+1][j+0].getZ());
				glVertex3d(surface[i+1][j+1].getX(), surface[i+1][j+1].getY(), surface[i+1][j+1].getZ());
				glVertex3d(surface[i+0][j+1].getX(), surface[i+0][j+1].getY(), surface[i+0][j+1].getZ());
			}
		}
	glEnd();

	// Dessine les liens au pole sud
	glBegin(GL_TRIANGLE_FAN);
		glVertex3d(poleSud.getX(), poleSud.getY(), poleSud.getZ());
		for (int i = 0; i < nbMeridiens; ++i) {
			glVertex3d(surface[nbParalleles-1][i].getX(), surface[nbParalleles-1][i].getY(), surface[nbParalleles-1][i].getZ());
		}
		glVertex3d(surface[nbParalleles-1][0].getX(), surface[nbParalleles-1][0].getY(), surface[nbParalleles-1][0].getZ());
	glEnd();
}

GLvoid DrawSphereTop(Point poleNord, Point poleSud, Point** surface, int nbMeridiens, int nbParalleles) {
	glColor3f(0.8,0.8,0.8);

	// Dessine les liens au pole nord
	glBegin(GL_TRIANGLE_FAN);
		glVertex3d(poleNord.getX(), poleNord.getY(), poleNord.getZ());
		for (int i = 0; i < nbParalleles; ++i) {
			glVertex3d(surface[i][0].getX(), surface[i][0].getY(), surface[i][0].getZ());
		}
		glVertex3d(surface[nbParalleles-1][0].getX(), surface[nbParalleles-1][0].getY(), surface[nbParalleles-1][0].getZ());
	glEnd();
}

GLvoid DrawSphereWireFrame(Point poleNord, Point poleSud, Point** points, double x, double y) {
	glColor3f(0.9,0.4,0.1);

	glLineWidth(2.0);

	// Lignes horizontales :
	for (int i = 0; i < x; ++i) {
		glBegin(GL_LINE_STRIP);
			for (int j = 0; j < y; ++j)
				glVertex3d(points[i][j].getX(), points[i][j].getY(), points[i][j].getZ());
		glEnd();
	}
	// Lignes verticales :
	for (int j = 0; j < y; ++j) {
		glBegin(GL_LINE_STRIP);
			glVertex3f(poleNord.getX(),poleNord.getY(),poleNord.getZ());
			for (int i = 0; i < x; ++i) 
				glVertex3d(points[i][j].getX(), points[i][j].getY(), points[i][j].getZ());
			glVertex3f(poleSud.getX(),poleSud.getY(),poleSud.getZ());
		glEnd();
	}
}
