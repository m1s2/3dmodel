#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <GL/glut.h>

#include "../headers/cube.h"
#include "../headers/Point.h"
#include "../headers/Vector.h"
#include "../headers/definitions.h"

Cube::Cube() {
	Point origin = Point(0.0,0.0,0.0);
	double edgeWidth = 1.0;
	
	std::array<Point, 4> upFace;
	upFace[0] = Point(origin.getX()+edgeWidth, origin.getY()+edgeWidth, origin.getZ()+edgeWidth);
	upFace[1] = Point(origin.getX()+edgeWidth, origin.getY()+edgeWidth, origin.getZ()-edgeWidth);
	upFace[2] = Point(origin.getX()-edgeWidth, origin.getY()+edgeWidth, origin.getZ()-edgeWidth);
	upFace[3] = Point(origin.getX()-edgeWidth, origin.getY()+edgeWidth, origin.getZ()+edgeWidth);

	std::array<Point, 4> downFace;
	downFace[0] = Point(origin.getX()+edgeWidth, origin.getY()-edgeWidth, origin.getZ()+edgeWidth);
	downFace[1] = Point(origin.getX()+edgeWidth, origin.getY()-edgeWidth, origin.getZ()-edgeWidth);
	downFace[2] = Point(origin.getX()-edgeWidth, origin.getY()-edgeWidth, origin.getZ()-edgeWidth);
	downFace[3] = Point(origin.getX()-edgeWidth, origin.getY()-edgeWidth, origin.getZ()+edgeWidth);

	_points[0] = upFace;
	_points[1] = downFace;
}

void Cube::draw() const {
	// glColor3f(1.0,0.5,0.5);
	// glBegin(GL_QUADS);
	// 	// Draw top face :
	// 	glNormal3f(normals[0].getX(), normals[0].getY(), normals[0].getZ());
	// 	glVertex3f(vertices[0].getX(), vertices[0].getY(), vertices[0].getZ());
	// 	glVertex3f(vertices[1].getX(), vertices[1].getY(), vertices[1].getZ());
	// 	glVertex3f(vertices[2].getX(), vertices[2].getY(), vertices[2].getZ());
	// 	glVertex3f(vertices[3].getX(), vertices[3].getY(), vertices[3].getZ());
	// 	// Draw Front face :
	// 	glNormal3f(normals[2].getX(), normals[2].getY(), normals[2].getZ());
	// 	glVertex3f(vertices[7].getX(), vertices[7].getY(), vertices[7].getZ());
	// 	glVertex3f(vertices[4].getX(), vertices[4].getY(), vertices[4].getZ());
	// 	glVertex3f(vertices[0].getX(), vertices[0].getY(), vertices[0].getZ());
	// 	glVertex3f(vertices[3].getX(), vertices[3].getY(), vertices[3].getZ());
	// 	// Draw left face :
	// 	glNormal3f(normals[4].getX(), normals[4].getY(), normals[4].getZ());
	// 	glVertex3f(vertices[6].getX(), vertices[6].getY(), vertices[6].getZ());
	// 	glVertex3f(vertices[7].getX(), vertices[7].getY(), vertices[7].getZ());
	// 	glVertex3f(vertices[3].getX(), vertices[3].getY(), vertices[3].getZ());
	// 	glVertex3f(vertices[2].getX(), vertices[2].getY(), vertices[2].getZ());
	// 	// Draw back face :
	// 	glNormal3f(normals[3].getX(), normals[3].getY(), normals[3].getZ());
	// 	glVertex3f(vertices[2].getX(), vertices[2].getY(), vertices[2].getZ());
	// 	glVertex3f(vertices[1].getX(), vertices[1].getY(), vertices[1].getZ());
	// 	glVertex3f(vertices[5].getX(), vertices[5].getY(), vertices[5].getZ());
	// 	glVertex3f(vertices[6].getX(), vertices[6].getY(), vertices[6].getZ());
	// 	// Draw right face :
	// 	glNormal3f(normals[5].getX(), normals[5].getY(), normals[5].getZ());
	// 	glVertex3f(vertices[1].getX(), vertices[1].getY(), vertices[1].getZ());
	// 	glVertex3f(vertices[0].getX(), vertices[0].getY(), vertices[0].getZ());
	// 	glVertex3f(vertices[4].getX(), vertices[4].getY(), vertices[4].getZ());
	// 	glVertex3f(vertices[5].getX(), vertices[5].getY(), vertices[5].getZ());
	// 	// Draw bottom face
	// 	glNormal3f(normals[1].getX(), normals[1].getY(), normals[1].getZ());
	// 	glVertex3f(vertices[7].getX(), vertices[7].getY(), vertices[7].getZ());
	// 	glVertex3f(vertices[6].getX(), vertices[6].getY(), vertices[6].getZ());
	// 	glVertex3f(vertices[5].getX(), vertices[5].getY(), vertices[5].getZ());
	// 	glVertex3f(vertices[4].getX(), vertices[4].getY(), vertices[4].getZ());
	// glEnd();
}

void Cube::drawCube(bool drawLines) const {
	std::array<Point, 4> faceUp = _points[0];
	std::array<Point, 4> faceDown = _points[1];
	glColor3d(0.5,1.0,0.5);
	// Dessins des faces des côtés
	for(unsigned int i(0); i < 4; ++i) {
		Point point0(faceUp[i]);
		Point point1(faceDown[i]);
		Point point2(faceDown[(i+1)%4]);
		Point point3(faceUp[(i+1)%4]);

		// Calcul de la normale
		Point point4(faceDown[(i+2)%4]); //point de l'arête suivante

		Vector normal(point4, point2);
		normal.normalize();
		glNormal3f(normal.getX(), normal.getY(), normal.getZ());

		glBegin(GL_QUADS);

			point0.doGlVertex3f();
			point1.doGlVertex3f();
			point2.doGlVertex3f();
			point3.doGlVertex3f();

		glEnd();
	}

	Vector normal(faceDown[0], faceUp[0]);
	normal.normalize();
	glNormal3f(normal.getX(), normal.getY(), normal.getZ());

	// Dessin des faces supérieures et inférieures
	glBegin(GL_QUADS);

		for(const Point& p: faceUp) {
			p.doGlVertex3f();
		}

	glEnd();

	normal = Vector(faceUp[0], faceDown[0]);
	normal.normalize();
	glNormal3f(normal.getX(), normal.getY(), normal.getZ());

	glBegin(GL_QUADS);

		for(int i = faceDown.size()-1; i >= 0; i--) {
			faceDown[i].doGlVertex3f();
		}

	glEnd();

	// Dessin des lignes
	if(drawLines) {
		glColor3d(1.0,0.0,0.0);
		glBegin(GL_LINE_STRIP);
			for(unsigned int i(0); i < 3; ++i) {
				Point point0(faceUp[i]);
				Point point1(faceDown[i]);
				Point point2(faceDown[(i+1)%4]);
				Point point3(faceUp[(i+1)%4]);

				point0.doGlVertex3f();
				point1.doGlVertex3f();
				point2.doGlVertex3f();
				point3.doGlVertex3f();
			}
		glEnd();

		for(const std::array<Point, 4>& face: _points) {
			glBegin(GL_LINE_STRIP);
				for(const Point& p: face) {
					p.doGlVertex3f();
				}
			glEnd();
		}
	}
}

void Cube::translate(const Vector& v) {
	for(std::array<Point,4>& face : _points) {
		for (Point point: face) {
			point.translate(v);
		}
	}
}