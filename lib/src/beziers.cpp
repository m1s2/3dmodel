#include <stdio.h>
#include <math.h>
#include <vector>
#include "../headers/Point.h"
#include "../headers/Vector.h"
#include "../headers/beziers.h"
#include "../headers/drawings.h"
#include "../headers/definitions.h"

void courbeBezier_Bernstein(Point* pointsControles, long nbPC, long nbU, Point* resultat) {
	// Si on ne peux pas créer la courbe (pas assez de points demandés, ou trop de
	// point de contrôle), on affiche un message d'erreur et on arrête le programme :
	if (nbPC > nbU) {
		printf("Could not compute : not enough points on the curve.\n");
		exit(1);
	}
	
	// On réalloue le tableau de points de la courbe, juste au cas où :
	delete resultat;
	resultat = new Point[nbU];

	// Pour tout les 'U' sur la courbe
	for (int j = 0; j < nbU; ++j) {
		// On calcule la valeur de U sur la courbe
		double u = (double)j * ((double)1.0 / (double)(nbU-1));
		// Les coordonnées du point avec valeur 'U' 
		// seront stockées ici :
		double px = 0;
		double py = 0;
		double pz = 0;

		// Donne le degré de la courbe, nommé upperBound parce que ce sera
		// la borne haute de la boucle ci-dessous :
		double upperBound = (double)nbPC;
		double bernstein;
		
		// On calcule les coordonnées x,y,z avec les polynomes : 
		for (int i = 0; i < (int)upperBound; ++i) {
			px += coefBernstein(upperBound-1.0, (double)i, u) * pointsControles[i].getX();
			py += coefBernstein(upperBound-1.0, (double)i, u) * pointsControles[i].getY();
			pz += coefBernstein(upperBound-1.0, (double)i, u) * pointsControles[i].getZ();
		}

		// On stocke la valeur dans le tableau représentant la courbe :
		resultat[j] = Point(px,py,pz);
	}

	return;
}

void surfaceBezier_Bernstein(Point** pointsControles, long nbPCx, long nbPCy, long nbU, long nbV, Point** resultat) {
	// delete resultat;

	// resultat = new Point*[(int)nbU];
	// for (int i = 0; i < nbU; ++i)
	// 	resultat[i] = new Point[(int)nbV];

	// Pour tous les points de la surface : 
	for (int u = 0; u < nbU; ++u) {
		for (int v = 0; v < nbV; ++v) {
			// Calculer les coordonnées du point :
			double px = 0.0;
			double py = 0.0;
			double pz = 0.0;

			// On calcule les coordonnées du point P(u,v)
			// avec les coefs de Bernstein
			for (int i = 0; i < nbPCx; ++i) {
				for (int j = 0; j < nbPCy; ++j) {
					px += ( coefBernstein((double)nbPCx-1.0, i, u * ((double)1.0 / (double)(nbU-1))) *
						coefBernstein((double)nbPCy-1.0, j, v * ((double)1.0 / (double)(nbV-1))) *
						pointsControles[i][j].getX() );
					py += ( coefBernstein((double)nbPCx-1.0, i, u * ((double)1.0 / (double)(nbU-1))) *
						coefBernstein((double)nbPCy-1.0, j, v * ((double)1.0 / (double)(nbV-1))) *
						pointsControles[i][j].getY() );
					pz += ( coefBernstein((double)nbPCx-1.0, i, u * ((double)1.0 / (double)(nbU-1))) *
						coefBernstein((double)nbPCy-1.0, j, v * ((double)1.0 / (double)(nbV-1))) *
						pointsControles[i][j].getZ() );
				}
			}

			resultat[u][v] = Point(px,py,pz);
		}
	}

	return;
}

void courbeBeziersCastelnau(Point* pointsControles, long nbPC, long nbU, Point* resultat) {
	// Ré-alloue la tableau de points résultants de la courbe
	// de Béziers, juste au cas où :
	delete resultat;
	resultat = new Point[(int)nbU];

	// Pour tous les 'nbU' points de la courbe, on calcule la valeur de 'U'
	// et ensuite, on extrapole les coordonnées du point donnée par 'U' :
	for (int i = 0; i < nbU; ++i)  {
		double u = (double)i * ((double)1.0 / (double)(nbU-1));
		resultat[i] = casteljauRecursif(u, pointsControles, nbPC);
	}
}

void courbeBeziersCastelnau(Point* pointsControles, long nbPC, long nbU, Point* resultat, std::vector<std::vector<Point>> &construction) {
	// Ré-alloue la tableau de points résultants de la courbe
	// de Béziers, juste au cas où :
	delete resultat;
	resultat = new Point[(int)nbU];

	// Pour tous les 'nbU' points de la courbe, on calcule la valeur de 'U'
	// et ensuite, on extrapole les coordonnées du point donnée par 'U' :
	for (int i = 0; i < nbU; ++i)  {
		double u = (double)i * ((double)1.0 / (double)(nbU-1));
		resultat[i] = casteljauRecursif(u, pointsControles, nbPC, construction);
	}
}

Point casteljauRecursif(double u, Point* ptsCtrl, long nbPC) {
	// Si on a plus que deux points de controle, on peut calculer le point
	// résultant de l'algorithme de Casteljau pour la valeur de U demandée
	// et on le retourne, passant à u+1 :
	if (nbPC == 2) {
		return Point(	u * ptsCtrl[0].getX() + (1-u) * ptsCtrl[1].getX(),
				u * ptsCtrl[0].getY() + (1-u) * ptsCtrl[1].getY(),
				u * ptsCtrl[0].getZ() + (1-u) * ptsCtrl[1].getZ());
	}

	// Sinon, on va calculer les n-1 points de contrôle correspondant à cette
	// étape récursive de l'algorithme de Casteljau.
	Point* ptsC = new Point[nbPC-1];
	for (int i = 0; i < nbPC-1; ++i) {
		ptsC[i] = Point(u * ptsCtrl[i].getX() + (1-u) * ptsCtrl[i+1].getX(),
				u * ptsCtrl[i].getY() + (1-u) * ptsCtrl[i+1].getY(),
				u * ptsCtrl[i].getZ() + (1-u) * ptsCtrl[i+1].getZ());
	}
	
	return casteljauRecursif(u, ptsC, nbPC-1);
}

Point casteljauRecursif(double u, Point* ptsCtrl, long nbPC, std::vector<std::vector<Point>> &construction) {
	// Si on a plus que deux points de controle, on peut calculer le point
	// résultant de l'algorithme de Casteljau pour la valeur de U demandée
	// et on le retourne, passant à u+1 :
	if (nbPC == 2) {
		return Point(	u * ptsCtrl[0].getX() + (1-u) * ptsCtrl[1].getX(),
				u * ptsCtrl[0].getY() + (1-u) * ptsCtrl[1].getY(),
				u * ptsCtrl[0].getZ() + (1-u) * ptsCtrl[1].getZ());
	}

	// Sinon, on va calculer les n-1 points de contrôle correspondant à cette
	// étape récursive de l'algorithme de Casteljau.
	Point* ptsC = new Point[nbPC-1];
	std::vector<Point> currentConstruction;
	for (int i = 0; i < nbPC-1; ++i) {
		ptsC[i] = Point(u * ptsCtrl[i].getX() + (1-u) * ptsCtrl[i+1].getX(),
				u * ptsCtrl[i].getY() + (1-u) * ptsCtrl[i+1].getY(),
				u * ptsCtrl[i].getZ() + (1-u) * ptsCtrl[i+1].getZ());
		if (CASTELJAU_CONSTRUCTION_ENABLED)
			currentConstruction.push_back(ptsC[i]);
	}
	
	// On ajoute les points de construction de cette étape de l'algorithme de
	// Casteljau pour l'afficher plus tard.
	if (CASTELJAU_CONSTRUCTION_ENABLED)
		construction.push_back(currentConstruction);

	return casteljauRecursif(u, ptsC, nbPC-1, construction);
}

double coefBernstein(double upperBound, double i, double u) {
	return (double)nCm(upperBound,(double)i) 		// i parmi N
		* pow(u,(double)i) 				// U exp i
		* pow((1.0 - u), (upperBound-(double)i));	// (1-u) ^ (n-i)
}

double nCm(double n, int i) {
	if (i == 0) return 1.0;
	double nom = fact(n);
	double denom = fact((double)i) * fact((n - (double)i));
	return nom / denom;
}

double fact(double n) { return factorial(std::floor(n)); }

double factorial(double n) { return ( n == 1.0 || n == 0.0 ) ? 1 : factorial(n-1) * n; }
