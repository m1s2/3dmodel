#include "../headers/volGrid.h"
#include <math.h>
#include <iostream>

volumetricGrid::volumetricGrid(const Point& _min, const Point& _max, const int _h, const int _w, const int _d) {
	this->min = Point(_min);
	this->max = Point(_max);
	this->h = _h;
	this->w = _w;
	this->d = _d;

	this->generateVoxels();
}

void volumetricGrid::generateVoxels() {
	/**
	 * From the point `min`, we extend a grid of voxels to 
	 * fill the space until reaching the final `max` point.
	 */

	double stepX = abs(max.x - min.x) / w;
	double stepY = abs(max.y - min.y) / h;
	double stepZ = abs(max.z - min.z) / d;

	Vector stepVoxel = Vector(stepX, stepY, stepZ);

	for (int i = 0; i < w; ++i) {
		for (int j = 0; j < h; ++j) {
			for (int k = 0; k < d; ++k) {
				Point minVoxelCurrent = Point(min.x + i * stepX, min.y + j * stepY, min.z + k * stepZ);
				Point maxVoxelCurrent = Point(min.x + (i+1) * stepX, min.y + (j+1) * stepY, min.z + (k+1) * stepZ);

				voxel currentVoxel = voxel(minVoxelCurrent, maxVoxelCurrent);

				/**
				 * The following lines compute the neighbors of the designated voxel.
				 * Each voxel can have a total of up to 26 neighbors (in 3D space using
				 * edge connexity).
				 * The computation starts by analysing the left-hand side of the voxel,
				 * starting from the lower-left quadrant, and working its way towards
				 * the top quadrant first, before going to the right.
				 */

				/**
				 * Computes the 9 neighbors on the left-hend side, if any.
				 */
				if (i > 0) {
					if (k > 0) {
						if (j > 0) {
							// Lower-left neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i-1) * h * d + (j-1) * w + (k-1) );
						}
						currentVoxel.neighbors.push_back( (i-1) * h * d + (j) * w + (k-1) );
						if (j < h-1) {
							// Top-left neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i-1) * h * d + (j+1) * w + (k-1) );
						}
					}
					// Bottom-center of the 9 left neighbors
					if ( j > 0 ) {
						currentVoxel.neighbors.push_back( (i-1) * h * d + (j-1) * w + (k) );
					}
					// Center of the 9 left neighbors :
					currentVoxel.neighbors.push_back( (i-1) * h * d + (j) * w + (k) );
					if ( j < h-1 ) {
						currentVoxel.neighbors.push_back( (i-1) * h * d + (j+1) * w + (k) );
					}
					if ( k < d-1 ) {
						if (j > 0) {
							// Lower-right neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i-1) * h * d + (j-1) * w + (k+1) );
						}
						currentVoxel.neighbors.push_back( (i-1) * h * d + (j) * w + (k+1) );
						if (j < h-1) {
							// Top-right neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i-1) * h * d + (j+1) * w + (k+1) );
						}
					}
				}

				/**
				 * Computes the neighbors in the 'ring' around the voxel :
				 */
				if (k > 0) {
					if (j > 0) {
						// Lower-left neighbor of the ring around the voxel.
						currentVoxel.neighbors.push_back( (i) * h * d + (j-1) * w + (k-1) );
					}
					currentVoxel.neighbors.push_back( (i) * h * d + (j) * w + (k-1) );
					if (j < h-1) {
						// Top-left neighbor of the ring around the voxel.
						currentVoxel.neighbors.push_back( (i) * h * d + (j+1) * w + (k-1) );
					}
				}
				// Bottom-center of the 9 center neighbors
				if ( j > 0 ) {
					currentVoxel.neighbors.push_back( (i) * h * d + (j-1) * w + (k) );
				}
				// Top-center of the 9 center neighbors
				if ( j < h-1 ) {
					currentVoxel.neighbors.push_back( (i) * h * d + (j+1) * w + (k) );
				}
				if ( k < d-1 ) {
					if (j > 0) {
						// Lower-right neighbor of the ring around the voxel.
						currentVoxel.neighbors.push_back( (i) * h * d + (j-1) * w + (k+1) );
					}
					currentVoxel.neighbors.push_back( (i) * h * d + (j) * w + (k+1) );
					if (j < h-1) {
						// Top-right neighbor of the ring around the voxel.
						currentVoxel.neighbors.push_back( (i) * h * d + (j+1) * w + (k+1) );
					}
				}

				/**
				 * Computes the neighbors on the right-hand side of the voxel.
				 */
				if (i < w-1) {
					if (k > 0) {
						if (j > 0) {
							// Lower-left neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i+1) * h * d + (j-1) * w + (k-1) );
						}
						currentVoxel.neighbors.push_back( (i+1) * h * d + (j) * w + (k-1) );
						if (j < h-1) {
							// Top-left neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i+1) * h * d + (j+1) * w + (k-1) );
						}
					}
					// Bottom-center of the 9 left neighbors
					if ( j > 0 ) {
						currentVoxel.neighbors.push_back( (i+1) * h * d + (j-1) * w + (k) );
					}
					// Center of the 9 left neighbors :
					currentVoxel.neighbors.push_back( (i+1) * h * d + (j) * w + (k) );
					if ( j < h-1 ) {
						currentVoxel.neighbors.push_back( (i+1) * h * d + (j+1) * w + (k) );
					}
					if ( k < d-1 ) {
						if (j > 0) {
							// Lower-right neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i+1) * h * d + (j-1) * w + (k+1) );
						}
						currentVoxel.neighbors.push_back( (i+1) * h * d + (j) * w + (k+1) );
						if (j < h-1) {
							// Top-right neighbor of the left-hand side of the 26 neighbors.
							currentVoxel.neighbors.push_back( (i+1) * h * d + (j+1) * w + (k+1) );
						}
					}
				}

				// Push back the voxel, including all its neighbors' indexes.
				voxels.push_back(currentVoxel);
			}
		}
	}
}

modelEdge& volumetricGrid::simplify(const modelEdge& model, volumetricGrid& grid) {
	/**
	 * Here, we will proceed as such : 
	 *   - compute an array of vertices from `model` that fit in the voxel
	 *       - the chosen points will be the first in the order they are arranged in, in the original model.
	 *       - for each voxel, all the other points contained within it will serve as a canvas to contruct links between voxels (like a neighbor list)
	 *       - for each edges in the original model, if there is one that links between the current voxel and one of its neighbors, create an edge.
	 */

	/**
	 * MAKE A FUNCTION INSIDE faceedge THAT RETURNS THE THREE INDEX OF POINTS, AND COMPUTE THEM AGAINST THE ORIGINAL MODEL, CHECK 2 OR 3 VERTICES INSIDE
	 * 2 -> EDGE
	 * 3 -> VERTEX
	 */

	std::vector<Point> new_vert;
	std::vector<Point> old_vert = model.getVertices();
	std::vector<edge> new_edges;
	std::vector<edge> old_edges = model.getEdges();
	std::vector<faceEdge> new_faces;
	unsigned int maxx = std::numeric_limits<unsigned int>::infinity();

	for (int i = 0; i < model.getNbFaces(); ++i) {
		//
		std::vector<unsigned int> faceIndexes = model.getVertexIndices(i);
		//

		// check which voxel the point is currently in :
		//
		unsigned int v1 = grid.getApproxVoxel(old_vert[faceIndexes[0]]);
		unsigned int v2 = grid.getApproxVoxel(old_vert[faceIndexes[1]]);
		unsigned int v3 = grid.getApproxVoxel(old_vert[faceIndexes[2]]);

		// if they are all in different voxels :
		if ( v1 != v2 && v2 != v3 && v3 != v1) {
			#ifdef EDGE_DETECT
			edge e1;
			edge e2;
			edge e3;
			#endif
			// do nothing
			if (!grid.voxels[v1].hasPoint()) {
				new_vert.push_back(old_vert[faceIndexes[0]]);
				#ifdef EDGE_DETECT
				e1.startPoint = new_vert.size() - 1;
				e3.endPoint = new_vert.size() - 1;
				#endif
				grid.voxels[v1].currentPoint = new_vert.size() - 1;
			} else {
				#ifdef EDGE_DETECT
				e1.startPoint = grid.voxels[v1].currentPoint;
				e3.endPoint = grid.voxels[v1].currentPoint;
				#endif
			}
			if (!grid.voxels[v2].hasPoint()) {
				new_vert.push_back(old_vert[faceIndexes[1]]);
				#ifdef EDGE_DETECT
				e1.endPoint = new_vert.size()-1;
				e2.startPoint = new_vert.size()-1;
				#endif
				grid.voxels[v2].currentPoint = new_vert.size()-1;
			} else {
				#ifdef EDGE_DETECT
				e1.endPoint = grid.voxels[v2].currentPoint;
				e2.startPoint = grid.voxels[v2].currentPoint;
				#endif
			}
			if (!grid.voxels[v3].hasPoint()) {
				new_vert.push_back(old_vert[faceIndexes[2]]);
				#ifdef EDGE_DETECT
				e2.endPoint = new_vert.size()-1;
				e3.startPoint = new_vert.size()-1;
				#endif
				grid.voxels[v3].currentPoint = new_vert.size()-1;
			} else {
				#ifdef EDGE_DETECT
				e2.endPoint = grid.voxels[v3].currentPoint;
				e3.startPoint = grid.voxels[v3].currentPoint;
				#endif
			}
			#ifdef EDGE_DETECT
			e1.firstFace = maxx;
			e2.firstFace = maxx;
			e3.firstFace = maxx;
			new_edges.push_back(e1);
			new_edges.push_back(e2);
			new_edges.push_back(e3);
			new_faces.push_back(faceEdge(new_edges.size()-3,new_edges.size()-2,new_edges.size()-1));
			#endif
		} else {
			// if they are in 2 different voxels :
			// ||  || (v2 == v3 && v3 != v1)
			if ( (v1 == v2 && v1 != v3)  ) {
				// put the point v3 in its voxel, and v1 in its own one
				#ifdef EDGE_DETECT
				edge e;
				#endif
				if (!grid.voxels[v3].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[2]]);
					#ifdef EDGE_DETECT
					e.startPoint = new_vert.size()-1;
					#endif
					grid.voxels[v3].currentPoint = new_vert.size()-1;
				} else {
					#ifdef EDGE_DETECT
					e.startPoint = grid.voxels[v3].currentPoint;
					#endif
				}
				if (!grid.voxels[v1].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[0]]);
					#ifdef EDGE_DETECT
					e.endPoint = new_vert.size()-1;
					#endif
					grid.voxels[v1].currentPoint = new_vert.size()-1;
				} else {
					#ifdef EDGE_DETECT
					e.endPoint = grid.voxels[v1].currentPoint;
					#endif
				}
				#ifdef EDGE_DETECT
				e.firstFace = maxx;
				new_edges.push_back(e);
				#endif
			} else if (v1 == v3 && v1 != v2) {
				// put the point v2 in its voxel, and v1 in its own one
				#ifdef EDGE_DETECT
				edge e;
				#endif
				if (!grid.voxels[v2].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[1]]);
					#ifdef EDGE_DETECT
					e.startPoint = new_vert.size()-1;
					#endif
					grid.voxels[v2].currentPoint = new_vert.size()-1;
				} else {
					#ifdef EDGE_DETECT
					e.startPoint = grid.voxels[v2].currentPoint;
					#endif
				}
				if (!grid.voxels[v1].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[0]]);
					#ifdef EDGE_DETECT
					e.endPoint = new_vert.size()-1;
					#endif
					grid.voxels[v1].currentPoint = new_vert.size()-1;
				} else {
					#ifdef EDGE_DETECT
					e.endPoint = grid.voxels[v1].currentPoint;
					#endif
				}
				#ifdef EDGE_DETECT
				e.firstFace = maxx;
				new_edges.push_back(e);
				#endif
			} else if (v2 == v3 && v3 != v1) {
				// put the point v1 in its voxel, and v2 in its own one
				#ifdef EDGE_DETECT
				edge e;
				#endif
				if (!grid.voxels[v1].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[0]]);
					#ifdef EDGE_DETECT
					e.startPoint = new_vert.size()-1;
					#endif
					grid.voxels[v1].currentPoint = new_vert.size()-1;
				} else {
					#ifdef EDGE_DETECT
					e.startPoint = grid.voxels[v1].currentPoint;
					#endif
				}
				if (!grid.voxels[v2].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[1]]);
					#ifdef EDGE_DETECT
					e.endPoint = new_vert.size()-1;
					#endif
					grid.voxels[v2].currentPoint = new_vert.size()-1;
				} else {
					#ifdef EDGE_DETECT
					e.endPoint = grid.voxels[v2].currentPoint;
					#endif
				}
				#ifdef EDGE_DETECT
				e.firstFace = maxx;
				new_edges.push_back(e);
				#endif
			} else if (v1 == v2 && v2 == v3 && v3 == v1) {
				// if they are all in the same voxel : 
				if (!grid.voxels[v1].hasPoint()) {
					new_vert.push_back(old_vert[faceIndexes[0]]);
					grid.voxels[v1].currentPoint = new_vert.size()-1;
				}
			}
		}
	}


	/**
	 * checks if there were any edges between points before, to make voxel connections :
	 */
	for (int i = 0; i < old_edges.size(); ++i) {
		unsigned int v_s = grid.getApproxVoxel(old_vert[old_edges[i].startPoint]);
		unsigned int v_e = grid.getApproxVoxel(old_vert[old_edges[i].endPoint]);

		if (grid.voxels[v_s].hasPoint() && grid.voxels[v_e].hasPoint()) {
			edge e (grid.voxels[v_s].currentPoint, grid.voxels[v_s].currentPoint, maxx);
			bool isAlreadyHere = false;
			for (int j = 0; j < new_edges.size(); ++j) {
				if (new_edges[i] == e) {
					isAlreadyHere = true;
					break;
				}
			}
			if (!isAlreadyHere) {
				new_edges.push_back(e);
			}
		}
	}

	std::cout << std::endl;

	// right now, we should have a new set of vertices, all corresponding from the earlier sampling,
	// and we should be able to analyse them for faces

	// std::vector<std::vector<std::pair<unsigned int, unsigned int>>> neighborsByEdges;
	// neighborsByEdges.resize(new_vert.size());
	// for (unsigned int i = 0; i < new_edges.size(); ++i) {
	// 	// get the start point :
	// 	unsigned int start = new_edges[i].startPoint;
	// 	// and the end point :
	// 	unsigned int end = new_edges[i].endPoint;
	// 	// add them both as neighbors of one another :
	// 	neighborsByEdges[start].push_back(std::make_pair(end,i));
	// 	neighborsByEdges[end].push_back(std::make_pair(start,i));
	// }

	// now we will analyse the model for faces. this should be fairly straightforward :

	// for (int i = 0; i < new_edges.size(); ++i) {
	// 	bool isCycle = false;
	// 	unsigned int maxx = std::numeric_limits<unsigned int>::infinity();
	// 	unsigned int p1 = new_edges[i].startPoint;
	// 	unsigned int p2 = new_edges[i].endPoint;
	// 	unsigned int p3 ;
	// 	unsigned int i_p2, i_p3;
	// 	for (int j = 0; j < neighborsByEdges[p2].size(); ++j) {
	// 		p3 = neighborsByEdges[p2][j].first;
	// 		i_p2 = j;
	// 		for (int k = 0; k < neighborsByEdges[p3].size(); ++k) {
	// 			if (neighborsByEdges[p3][k].first == p1) {
	// 				i_p3 = k;
	// 				isCycle = true;
	// 				break;
	// 			}
	// 		}
	// 		if (isCycle) {
	// 			break;
	// 		}
	// 	}
	// 	if (isCycle) {
	// 		// we should now have the value of p3 :
	// 		edge e1(p2,p3,maxx);
	// 		edge e2(p3,p1,maxx);

	// 		unsigned int f_e2, f_e3;

	// 		faceEdge f(i, i_p2, i_p3);
	// 		bool isAlreadyHere = false;
	// 		unsigned int i_f;
	// 		for (int j = 0; j < new_faces.size(); ++j) {
	// 			if (new_faces[j] == f) {
	// 				isAlreadyHere = true;
	// 				break;
	// 			}
	// 		}
	// 		if (!isAlreadyHere) {
	// 			new_faces.push_back(f);
	// 		}
	// 	}
	// }

	std::cout << "STATISTICS ABOUT THE NEW MODEL : " << std::endl;
	std::cout << new_vert.size() << " new vertices." << std::endl;
	std::cout << new_edges.size() << " new edges." << std::endl;
	std::cout << new_faces.size() << " new faces." << std::endl;
	std::cout << "STATISTICS ABOUT THE NEW MODEL : " << std::endl;

	modelEdge* newModel = new modelEdge();
	newModel->setPoints(new_vert);
	newModel->setEdges(new_edges);
	newModel->setFaces(new_faces);
	return *newModel;
}

unsigned int volumetricGrid::getApproxVoxel(const Point& p) const {
	double stepX = abs(max.x - min.x) / w;
	double stepY = abs(max.y - min.y) / h;
	double stepZ = abs(max.z - min.z) / d;

	double approxX = (p.x - this->min.x) / (stepX);
	double approxY = (p.y - this->min.y) / (stepY);
	double approxZ = (p.z - this->min.z) / (stepZ);

	unsigned int x = (unsigned int) floor(approxX);
	unsigned int y = (unsigned int) floor(approxY);
	unsigned int z = (unsigned int) floor(approxZ);

	unsigned int index = x * h*d + y * w + z;

	// if (!voxels[index].isInside(p)) {
	// 	std::cout << "error : point " << p << " was not inside voxel " << index << std::endl;
	// }
	return index;
}