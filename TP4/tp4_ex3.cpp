#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <fcntl.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/drawings.h"	// Drawing functions
#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/beziers.h"	// Bezier functions
#include "../lib/headers/sphere.h"	// Sphere generation

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -03.0
#define MAX_X_BOUNDING  03.0
#define MIN_Y_BOUNDING -03.0
#define MAX_Y_BOUNDING  03.0

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y);

void calculSphere(double,double, double,Point**) ;
GLvoid DrawSurfaceWireFrame(Point** points, double x, double y);

#pragma endregion

Sphere s;

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP4 : Representations surfaciques : Sphere");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	int rayon = 10;
	int nbParalleles = 15;
	int nbMeridiens = 30;

	Point origin = Point(0.0,0.0,0.0);

	s = Sphere(origin, rayon, nbParalleles, nbMeridiens);

}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	s.draw();
	s.drawWireFrame();
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		case 113:
			exit(1);
			break; 
		case KEY_TAB:
			window_display(); break;
		case KEY_I:
			s.addParallel(); window_display(); break;
		case KEY_K:
			s.removeParallel(); window_display(); break;
		case KEY_J:
			s.removeMeridian(); window_display(); break;
		case KEY_L:
			s.addMeridian(); window_display(); break;
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion
