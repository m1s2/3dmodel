#include <math.h>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>

#include "../lib/headers/model.h"	// Model class
#include "../lib/headers/options.h"	// Program options
#include "../lib/headers/halfedges.h"	// Model class using half edges

#pragma region Definitions et headers de fonctions 

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);
GLvoid readModel(const char* path, Point*tab);

// Actual content
options progParams;
modelHalfEdge mmm;
bool points;
bool boundingBox;
double normalOrtho[6] = {-1.0,1.0, -1.0,1.0, -1.0,1.0};

#pragma endregion

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(600, 50);
	glutCreateWindow("OpenGL TP9 : Subdivisions de Maillages");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	// Ajoute lumières
	progParams.updateLight();

	// Allume lumières
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	mmm = modelHalfEdge("../models/max.off");

	progParams.cameraOptions.setCenter(0.0,0.0,0.0);
	progParams.cameraOptions.zoomLevel = 1.0;
	double* bb = mmm.getBoundaries();
	std::cout << "computed bounds" << std::endl;
	std::cout << bb[0] << "," << bb[1] << std::endl;
	std::cout << bb[2] << "," << bb[3] << std::endl;
	std::cout << bb[4] << "," << bb[5] << std::endl;
	progParams.setOrthoArray(bb);
	progParams.drawAll = true;
	progParams.drawMode = GL_TRIANGLES;
	points = false;
	boundingBox = false;


	/**
	 * If C = world coords of object
	 * MULTIPLY BY INVC TO GET WORLD COORDS from object coords
	 */
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
//////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	progParams.updateCamera();
	DrawPoint(progParams.lightPos);
	DrawLine(Vector(Point(0.0,0.0,0.0), progParams.lightPos));
	DrawLine(progParams.x);
	DrawLine(progParams.y);

	if (!points) {
		if (progParams.drawMode == GL_TRIANGLES) {
			mmm.drawElements();
		} else {
			mmm.drawElementsWireframe();
		}
	} else {
		mmm.drawPoints();
	}
	if (boundingBox) {
		mmm.drawBoundingBox();
	}
	
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	progParams.updateOrtho();

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier
GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;

		case KEY_Z:
			progParams.moveCameraUp();
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_Q:
			progParams.moveCameraLeft();
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_S:
			progParams.moveCameraDown();
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_D:
			progParams.moveCameraRight();
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_A:
			progParams.unzoom(0.01);
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_E:
			progParams.zoom(0.01);
			progParams.updateLight();
			glutPostRedisplay();
			break;
		
		case KEY_T:
			progParams.drawAll = !progParams.drawAll;
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_G:
			if (progParams.drawMode == GL_LINES) {
				progParams.drawMode = GL_TRIANGLES;
			} else {
				progParams.drawMode = GL_LINES;
			}
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_R:
			mmm.subdiviseButterfly();
			mmm.printInfo();
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_X:
			(glIsEnabled(GL_LIGHTING)) ? glDisable(GL_LIGHTING) : glEnable(GL_LIGHTING);
			break;

		case KEY_P:
			mmm.printInfo();
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_M:
			points = !points;
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_B:
			boundingBox = !boundingBox;
			progParams.updateLight();
			glutPostRedisplay();
			break;

		case KEY_O:
			progParams.printOrtho();
			break;

		case KEY_W:
			progParams.setOrthoArray(normalOrtho);
			progParams.updateLight();
			glutPostRedisplay();
			break;

		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion