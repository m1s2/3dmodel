#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <fcntl.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/drawings.h"	// Drawing functions
#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/beziers.h"	// Define blocks

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -03.0
#define MAX_X_BOUNDING  03.0
#define MIN_Y_BOUNDING -03.0
#define MAX_Y_BOUNDING  03.0

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y);

void genererSurfaceReglee(Point*, Point*, double, double, Point**);
GLvoid DrawSurfaceWireFrame(Point** points, double x, double y);
GLvoid DrawSurfacePointCloud(Point** points, double x, double y);

#pragma endregion
GLvoid (*currentDraw)(Point**,double,double);
int currentPoint;				/* Identifiant point sélectionné */
double currentPx, currentPy, currentPz;		/* Coordonnées point sélectionné */
const double nbPtsCtrl		= 5;		/* Nombre de points courbe de contrôle */
const double nbPtsCourbe	= 15;		/* Nombre de points courbe Bezier résultante */
const double nbV		= 60;		/* Nombre de réplications des courbes sur la surface réglée */
Point* tabCtrl1;				/* Point de contrôle */
Point* tabCtrl2;				/* Point de contrôle */
Point* ptsCourbe1;				/* Points de la courbe résultante n*1 */
Point* ptsCourbe2;				/* Points de la courbe résultante n*2 */
Point** surfaceReglee;				/* Points résultants de la surface réglée */

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP3 : Courbes parametriques : Surfaces réglées");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	// Création des points de contrôle
	tabCtrl1 = new Point[(int)nbPtsCourbe];
	tabCtrl2 = new Point[(int)nbPtsCourbe];
	tabCtrl1[0] = Point(-5.0,-0.5, 0.0);
	tabCtrl1[1] = Point(-4.0, 0.5, 0.0);
	tabCtrl1[2] = Point(-4.0, 1.5, 0.0);
	tabCtrl1[3] = Point(-1.5, 2.5, 0.0);
	tabCtrl1[4] = Point( 1.0, 5.0, 0.0);
	// Courbe 2 : 
	tabCtrl2[0] = Point(-1.0,-5.0, 0.0);
	tabCtrl2[1] = Point( 0.0,-3.0, 0.0);
	tabCtrl2[2] = Point( 1.0,-1.5, 0.0);
	tabCtrl2[3] = Point( 3.0, 0.0, 0.0);
	tabCtrl2[4] = Point( 5.0, 1.0, 0.0);

	// Allocation points de la courbe génératrice
	ptsCourbe1 = new Point[(int)nbPtsCourbe];
	ptsCourbe2 = new Point[(int)nbPtsCourbe];

	surfaceReglee = new Point*[(int)nbPtsCourbe];
	for (int i = 0; i < nbPtsCourbe; ++i) {
		surfaceReglee[i] = new Point[(int)nbV];
	}

	currentDraw = &DrawSurfaceWireFrame;

	// Sélection point de contrôle sélectionné
	currentPoint = 0;
	currentPx = tabCtrl1[currentPoint].getX();
	currentPy = tabCtrl1[currentPoint].getY();
	currentPz = tabCtrl1[currentPoint].getZ();
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	// Dessine une grille pour mieux voir les distances
	DrawGrid(MIN_X_BOUNDING, MIN_Y_BOUNDING, MAX_X_BOUNDING+1, MAX_Y_BOUNDING+1);

	// Dessine les points de controle :
	// (avec couleur rouge foncé)
	DrawCurve(tabCtrl1, nbPtsCtrl, 0.5f, 0.0f, 0.0f);
	DrawCurve(tabCtrl2, nbPtsCtrl, 0.5f, 0.0f, 0.0f);

	// Re-calcule les courbes de Beziers avec Bernstein et les affiche :
	courbeBezier_Bernstein(tabCtrl1, nbPtsCtrl, nbPtsCourbe, ptsCourbe1);
	courbeBezier_Bernstein(tabCtrl2, nbPtsCtrl, nbPtsCourbe, ptsCourbe2);

	DrawCurve(ptsCourbe1, nbPtsCourbe);
	DrawCurve(ptsCourbe2, nbPtsCourbe);

	genererSurfaceReglee(ptsCourbe1, ptsCourbe2, nbPtsCourbe, nbV, surfaceReglee);

	// Afficher les règles entre vecteurs :
	Point** fullSurface = new Point*[(int)nbPtsCourbe];
	for (int i = 0; i < nbPtsCourbe; ++i) {
		fullSurface[i] = new Point[(int)nbV+2];
		fullSurface[i][0] = Point(ptsCourbe1[i]);
		for (int j = 1; j <= nbV; ++j) {
			fullSurface[i][j] = Point(surfaceReglee[i][j-1]);
		}
		fullSurface[i][(int)nbV+1] = Point(ptsCourbe2[i]);
	}

	currentDraw(fullSurface, nbPtsCourbe, nbV+2);

}

void genererSurfaceReglee(Point* courbeA, Point* courbeB, double nbPtsCourbe, double nbV, Point** pointsResultants) {
	// On crée un ensemble de vecteurs
	Vector* directors = new Vector[(int)nbPtsCourbe];

	// Pour tous les points des courbes :
	for (int i = 0; i < nbPtsCourbe; ++i) {
		Point* ptDep = &courbeA[i];
		Point* ptFin = &courbeB[i];

		directors[i] = Vector(ptFin->getX() - ptDep->getX(),ptFin->getY() - ptDep->getY(),ptFin->getZ() - ptDep->getZ());
		directors[i].scale((double)1.0/nbV);

		for (int j = 0; j < nbV; ++j) {
			Vector v = Vector(directors[i]);
			v.scale(j+1);
			pointsResultants[i][j] = Point(courbeA[i]);
			pointsResultants[i][j].translate(v);
		}
	}

	delete[] directors;
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		case 113:
			exit(1);
			break;
		case 112:
			currentDraw = (currentDraw == &DrawSurfaceWireFrame) ? &DrawSurfacePointCloud : &DrawSurfaceWireFrame;
			window_display();
			break;
		case KEY_TAB:
			currentPoint++;
			if (currentPoint == nbPtsCtrl)
				currentPoint = 0;
			break;
		case KEY_I:
			tabCtrl1[currentPoint].setY(tabCtrl1[currentPoint].getY()+0.5);
			window_display();
			break; 
		case KEY_J:
			tabCtrl1[currentPoint].setX(tabCtrl1[currentPoint].getX()-0.5);
			window_display();
			break; 
		case KEY_K:
			tabCtrl1[currentPoint].setY(tabCtrl1[currentPoint].getY()-0.5);
			window_display();
			break; 
		case KEY_L:
			tabCtrl1[currentPoint].setX(tabCtrl1[currentPoint].getX()+0.5);
			window_display();
			break; 
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion

GLvoid DrawSurfaceWireFrame(Point** points, double x, double y) {
	glPointSize(2);
	glLineWidth(0.5);
	// Color : blue
	glColor3f(0.0,0.0,1.0);
	glBegin(GL_LINES);
	for (int i = 0; i < x-1; ++i) {
		int indexI;
		int indexJ;
		for (int j = 0; j < y; ++j) {
			// indexI = i+0;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+0;
			// indexJ = j+1;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+1;
			// indexJ = j+1;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+1;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+0;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			glVertex3d(points[i][j].getX(),points[i][j].getY(),points[i][j].getZ());
			glVertex3d(points[i+1][j].getX(),points[i+1][j].getY(),points[i+1][j].getZ());
		}
	}
	for (int i = 0; i < x; ++i) {
		int indexI;
		int indexJ;
		for (int j = 0; j < y-1; ++j) {
			glVertex3d(points[i][j].getX(),points[i][j].getY(),points[i][j].getZ());
			glVertex3d(points[i][j+1].getX(),points[i][j+1].getY(),points[i][j+1].getZ());
		}
	}
	glEnd();
}

GLvoid DrawSurfacePointCloud(Point** points, double x, double y) {
	glPointSize(2);
	glLineWidth(0.5);
	// Color : blue
	glColor3f(0.0,0.0,1.0);
	glBegin(GL_POINTS);
	for (int i = 0; i < x; ++i) {
		int indexI;
		int indexJ;
		for (int j = 0; j < y; ++j) {
			// indexI = i+0;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+0;
			// indexJ = j+1;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+1;
			// indexJ = j+1;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+1;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+0;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			glVertex3d(points[i][j].getX(),points[i][j].getY(),points[i][j].getZ());
		}
	}
	glEnd();
}
