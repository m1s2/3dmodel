#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <fcntl.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/drawings.h"	// Drawing functions
#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/beziers.h"	// Define blocks

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -03.0
#define MAX_X_BOUNDING  03.0
#define MIN_Y_BOUNDING -03.0
#define MAX_Y_BOUNDING  03.0

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y);

void genererSurfaceReglee(Point*, Point*, double, double, Point**);
GLvoid DrawSurfaceWireFrame(Point** points, double x, double y);

#pragma endregion

int currentPointX;				/* Identifiant point sélectionné */
int currentPointY;				/* Identifiant point sélectionné */
double currentPx, currentPy, currentPz;		/* Coordonnées point sélectionné */
const double nbPtsCtrlX		= 3;		/* Nombre de points courbe de contrôle */
const double nbPtsCtrlY		= 3;		/* Nombre de points courbe de contrôle */
const double nbPtsCourbeX	= 10;		/* Nombre de points courbe Bezier generatrice */
const double nbPtsCourbeY	= 10;		/* Nombre de points courbe Bezier directrice */
Point** tabCtrl;				/* Point de contrôle */
Point** surfaceReglee;				/* Points résultants de la surface réglée */

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP3 : Courbes parametriques : Surface de Beziers");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	// Création des points de contrôle
	tabCtrl = new Point*[(int)nbPtsCtrlX];
	for (int i = 0; i < nbPtsCtrlX; ++i)
		tabCtrl[i] = new Point[(int)nbPtsCtrlY];

	tabCtrl[0][0] = Point(-2.0, 2.0, 0.0);
	tabCtrl[0][1] = Point( 0.0, 2.0, 0.0);
	tabCtrl[0][2] = Point( 2.0, 2.0, 0.0);
	tabCtrl[1][0] = Point(-2.0, 0.0, 0.0);
	tabCtrl[1][1] = Point( 0.0, 0.0, 0.0);
	tabCtrl[1][2] = Point( 2.0, 0.0, 0.0);
	tabCtrl[2][0] = Point(-2.0,-2.0, 0.0);
	tabCtrl[2][1] = Point( 0.0,-2.0, 0.0);
	tabCtrl[2][2] = Point( 2.0,-2.0, 0.0);

	surfaceReglee = new Point*[(int)nbPtsCourbeX];
	for (int i = 0; i < nbPtsCourbeX; ++i)
		surfaceReglee[i] = new Point[(int)nbPtsCourbeY];

	// Sélection point de contrôle sélectionné
	currentPointX = 0;
	currentPointY = 0;
	currentPx = tabCtrl[currentPointX][currentPointY].getX();
	currentPy = tabCtrl[currentPointX][currentPointY].getY();
	currentPz = tabCtrl[currentPointX][currentPointY].getZ();

	gluLookAt(0,0,0,50,50,50,1,0,0);
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	// Dessine une grille pour mieux voir les distances
	DrawGrid(MIN_X_BOUNDING, MIN_Y_BOUNDING, MAX_X_BOUNDING+1, MAX_Y_BOUNDING+1);

	// Dessine les points de controle :
	// (avec couleur rouge foncé)
	for (int i = 0; i < nbPtsCtrlX; ++i)
		for (int j = 0; j < nbPtsCtrlY; ++j)
			DrawPoint(tabCtrl[i][j]);

	// Re-calcule les courbes de Beziers avec Bernstein et les affiche :
	surfaceBezier_Bernstein(tabCtrl, nbPtsCtrlX, nbPtsCtrlY, nbPtsCourbeX, nbPtsCourbeY, surfaceReglee);

	// Draw surface (3D) ? :
	DrawSurfaceWireFrame(surfaceReglee, nbPtsCourbeX, nbPtsCourbeY);

}

void genererSurfaceReglee(Point* courbeA, Point* courbeB, double nbPtsCourbe, double nbV, Point** pointsResultants) {
	// On crée un ensemble de vecteurs
	Vector* directors = new Vector[(int)nbPtsCourbe];

	// Pour tous les points des courbes :
	for (int i = 0; i < nbPtsCourbe; ++i) {
		Point* ptDep = &courbeA[i];
		Point* ptFin = &courbeB[i];

		directors[i] = Vector(ptFin->getX() - ptDep->getX(),ptFin->getY() - ptDep->getY(),ptFin->getZ() - ptDep->getZ());
		directors[i].scale((double)1.0/nbV);

		for (int j = 0; j < nbV; ++j) {
			Vector v = Vector(directors[i]);
			v.scale(j+1);
			pointsResultants[i][j] = Point(courbeA[i]);
			pointsResultants[i][j].translate(v);
		}
	}

	delete[] directors;
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		case 113:
			exit(1);
			break;
		case KEY_TAB:
			currentPointX++;
			if (currentPointX == nbPtsCtrlX)
				currentPointX = 0;
			currentPointY++;
			if (currentPointY == nbPtsCtrlY)
				currentPointY = 0;
			break;
		case KEY_I:
			tabCtrl[currentPointX][currentPointY].setY(tabCtrl[currentPointX][currentPointY].getY()+0.5);
			window_display();
			break; 
		case KEY_J:
			tabCtrl[currentPointX][currentPointY].setX(tabCtrl[currentPointX][currentPointY].getX()-0.5);
			window_display();
			break; 
		case KEY_K:
			tabCtrl[currentPointX][currentPointY].setY(tabCtrl[currentPointX][currentPointY].getY()-0.5);
			window_display();
			break; 
		case KEY_L:
			tabCtrl[currentPointX][currentPointY].setX(tabCtrl[currentPointX][currentPointY].getX()+0.5);
			window_display();
			break; 
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

#pragma endregion

GLvoid DrawSurfaceWireFrame(Point** points, double x, double y) {
	glPointSize(2);
	glLineWidth(0.5);
	// Color : blue
	glColor3f(0.0,0.0,1.0);
	glBegin(GL_LINES);
	for (int i = 0; i < x-1; ++i) {
		int indexI;
		int indexJ;
		for (int j = 0; j < y; ++j) {
			// indexI = i+0;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+0;
			// indexJ = j+1;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+1;
			// indexJ = j+1;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+1;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			// indexI = i+0;
			// indexJ = j+0;
			// glVertex3d(points[indexI][indexJ].getX(),points[indexI][indexJ].getY(),points[indexI][indexJ].getZ());
			glVertex3d(points[i][j].getX(),points[i][j].getY(),points[i][j].getZ());
			glVertex3d(points[i+1][j].getX(),points[i+1][j].getY(),points[i+1][j].getZ());
		}
	}
	for (int i = 0; i < x; ++i) {
		int indexI;
		int indexJ;
		for (int j = 0; j < y-1; ++j) {
			glVertex3d(points[i][j].getX(),points[i][j].getY(),points[i][j].getZ());
			glVertex3d(points[i][j+1].getX(),points[i][j+1].getY(),points[i][j+1].getZ());
		}
	}
	glEnd();
}