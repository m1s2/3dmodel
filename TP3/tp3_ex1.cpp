#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <fcntl.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"	// Vector class
#include "../lib/headers/Point.h"	// Point class
#include "../lib/headers/drawings.h"	// Drawing functions
#include "../lib/headers/definitions.h"	// Define blocks
#include "../lib/headers/beziers.h"	// Define blocks

#pragma region Definitions et headers de fonctions 

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -03.0
#define MAX_X_BOUNDING  03.0
#define MIN_Y_BOUNDING -03.0
#define MAX_Y_BOUNDING  03.0

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y); 

void calculerSurfaceCylindrique(const Point*, const double, const Vector, Point, const double, Point**);
void tournerSelonAxeDirecteur(Vector, double, const Vector);
double** genererMatriceRodrigues(const Vector, const double);

void freeParams();

#pragma endregion

int currentPoint;				/* Identifiant point sélectionné */
double currentPx, currentPy, currentPz;		/* Coordonnées point sélectionné */
const double nbPtsCtrl		= 5;		/* Nombre de points courbe de contrôle */
const double nbPtsCourbe	= 10;		/* Nombre de points courbe résultante */
const double nbV		= 6;		/* Nombre de réplications sur surface cylindrique */
Point* ptsCtrl;					/* Point de contrôle */
Point* ptsCourbe;				/* Points de la courbe résultante */
Vector directeur;				/* Droite directrice de la surface cylindrique */
Point** courbesCylindriques;			/* Points résultants de la surface cylindrique */

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP2 : Courbes parametriques : Beziers par Bernstein");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

void init_scene() {
	// Création des points de contrôle
	ptsCtrl = new Point[(int)nbPtsCtrl];
	ptsCtrl[0] = Point(-1.0,-0.5, 0.0);
	ptsCtrl[1] = Point( 0.0,-0.5, 0.0);
	ptsCtrl[2] = Point( 1.0, 0.5, 0.0);
	ptsCtrl[3] = Point( 1.5, 1.0, 0.0);
	ptsCtrl[4] = Point( 2.0, 2.0, 0.0);

	// Création de la droite directrice
	directeur = Vector(2.0,1.0,0.0);

	// Allocation points de la courbe génératrice
	ptsCourbe = new Point[(int)nbPtsCourbe];

	// Allocation points surface cylindrique résultante
	courbesCylindriques = new Point*[(int)nbV];
	for (int i = 0; i < (int) nbV; ++i) 
		courbesCylindriques[i] = new Point[(int)nbPtsCourbe];

	// Sélection point de contrôle sélectionné
	currentPoint = 0;
	currentPx = ptsCtrl[currentPoint].getX();
	currentPy = ptsCtrl[currentPoint].getY();
	currentPz = ptsCtrl[currentPoint].getZ();
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	// Dessine une grille pour mieux voir les distances
	DrawGrid(MIN_X_BOUNDING, MIN_Y_BOUNDING, MAX_X_BOUNDING+1, MAX_Y_BOUNDING+1);

	// Dessine les points de controle :
	// (avec couleur rouge foncé)
	DrawCurve(ptsCtrl, nbPtsCtrl, 0.5f, 0.0f, 0.0f);

	// Re-calcule la courbe de Beziers avec Bernstein et l'affiche :
	courbeBezier_Bernstein(ptsCtrl, nbPtsCtrl, nbPtsCourbe, ptsCourbe);

	// Libere tableau
	// for (int i = 0; i < nbV-1; ++i)
		// delete[] courbesCylindriques[i];
	// delete[] courbesCylindriques;

	// Dessinner les courbes correpondantes en V
	calculerSurfaceCylindrique(ptsCourbe, nbPtsCourbe, directeur, ptsCtrl[0], nbV, courbesCylindriques);

	// Afficher directive :
	DrawLine(directeur,ptsCtrl[0]);

	DrawCurve(ptsCourbe, nbPtsCourbe);

	// for (int i = 0; i < (int) nbV; ++i) {
	// 	DrawCurve(courbesCylindriques[i], nbPtsCourbe);
	// }

}


void calculerSurfaceCylindrique(const Point* courbeOrigine, 
				const double nbPtsOrigine, 
				const Vector directeur, 
				Point origineVecteur, 
				const double nbV, 
				Point** pointsResultants) {

	// Prendre le vecteur directeur normalisé
	Vector unitDirect = Vector(directeur);
	unitDirect.normalize();

	/*
	// Verifier que le tableau de points résultants
	// est bien à la bonne taille :
	for (int i = 0; i < nbV; ++i)
		delete[] pointsResultants[i];
	delete[] pointsResultants;
	// Ré-allouer les points de surface :
	pointsResultants = new Point*[(int)nbV];	// Créer nbV courbes 
	for (int i = 0; i < nbV; ++i)
		pointsResultants[i] = new Point[(int)nbPtsOrigine];
	*/

	Vector** vecteursCourbe = new Vector*[(int)nbPtsOrigine];
	for (int i = 0; i < nbV; ++i)
		vecteursCourbe[i] = new Vector[(int)nbPtsCourbe];

	// Faire 'nbV' projections de points sur la droite directrice
	// afin de pouvoir créer la surface cylindrique :
	for (int i = 0; i < nbV; ++i) {
		for (int j = 0; j < nbPtsOrigine; ++j) {
			Point proj = Point(courbeOrigine[j]).ProjectOnLine(unitDirect, origineVecteur);
			pointsResultants[i][j] = Point(proj);
			// On prends les vecteurs
			vecteursCourbe[i][j] = Vector(courbeOrigine[j].getX()-proj.getX(),
						courbeOrigine[j].getY() - proj.getY(),
						courbeOrigine[j].getZ() - proj.getZ());
		}
	}

	double pasRotation = 360.0 / (nbV);

	// Pour toutes les courbes isoparamétriques résultantes :
	// Tourner tous les vecteurs dans le bon sens et
	// translater touts les points générés par leur vecteur 
	for (int i = 0; i < nbV; ++i)  {
		double angleRotation = ((double)i+1.0) * pasRotation;
		for (int j = 0; j < nbPtsCourbe; ++j) {
			tournerSelonAxeDirecteur(vecteursCourbe[i][j], angleRotation, directeur);
			pointsResultants[i][j].translate(vecteursCourbe[i][j]);
		}
	}
	
}

void tournerSelonAxeDirecteur(Vector demande, double angle, const Vector axeRotation) {
	// Ramener l'angle entre 0 et 360
	while (angle > 360.0) 
		angle -= 360.0;

	double* coordVect = demande.toArray();

	// Générer la matrice de rotation du vecteur demandé selon
	// l'axe de rotation pour un angle donné alpha :
	double** rodriguesMatrix = genererMatriceRodrigues(axeRotation, angle);

	double* vectTourne = new double[3]();

	// Effectuer la multiplication matricielle :
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			vectTourne[i] += coordVect[j] * rodriguesMatrix[j][i];

	// Changer les coordonnées vecteur :
	demande.setX(vectTourne[0]);
	demande.setY(vectTourne[1]);
	demande.setZ(vectTourne[2]);

	// delete[] rodriguesMatrix;
	// delete[] vectTourne;
	// delete[] coordVect;
}

double** genererMatriceRodrigues(const Vector directeur, const double angle) {
	// Génération des matrices de la formule de Rodrigues :
	double** identityMatrix = new double*[3];
	double** planeMatrix = new double*[3];
	double** offsetMatrix = new double*[3];
	double** rodriguesMatrix = new double*[3];
	for (int i = 0; i < 3; ++i) {
		identityMatrix[i] = new double[3];
		planeMatrix[i] = new double[3];
		offsetMatrix[i] = new double[3];
		rodriguesMatrix[i] = new double[3];
	}

	// Coefficients de matrices
	double coefAlpha = std::cos(angle);
	double coefBeta  = ( 1 - std::cos(angle) );
	double coefTheta = std::sin(angle);

	// Coordonnées vecteur directeur :
	double nx = directeur.getX();
	double ny = directeur.getY();
	double nz = directeur.getZ();

	identityMatrix[0][0] = coefAlpha; identityMatrix[0][1] = 0; identityMatrix[0][2] = 0;
	identityMatrix[1][0] = 0; identityMatrix[1][1] = coefAlpha; identityMatrix[1][2] = 0;
	identityMatrix[2][0] = 0; identityMatrix[2][1] = 0; identityMatrix[2][2] = coefAlpha;

	planeMatrix[0][0] = nx*nx*coefBeta; planeMatrix[0][1] = nx*ny*coefBeta; planeMatrix[0][2] = nx*nz*coefBeta;
	planeMatrix[1][0] = nx*ny*coefBeta; planeMatrix[1][1] = ny*ny*coefBeta; planeMatrix[1][2] = ny*nz*coefBeta;
	planeMatrix[2][0] = nx*nz*coefBeta; planeMatrix[2][1] = ny*nz*coefBeta; planeMatrix[2][2] = nz*nz*coefBeta;

	offsetMatrix[0][0] = 0; offsetMatrix[0][1] = -nz*coefTheta; offsetMatrix[0][2] =  ny*coefTheta;
	offsetMatrix[1][0] =  nz*coefTheta; offsetMatrix[1][1] = 0; offsetMatrix[1][2] = -nx*coefTheta;
	offsetMatrix[2][0] = -ny*coefTheta; offsetMatrix[2][1] =  nx*coefTheta; offsetMatrix[0][1] = 0;

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			rodriguesMatrix[i][j] = identityMatrix[i][j] + planeMatrix[i][j] + offsetMatrix[i][j];

	delete[] identityMatrix;
	delete[] offsetMatrix;
	delete[] planeMatrix;
	
	return rodriguesMatrix;
}

//////////////////////////////////
//	AFFICHAGES ET INPUTS	//
//////////////////////////////////

#pragma region Affichages et inputs 

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	// Effectue un rendu de la scène
	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}


// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		case 113:
			exit(1);
			break;
		case KEY_TAB:
			currentPoint++;
			if (currentPoint == nbPtsCtrl)
				currentPoint = 0;
			break;
		case KEY_I:
			ptsCtrl[currentPoint].setY(ptsCtrl[currentPoint].getY()+0.5);
			window_display();
			break; 
		case KEY_J:
			ptsCtrl[currentPoint].setX(ptsCtrl[currentPoint].getX()-0.5);
			window_display();
			break; 
		case KEY_K:
			ptsCtrl[currentPoint].setY(ptsCtrl[currentPoint].getY()-0.5);
			window_display();
			break; 
		case KEY_L:
			ptsCtrl[currentPoint].setX(ptsCtrl[currentPoint].getX()+0.5);
			window_display();
			break; 
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}




void freeParams() {
	delete[] ptsCtrl;
	delete[] ptsCourbe;

	for (int i = 0; i < (int)nbV; ++i) {
		free(courbesCylindriques[i]);
	}
	free(courbesCylindriques);
	return;
}

#pragma endregion
