#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"
#include "../lib/headers/Point.h"

// Définition de la taille de la fenêtre
#define WIDTH  900
#define HEIGHT 900

// Définition de la couleur de la fenêtre
#define RED   0.3
#define GREEN 0.3
#define BLUE  0.3
#define ALPHA 1

// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y); 

GLvoid DrawLine(Vector v);
GLvoid DrawLine(Vector v, Point dep);
GLvoid DrawPoint(Point p);

int main(int argc, char **argv) 
{  
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL : Premier exemple : carré");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();  

	return 1;
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}

// Initialisation de la scene. Peut servir à stocker des variables de votre programme
// & initialiser
void init_scene()
{
	//
}

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	Vector v1 = Vector(2,-1,0);
	Point a = Point(0,0,0);
	// Point b = Point(2,0,0);
	Point c = Point(-1,0,0);
	Point d = c.ProjectOnLine(v1,a);
	
	DrawPoint(a);
	DrawLine(v1,a);
	// DrawPoint(b);
	DrawPoint(c);
	DrawPoint(d);

	// C'est l'endroit où l'on peut dessiner. On peut aussi faire appel
	// à une fonction (render_scene() ici) qui contient les informations 
	// que l'on veut dessiner
	// render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(-2.0, 2.0, -2.0, 2.0, -2.0, 2.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:  
			exit(1);                    
			break; 
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}



//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene()
{
	//Définition de la couleur
	glColor3f(1.0, 1.0, 1.0);

	// Nous créons ici un polygone. Nous pourrions aussi créer un triangle ou des lignes. Voir ci-dessous les parties 
	// en commentaires (il faut commenter le bloc qui ne vous intéresse pas et décommenter celui que vous voulez tester.

	// Création de deux lignes
	// glBegin(GL_POLYGON);
	// 	glVertex3f(-1, -1, 0);
	// 	glVertex3f(1, 1, 0);
	// 	glVertex3f(1, -1, 0);
	// 	glVertex3f(-1, 1, 0); 
	// glEnd();
  
	// Création d'un polygone
	glBegin(GL_POLYGON);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, -1, 0);
		glVertex3f(1, 1, 0);
		glVertex3f(-1, 1, 0);
	glEnd();

	glColor3f(1.0, 1.0, 0.0);

	// création d'un triangle
	glBegin(GL_TRIANGLES);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, -1, 0);
		glVertex3f(1, 1, 0);
	glEnd();
}

GLvoid DrawLine(Vector v) {
	glColor3f(0,0,0);	// Donne couleur noire
	glLineWidth(2);		// Donne largeur de crayon
	glBegin(GL_LINES);	// dessine
		glVertex3f(-0.7,0.3,0);				// debut ligne
		glVertex3f(v.getX(), v.getY(), v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawLine(Vector v, Point dep) {
	glColor3f(0,0,0);	// Donne couleur noire
	glLineWidth(2);		// Donne largeur de crayon
	glBegin(GL_LINES);	// dessine
		glVertex3f(dep.getX(), dep.getY(), dep.getZ());				// debut ligne
		glVertex3f(dep.getX()+v.getX(), dep.getY()+v.getY(), dep.getZ()+v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawPoint(Point p) {
	glPointSize(2);
	glColor3f(1,0,0.5);
	glBegin(GL_POINTS);
		glVertex3f(p.getX(), p.getY(), p.getZ());
	glEnd();
}
