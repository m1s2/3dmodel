#ifndef MESH_H
#define MESH_H


#include <vector>
#include <string>
#include "Vec3.h"
#include "Skeleton.h"

#include <cmath>

#include <GL/glut.h>


// -------------------------------------------
// Basic Mesh class
// -------------------------------------------

struct MeshVertex {
    inline MeshVertex () {
        w.clear();
    }
    inline MeshVertex (const Vec3 & _p, const Vec3 & _n) : p (_p), n (_n) {
        w.clear();
    }
    inline MeshVertex (const MeshVertex & vertex) : p (vertex.p), n (vertex.n) , w(vertex.w) {
    }
    inline virtual ~MeshVertex () {}
    inline MeshVertex & operator = (const MeshVertex & vertex) {
        p = vertex.p;
        n = vertex.n;
        w = vertex.w;
        return (*this);
    }
    // membres :
    Vec3 p; // une position
    Vec3 n; // une normale
    std::vector< double > w; // skinning weights
};

struct MeshTriangle {
    inline MeshTriangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline MeshTriangle (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline MeshTriangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    inline virtual ~MeshTriangle () {}
    inline MeshTriangle & operator = (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};




class Mesh {
public:
    std::vector<MeshVertex> V;
    std::vector<MeshTriangle> T;

    void loadOFF (const std::string & filename);
    void recomputeNormals ();


    void computeSkinningWeights( Skeleton const & skeleton ) {
        //---------------------------------------------------//
        //---------------------------------------------------//
        // code to change :

        // Indications:
        // you should compute weights for each vertex w.r.t. the skeleton bones
        // so each vertex will have B weights (B = number of bones)
        // these weights shoud be stored in vertex.w:

        for( unsigned int i = 0 ; i < V.size() ; ++i ) {
            MeshVertex & vertex = V[ i ];
            float totalLength = 0.0;
            for (unsigned int j = 0; j < skeleton.bones.size(); ++j) {
                Vec3 startPoint = skeleton.articulations[skeleton.bones[j].joints[0]].p;
                Vec3 endPoint   = skeleton.articulations[skeleton.bones[j].joints[1]].p;

                float coefDist = 8.0;

                if ((vertex.p - startPoint).length() < (vertex.p - endPoint).length()) {
                    Vec3 boneVect = endPoint - startPoint;
                    float startLength = boneVect.length();
                    // Compute projection on line
                    boneVect.normalize();
                    float bap = Vec3::dot( (vertex.p - startPoint), boneVect ) / boneVect.length();
                    // Check the sides of the projection
                    Vec3 projection;
                    if (bap > 0.0 && bap < startLength) {
                        projection = startPoint+(bap * boneVect);
                    } else if (bap < 0.0) {
                        projection = startPoint;
                    } else {
                        projection = endPoint;
                    }
                    // compute new weight
                    float length = 1.0f / pow(((vertex.p - projection).length()),coefDist);
                    vertex.w.push_back(length);
                    totalLength += length;
                } else {
                    Vec3 boneVect = startPoint - endPoint;
                    float startLength = boneVect.length();
                    // Compute projection on line
                    boneVect.normalize();
                    float bap = Vec3::dot( (vertex.p - endPoint), boneVect ) / boneVect.length();
                    Vec3 projection;
                    if (bap > 0.0 && bap < startLength) {
                        projection = endPoint+(bap * boneVect);
                    } else if (bap < 0.0) {
                        projection = endPoint;
                    } else {
                        projection = startPoint;
                    }

                    float length = 1.0f / pow(((vertex.p - projection).length()),coefDist);
                    vertex.w.push_back(length);
                    totalLength += length;
                }
            }
            float coef = totalLength / vertex.w.size();
            for (unsigned int j = 0; j < vertex.w.size(); ++j) {
                vertex.w[j] = vertex.w[j]/totalLength;
            }
        }

        //---------------------------------------------------//
        //---------------------------------------------------//
        //---------------------------------------------------//
    }

    void draw() const {
        glEnable(GL_LIGHTING);
        glBegin (GL_TRIANGLES);
        for (unsigned int i = 0; i < T.size (); i++)
            for (unsigned int j = 0; j < 3; j++) {
                const MeshVertex & v = V[T[i].v[j]];
                glNormal3f (v.n[0], v.n[1], v.n[2]);
                glVertex3f (v.p[0], v.p[1], v.p[2]);
            }
        glEnd ();
    }

    void drawTransformedMesh( SkeletonTransformation const & transfo, int boneSelected = -1 ) const {
        std::vector< Vec3 > newPositions( V.size() );
        std::vector< Vec3 > newNormals( V.size() );

        //---------------------------------------------------//
        //---------------------------------------------------//
        // code to change :
        for( unsigned int i = 0 ; i < V.size() ; ++i ) {
            Vec3 p = V[i].p;

            // Indications:
            // you should use the skinning weights to blend the transformations of the vertex position by the bones.

            Vec3 newPosition = Vec3(0.0,0.0,0.0); // V[i].p
            Vec3 newNormal = V[i].n;

            for (unsigned int j = 0; j < transfo.boneTransformations.size(); ++j) {
                Mat3 rotation = transfo.boneTransformations[j].worldSpaceRotation;
                Vec3 translation = transfo.boneTransformations[j].worldSpaceTranslation;
                newPosition += V[i].w[j] * (rotation * p + translation);
            }
            newPositions[ i ] = newPosition;
        }

        //---------------------------------------------------//
        //---------------------------------------------------//
        //---------------------------------------------------//

        glEnable(GL_LIGHTING);
        if (boneSelected != -1) {
            glEnable(GL_COLOR_MATERIAL);
        }
        glBegin (GL_TRIANGLES);
        for (unsigned int i = 0; i < T.size (); i++)
            for (unsigned int j = 0; j < 3; j++) {
                const MeshVertex & v = V[T[i].v[j]];
                Vec3 p = newPositions[ T[i].v[j] ];
                Vec3 n1 = newPositions[ T[i].v[1] ] - newPositions[ T[i].v[0] ];
                Vec3 n2 = newPositions[ T[i].v[2] ] - newPositions[ T[i].v[0] ];
                Vec3 n = Vec3::cross(n1, n2);
                n.normalize();
                if (boneSelected != -1) {
                    float coefSkin = V[T[i].v[j]].w[boneSelected];
                    // if (coefSkin > 1e-4) {
                    //     glColor3f((1.0-coefSkin), 0.0, coefSkin);
                    // } else {
                    //     glColor3f(1.0,1.0,1.0);
                    // }
                    glColor3f((1.0-coefSkin), 0.0, coefSkin);
                }
                glNormal3f (n[0], n[1], n[2]);
                glVertex3f (p[0], p[1], p[2]);
            }
        glEnd ();
        if (boneSelected != -1) {
            glDisable(GL_COLOR_MATERIAL);
        }
    }
};



#endif
