#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <fcntl.h>
#include <GL/glut.h>
#include <vector>
#include "../lib/headers/Vector.h"
#include "../lib/headers/Point.h"
#include "../lib/headers/drawings.h"
#include "../lib/headers/definitions.h"
#include "../lib/headers/beziers.h"

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -03.0
#define MAX_X_BOUNDING  03.0
#define MIN_Y_BOUNDING -03.0
#define MAX_Y_BOUNDING  03.0

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y); 

// Variables utilisées pendant le reste du programme
double currentPx, currentPy, currentPz;
int currentPoint;
double nbPtsCtrl, nbPtsCourbe;
Point* ptsCtrl;
Point* ptsCourbe;
std::vector< std::vector<Point> > ptsCasteljau;

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP2 : Courbes parametriques : Beziers par Casteljau");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

#pragma region Affichage, gestion inputs 

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() {
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}

// Initialisation de la scene. Peut servir à stocker des variables de votre programme
// & initialiser
void init_scene() {
	nbPtsCtrl = 4;
	ptsCtrl = new Point[(int)nbPtsCtrl];
	ptsCtrl[0] = Point(-1.0,-0.5, 0.0);
	ptsCtrl[1] = Point( 0.0,-0.5, 0.0);
	ptsCtrl[2] = Point( 1.0, 0.5, 0.0);
	ptsCtrl[3] = Point( 1.5, 1.0, 0.0);

	nbPtsCourbe = 50;
	ptsCourbe = new Point[(int)nbPtsCourbe];

	currentPoint = 0;
	currentPx = ptsCtrl[currentPoint].getX();
	currentPy = ptsCtrl[currentPoint].getY();
	currentPz = ptsCtrl[currentPoint].getZ();

	ptsCasteljau = std::vector< std::vector<Point> >();
}

// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s'appliquent au modèle de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:  
			exit(1);                    
			break;
		// Ajout de Q pour l'arret programme :
		case 113:
			exit(1);
			break;
		case KEY_TAB:
			currentPoint++;
			if (currentPoint == nbPtsCtrl)
				currentPoint = 0;
			break;
		case KEY_I:
			ptsCtrl[currentPoint].setY(ptsCtrl[currentPoint].getY()+0.5);
			window_display();
			break; 
		case KEY_J:
			ptsCtrl[currentPoint].setX(ptsCtrl[currentPoint].getX()-0.5);
			window_display();
			break; 
		case KEY_K:
			ptsCtrl[currentPoint].setY(ptsCtrl[currentPoint].getY()-0.5);
			window_display();
			break; 
		case KEY_L:
			ptsCtrl[currentPoint].setX(ptsCtrl[currentPoint].getX()+0.5);
			window_display();
			break; 
		case 43:
			nbPtsCourbe+=10;
			window_display();
			break;
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene(){
	// Dessine une grille pour mieux voir les distances
	DrawGrid(MIN_X_BOUNDING, MIN_Y_BOUNDING, MAX_X_BOUNDING+1, MAX_Y_BOUNDING+1);

	// Dessine les points de controle :
	// (avec couleur rouge foncé)
	DrawCurve(ptsCtrl, nbPtsCtrl, 0.5f, 0.0f, 0.0f);

	ptsCasteljau.erase(ptsCasteljau.begin(),ptsCasteljau.end());

	courbeBeziersCastelnau(ptsCtrl, nbPtsCtrl, nbPtsCourbe, ptsCourbe, ptsCasteljau);

	DrawCurve(ptsCourbe, nbPtsCourbe);

	for (int i = 0; i < ptsCasteljau.size(); ++i) {
		DrawCurve(ptsCasteljau.at(i), ptsCasteljau.at(i).size(),i);
	}

	// printf("\n\n\n");
}