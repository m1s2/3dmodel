#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>
#include <GL/glut.h>
#include "../lib/headers/Vector.h"
#include "../lib/headers/Point.h"

// Définition de la taille de la fenêtre
#define WIDTH  720
#define HEIGHT 720

// Définition de la couleur de la fenêtre
#define RED   0.3
#define GREEN 0.3
#define BLUE  0.3
#define ALPHA 1

// Definition de la fenetre de rendu (clipping grid)
#define MIN_X_BOUNDING -5.5
#define MAX_X_BOUNDING 5.5
#define MIN_Y_BOUNDING -5.5
#define MAX_Y_BOUNDING 5.5

// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27

#define glf GLfloat

// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height); 
GLvoid window_key(unsigned char key, int x, int y); 

void HermiteCubicCurve(Point p0, Point p1, Vector v0, Vector v1, long nbU, Point* resultat);
void HermiteCubicCurveMatrix(Point p0, Point p1, Vector v0, Vector v1, long nbU, Point* resultat);
Point multMatriceHermite(double u, double** m, double** p);

GLvoid DrawLine(Vector v);
GLvoid DrawLine(Vector v, Point p);
GLvoid DrawPoint(Point p);
GLvoid DrawCurve(Point* pointsOfCurve, long nbPoints);
GLvoid DrawCurve(Point* pointsOfCurve, Point endPoint, long nbPoints);
GLvoid DrawGrid(double lowerX, double lowerY, double higherX, double higherY);

int main(int argc, char* argv[]) {
	// initialisation  des paramètres de GLUT en fonction
	// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

	// définition et création de la fenêtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("OpenGL TP2 : Courbes parametriques");

	// initialisation de OpenGL et de la scène
	initGL();  
	init_scene();

	// choix des procédures de callback pour 
	// le tracé graphique
	glutDisplayFunc(&window_display);
	// le redimensionnement de la fenêtre
	glutReshapeFunc(&window_reshape);
	// la gestion des évènements clavier
	glutKeyboardFunc(&window_key);

	// la boucle prinicipale de gestion des évènements utilisateur
	glutMainLoop();

	exit(0);
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL() 
{
	glClearColor(RED, GREEN, BLUE, ALPHA);        
}

// Initialisation de la scene. Peut servir à stocker des variables de votre programme
// & initialiser
void init_scene()
{
	//
}

// fonction de call-back pour l'affichage dans la fenêtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	DrawGrid(MIN_X_BOUNDING, MIN_Y_BOUNDING, MAX_X_BOUNDING, MAX_Y_BOUNDING);

	// Point depart Hermite
	Point pdh = Point(0, 0, 0);
	// Point fin hermite
	Point pfh = Point(2, 0, 0);
	// Vecteur depart hermite
	Vector vdh = Vector(1, 1, 0);
	// Vecteur fin hermite
	Vector vfh = Vector(1, -1, 0);
	long nbU = 10;
	Point* res = new Point[nbU];
	HermiteCubicCurve(pdh, pfh, vdh, vfh, nbU, res);

	DrawPoint(pdh);
	DrawPoint(pfh);
	DrawLine(vdh, pdh);
	DrawLine(vfh, pfh);

	DrawCurve(res, nbU);

	// C'est l'endroit où l'on peut dessiner. On peut aussi faire appel
	// à une fonction (render_scene() ici) qui contient les informations 
	// que l'on veut dessiner
	// render_scene();

	// trace la scène graphique qui vient juste d'être définie
	glFlush();
}

// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
	// possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est 
	// de trop grosse taille par rapport à la fenêtre.
	glOrtho(MIN_X_BOUNDING, MAX_X_BOUNDING, MIN_Y_BOUNDING, MAX_Y_BOUNDING, -10.0, 10.0);

	// toutes les transformations suivantes s�appliquent au mod�le de vue 
	glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des évènements clavier

GLvoid window_key(unsigned char key, int x, int y) 
{  
	switch (key) {    
		case KEY_ESC:  
			exit(1);                    
			break;
		// Ajout de "Q" pour quitter le programme
		case 113:
			exit(1);
			break; 
		default:
			printf ("La touche %d n'est pas active.\n", key);
			break;
	}     
}



//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
/////////////////////////////////////////////////////////////////////////////////////////
void render_scene()
{
	//Définition de la couleur
	glColor3f(1.0, 1.0, 1.0);

	// Nous créons ici un polygone. Nous pourrions aussi créer un triangle ou des lignes. Voir ci-dessous les parties 
	// en commentaires (il faut commenter le bloc qui ne vous intéresse pas et décommenter celui que vous voulez tester.

	// Création de deux lignes
	// glBegin(GL_POLYGON);
	// 	glVertex3f(-1, -1, 0);
	// 	glVertex3f(1, 1, 0);
	// 	glVertex3f(1, -1, 0);
	// 	glVertex3f(-1, 1, 0); 
	// glEnd();
  
	// Création d'un polygone
	glBegin(GL_POLYGON);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, -1, 0);
		glVertex3f(1, 1, 0);
		glVertex3f(-1, 1, 0);
	glEnd();

	glColor3f(1.0, 1.0, 0.0);

	// création d'un triangle
	glBegin(GL_TRIANGLES);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, -1, 0);
		glVertex3f(1, 1, 0);
	glEnd();
}

GLvoid DrawLine(Vector v) {
	glColor3f(0,0,0);	// Donne couleur noire
	glLineWidth(2);		// Donne largeur de crayon
	glBegin(GL_LINES);	// dessine
		glVertex3f(-0.7,0.3,0);				// debut ligne
		glVertex3f(v.getX(), v.getY(), v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawLine(Vector v, Point p) {
	glColor3f(0,0,0);	// Donne couleur noire
	glLineWidth(2);		// Donne largeur de crayon

	glBegin(GL_LINES);	// dessine
		glVertex3f(p.getX(), p.getY(), p.getZ());	// debut ligne
		glVertex3f(p.getX()+v.getX(), p.getY()+v.getY(), p.getZ()+v.getZ());	// fin ligne
	glEnd();
}

GLvoid DrawPoint(Point p) {
	glPointSize(2);
	glColor3f(1,0,0.5);
	glBegin(GL_POINTS);
		glVertex3f(p.getX(), p.getY(), p.getZ());
	glEnd();
}

GLvoid DrawCurve(Point* pointsOfCurve, long nbPoints) {
	// Stores current X,Y,Z point information
	float px, py, pz;

	// OpenGL drawing parameters
	glPointSize(2);
	glLineWidth(1);
	glColor3f(0.f,1.f,1.f);
	// OpenGL render loop
	glBegin(GL_LINE_STRIP);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve[i].getX();
		py = pointsOfCurve[i].getY();
		pz = pointsOfCurve[i].getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
}

GLvoid DrawCurve(Point* pointsOfCurve, Point endPoint, long nbPoints) {
	// Stores current X,Y,Z point information
	float px, py, pz;

	// OpenGL drawing parameters
	glPointSize(2);
	glLineWidth(1);
	glColor3f(0.f,1.f,1.f);
	// OpenGL render loop
	glBegin(GL_LINE_STRIP);
	for (long i = 0; i < nbPoints; ++i) {
		px = pointsOfCurve[i].getX();
		py = pointsOfCurve[i].getY();
		pz = pointsOfCurve[i].getZ();
		glVertex3f(px,py,pz);
	}
	glEnd();
	glColor3f(1.f, 0.f, 1.f);
	glBegin(GL_LINES);
		glVertex3f(pointsOfCurve[nbPoints-1].getX(), pointsOfCurve[nbPoints-1].getY(), pointsOfCurve[nbPoints-1].getZ());
		glVertex3f(endPoint.getX(), endPoint.getY(), endPoint.getZ());
	glEnd();
}

void HermiteCubicCurve(Point p0, Point p1, Vector v0, Vector v1, long nbU, Point* resultat) {
	free(resultat);
	resultat = new Point[nbU];

	double px,py,pz,u;
	double f1,f2,f3,f4;

	for (int i = 0; i < nbU; ++i) {
		// Prends la valeur de U :
		u = (double)i * ((double)1 / (double)(nbU-1));
		// Calcule les coefficients F1 à F4 :
		f1 =  2 * pow(u,3) - 3 * pow(u,2)                + 1;
		f2 = -2 * pow(u,3) + 3 * pow(u,2)                   ;
		f3 =      pow(u,3) - 2 * pow(u,2) +     pow(u,1)    ;
		f4 =      pow(u,3) -     pow(u,2)                   ;
		// Détermine les coordonées X,Y, et Z du point P(U) :
		px = f1 * p0.getX() + f2 * p1.getX() + f3 * v0.getX() + f4 * v1.getX();
		py = f1 * p0.getY() + f2 * p1.getY() + f3 * v0.getY() + f4 * v1.getY();
		pz = f1 * p0.getZ() + f2 * p1.getZ() + f3 * v0.getZ() + f4 * v1.getZ();
		resultat[i] = Point(px,py,pz);
	}
	return;
}

void HermiteCubicCurveMatrix(Point p0, Point p1,
			Vector v0, Vector v1, 
			long nbU, Point* resultat) {

	// generer matrice des points et des vecteurs
	double** matrice_transition = new double*[4];
	for (int i = 0; i < 4; ++i) {
		matrice_transition[i] = new double[4];
	}

	// OH LAWD HE COMIN

	matrice_transition[0][0] = 2.0;
	matrice_transition[0][1] = -2.0;
	matrice_transition[0][2] = 1.0;
	matrice_transition[0][3] = 1.0;

	matrice_transition[1][0] = -3.0;
	matrice_transition[1][1] = 3.0;
	matrice_transition[1][2] = -2.0;
	matrice_transition[1][3] = -1.0;
	
	matrice_transition[2][0] = 0.0;
	matrice_transition[2][1] = 0.0;
	matrice_transition[2][2] = 1.0;
	matrice_transition[2][3] = 0.0;
	
	matrice_transition[3][0] = 1.0;
	matrice_transition[3][1] = 0.0;
	matrice_transition[3][2] = 0.0;
	matrice_transition[3][3] = 0.0;

	// vectors and points
	double** vp = new double*[4];
	vp[0] = p0.toArray();
	vp[1] = p1.toArray();
	vp[2] = v0.toArray();
	vp[3] = v1.toArray();

	free(resultat);
	resultat = new Point[nbU]; // cree tableau de points

	for (int i = 0; i < nbU; ++i) {
		double u = (double)i*(1.0/(double)(nbU-1));
		resultat[i] = multMatriceHermite(u, matrice_transition, vp);
		printf("Point created with coordinates (%f,%f,%f)\n", resultat[i].getX(), resultat[i].getY(), resultat[i].getZ());
	}
}

Point multMatriceHermite(double u, double** m, double** p) {
	// on connait deja les tailles de matrices, etant
	// donne que la hermite cubic est de degre 3
	//
	// u = 1*4 = {x,x,x,x}
	// generee ci dessous
	//     ( x,x,x,x )
	//     ( x,x,x,x )
	// m = ( x,x,x,x ) = 4*4
	//     ( x,x,x,x )
	// donnée
	//     ( x )
	// p = ( x ) = 4*1
	//     ( x )
	//     ( x )
	// chaque donnée de p possède 3 coordonnees x,y,z, 
	// donc p est vraiment comme suit :
	//     ( x,x,x )
	// p = ( x,x,x ) = 4*3
	//     ( x,x,x )
	//     ( x,x,x )
	// donnée
	
	double* m_u = new double[4];
	m_u[0] = pow(u,3);
	m_u[1] = pow(u,2);
	m_u[2] = u;
	m_u[3] = 1.0;

	// stocke u*m
	double* m_um = new double[4];
	m_um[0] = 0.0; m_um[1] = 0.0; m_um[2] = 0.0; m_um[3] = 0.0;
	// fait la multiplication u*m
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			m_um[i] += m_u[j]*m[j][i];
		}
	}
	// donne le resultat final pour x,y,z
	double frx, fry, frz; // coordonnées point final
	for (int i = 0; i < 4; ++i) {
		frx += m_um[i]*p[i][0];
		fry += m_um[i]*p[i][1];
		frz += m_um[i]*p[i][2];
	}

	free(m_u);
	free(m_um);
	return Point(frx,fry,frz);
}

GLvoid DrawGrid(double lowerX, double lowerY, double higherX, double higherY) {
	// Gets the nearest values to draw the grid from :
	double lx = ceil(lowerX);
	double ly = ceil(lowerY);
	double hx = floor(higherX);
	double hy = floor(higherY);

	// Determines how many points to draw and allocates memory:
	int nbPoints = ((int)hx - (int)lx) * ((int)hy - (int)ly);
	Point* gridPoints = new Point[nbPoints];

	int height = (int)hx - (int)lx;
	int width = (int)hy - (int)ly;
	// Creates points to draw :
	for (int x = 0; x < height; ++x) {
		for (int y = 0; y < width; ++y) {
			gridPoints[x*width+y] = Point(x+(int)lx,y+(int)ly,0);
		}
	}

	// Draws the points :
	glColor3f(1.0,1.0,1.0);
	glPointSize(1.0);

	GLdouble x,y,z;

	glBegin(GL_POINTS);
		for(int i = 0; i < nbPoints; ++i) {
			x = gridPoints[i].getX();
			y = gridPoints[i].getY();
			z = gridPoints[i].getZ();
			glVertex3f(x,y,z);
		}
	glEnd();
}