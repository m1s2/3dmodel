#ifndef FUNCTION_HEADERS_H
#define FUNCTION_HEADERS_H

#include "common.h"

/**
 * Sets up the scene.
 */
void initGL();

/**
 * This function is called when the user presses "R" on their keyboard,
 * and begins the actual raytracing process. It calls `findRay()` on 
 * each pixel of the scene, and then it calls `castRay()`, to render the
 * color for that pixel. It also saves the image to disk.
 */
void rayTrace();

/**
 * Does the same job as `rayTrace()`, but multi-threaded !
 */
void rayTrace_MT();

/**
 * This function is a worker thread for `rayTrace_MT()`. It will
 * take in input a pointer to a `threadInfo` struct, converted to
 * `void*` to adhere to C's multithreaded POSIX functions.
 */
void* rayTracing_Thread_Worker(void*);

/**
 * This function takes in input the coordinates of the window's pixel
 * and computes a start point and a direction for the ray to cast from
 * this given pixel. Returns the direction vector, since the camera
 * used here is a single-POV-camera.
 */
std::vector <vec4> findRay(GLdouble x, GLdouble y);

/**
 * Does the actual computation for each of the pixels in the scene.
 * This function is originally called with the results of `findRay()`
 * for each of the pixels in the scene.
 * 
 * This code shall do the following :
 *     - compute the intersection between all objects in the scene
 *         - get the closest one
 *     - cast the corresponding rays for light bouncing around the scene
 *     - return a 4D vector, representing the color in RGBA values of the intersected object !
 */
vec4 castRay(vec4 p0, vec4 E, Object *lastHitObject, int depth);

/**
 * Regularizes the color vector passed to it (puts all coordinates at 1.0 maximum)
 */
vec4 regularizeColor(vec4);

/**
 * The code will check for each object in the scene
 * if the ray from the point of intersection to the
 * light is interrupted. In which case we will return
 * black !
 * 
 * In it's current form, the program also prints out
 * a verbose message of the light path interaction, if
 * one exists and `showInfo` is set to true.
 * 
 * @returns TRUE : the object is shaded by another object
 * @returns FALSE: the path from the object to the light is free
 */
bool shadowFeeler(vec4 p0, Object *object, bool showInfo = false);

/**
 * This function will randomly sample a box around the 
 * "real" light source, `4*nbIter` times and return the
 * proportion of rays in the shade, to provide soft shadows
 * for the final render of the scene.
 */
double shadowFeelerBox_Random(vec4 p0, Object *object, int nbIter);

/**
 * Does the same job as `shadowFeeler_Random()`, but as its name implies,
 * does it in a deterministic fashion to prevent unwanted noise to appear
 * on the image.
 */
double shadowFeelerBox_Deterministic(const vec4 p0, const Object *object, const int nbIter);

/**
 * Function made for debugging purposes. Computes the intersection
 * with all objects in the scene, and prints out the information
 * about which ones it interacts with.
 */
void castRayDebug(vec4 p0, vec4 dir);

/**
 * Does the same job as `castRayDebug()`, but only prints out
 * the closest object in the scene.
 */
void castRayDebugClosest(vec4 p0, vec4 dir);

/**
 * Takes in input a ray, a start position, and a depth, and will simulate the
 * bouncing effect for a ray to use in the final render. It is used to debug
 * the possibly weird bounces of rays I could have when rendering a scene.
 */
void bounceRayVerbose(vec4 p0, vec4 dir, int depth);

/**
 * Scans all the objects in the scene, and returns true if the ray between
 * the 2 points `p0` and `light` is intersected by something (anything,
 * and without distinction of distance from p0).
 */
bool scanAllObjects(const vec4 p0, const vec4 light, const Object* closest);

/**
 * Error callback function for GLFW
 */
static void error_callback(int error, const char* description);

/**
 * Sets up a Cornell box for the current scene
 */
void initCornellBox();

/**
 * Sets up a scene with a single square in it
 */
void initUnitSquare();

/**
 * Sets up a scene with a single sphere in it.
 * [[ currently has 2 for testing purposes ]]
 */
void initUnitSphere();

/**
 * Draws the object for the rasterized interactive version of the scene.
 * Calls upon the power of shaders, to render an image on the screen.
 */
void drawObject(Object * object, GLuint vao, GLuint buffer);

/**
 * Mouse control function for GLFW.
 */
void mouseMove(GLFWwindow* window, double x, double y);

/**
 * Click event handler for GLFW.
 */
static void mouseClick(GLFWwindow* window, int button, int action, int mods);

/**
 * Keyboard event handler for GLFW
 */
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

/**
 * Struct for the multi-threaded version of the `rayTrace()` function.
 * Each worker thread will render only a stripe of the final image,
 * in between indexes `lowerBound` and `lowerBound+heightToRender`.
 */
typedef struct rayTracedThreadInfo {
	unsigned int lowerBound;
	unsigned int heightToRender;
	void* shmAddress;
} threadInfo;

typedef vec4 color4;
typedef vec4 point4;

namespace GLState {
	int window_width, window_height;

	bool render_line;

	std::vector < GLuint > objectVao;
	std::vector < GLuint > objectBuffer;

	GLuint vPosition, vNormal, vTexCoord;

	GLuint program;

	// Model-view and projection matrices uniform location
	GLuint  ModelView, ModelViewLight, NormalMatrix, Projection;

	//==========Trackball Variables==========
	static float curquat[4],lastquat[4];
	/* current transformation matrix */
	static float curmat[4][4];
	mat4 curmat_a;
	/* actual operation  */
	static int scaling;
	static int moving;
	static int panning;
	/* starting "moving" coordinates */
	static int beginx, beginy;
	/* ortho */
	float ortho_x, ortho_y;
	/* current scale factor */
	static float scalefactor;

	mat4  projection;
	mat4 sceneModelView;

	color4 light_ambient;
	color4 light_diffuse;
	color4 light_specular;
};

class rayTraceReceptor : public cmps3120::png_receptor
{
private:
	const unsigned char *buffer;
	unsigned int width;
	unsigned int height;
	int channels;
	
public:
	rayTraceReceptor(const unsigned char *use_buffer, unsigned int width, unsigned int height, int channels){
		this->buffer = use_buffer;
		this->width = width;
		this->height = height;
		this->channels = channels;
	}

	cmps3120::png_header get_header(){
		cmps3120::png_header header;
		header.width = width;
		header.height = height;
		header.bit_depth = 8;
		switch (channels)
		{
			case 1:
			header.color_type = cmps3120::PNG_GRAYSCALE;break;
			case 2:
			header.color_type = cmps3120::PNG_GRAYSCALE_ALPHA;break;
			case 3:
			header.color_type = cmps3120::PNG_RGB;break;
			default:
			header.color_type = cmps3120::PNG_RGBA;break;
		}
		return header;
	}
	
	cmps3120::png_pixel get_pixel(unsigned int x, unsigned int y, unsigned int level){
		cmps3120::png_pixel pixel;
		unsigned int idx = y*width+x;
		/* pngdecode wants 16-bit color values */
		pixel.r = buffer[4*idx]*257;
		pixel.g = buffer[4*idx+1]*257;
		pixel.b = buffer[4*idx+2]*257;
		pixel.a = buffer[4*idx+3]*257;
		return pixel;
	}
};

/* -------------------------------------------------------------------------- */
/* ----------------------  Write Image to Disk  ----------------------------- */
bool write_image(const char* filename, const unsigned char *Src, int Width, int Height, int channels){
	cmps3120::png_encoder the_encoder;
	cmps3120::png_error result;
	rayTraceReceptor image(Src,Width,Height,channels);
	the_encoder.set_receptor(&image);
	result = the_encoder.write_file(filename);
	if (result == cmps3120::PNG_DONE)
		std::cerr << "finished writing "<<filename<<"."<<std::endl;
	else
		std::cerr << "write to "<<filename<<" returned error code "<<result<<"."<<std::endl;
	return result==cmps3120::PNG_DONE;
}

#endif
