#ifndef __ANGEL_VEC_H__
#define __ANGEL_VEC_H__

#include "../include/common.h"

namespace Angel {

	struct vec2 {
		/**
		 * X argument
		 */
		GLfloat  x;

		/**
		 * Y argument
		 */
		GLfloat  y;

		/**
		 * Basic no-argument constructor
		 */
		vec2( GLfloat s = GLfloat(0.0) ) : x(s), y(s) {}
		
		/**
		 * Constructor with arguments
		 */
		vec2( GLfloat x, GLfloat y ) : x(x), y(y) {}

		/**
		 * Copy constructor
		 */
		vec2( const vec2& v ) : x(v.x), y(v.y) {}

		/**
		 * Array access operator. Returns a reference to the object.
		 */
		GLfloat& operator[](int i) {
			return *(&x + i);
		}

		/**
		 * Array access operator. Returns a copy of the object.
		 */
		const GLfloat operator[](int i) const {
			return *(&x + i);
		}

		/**
		 * Unary minus operator. Returns the opposite vector.
		 */
		vec2 operator- () const {
			return vec2( -x, -y);
		}

		/**
		 * Addition operator. Returns a new vector 
		 * with their coordinates added together.
		 */
		vec2 operator+ (const vec2& v) const {
			return vec2( x + v.x, y + v.y );
		}
		
		/**
		 * Binary minus operator. Returns a new vector
		 * with their coordinates substracted from one another.
		 */
		vec2 operator- (const vec2& v) const {
			return vec2( x - v.x, y - v.y );
		}

		/**
		 * Scalar multiplication. Returns a new vector.
		 */
		vec2 operator* (const GLfloat s) const { 
			return vec2( s*x, s*y );
		}

		/**
		 * Vector element-wise multiplication.
		 * Returns a new vector.
		 */
		vec2 operator* (const vec2& v) const {
			return vec2( x*v.x, y*v.y );
		}

		/**
		 * Scalar multiplication, when invoqued  
		 * using a float value first in line.
		 */
		friend vec2 operator* (const GLfloat s, const vec2& v) {
			#ifdef DEBUG
				if ( std::fabs(s) < DivideByZeroTolerance ) {
					std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
						<< "Division by zero" << std::endl;
					return vec2();
				}
			#endif // DEBUG

			GLfloat r = GLfloat(1.0) / s;
			return v * r;
		}

		/**
		 * Scalar division. Returns a new vector.
		 */
		vec2 operator/ (const GLfloat s) const;

		/**
		 * Vector addition. Returns a new vector.
		 */
		vec2& operator+=(const vec2& v) {
			x += v.x;
			y += v.y;
			return *this;
		}

		/**
		 * Substracts the elements of the second vector to the first.
		 * Returns a new vector.
		 */
		vec2& operator-=(const vec2& v) {
			x -= v.x;
			y -= v.y;
			return *this;
		}

		/**
		 * Scalar multiplication. Returns a new vector.
		 */
		vec2& operator*=(const GLfloat s) {
			x *= s;
			y *= s;
			return *this;
		}

		/**
		 * Vector element-wise multiplication. Returns a new vector.
		 */
		vec2& operator*=(const vec2& v) {
			x *= v.x;
			y *= v.y;
			return *this;
		}

		/**
		 * Element-wise division by a scalar S. Retusn a new vector.
		 */
		vec2& operator/=(const GLfloat s) {
			#ifdef DEBUG
				if ( std::fabs(s) < DivideByZeroTolerance ) {
					std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
						<< "Division by zero" << std::endl;
				}
			#endif // DEBUG

			GLfloat r = GLfloat(1.0) / s;
			*this *= r;

			return *this;
		}

		/**
		 * Allows for output of the vector's coordinates.
		 */
		friend std::ostream& operator<<(std::ostream& os, const vec2& v) {
			return os << "( " << v.x << ", " << v.y <<  " )";
		}

		/**
		 * Allows for input of the vector coordinates into their fields
		 */
		friend std::istream& operator>>(std::istream& is, vec2& v) {
			return is >> v.x >> v.y;
		}

		/**
		 * Explicit cast to float pointer for THE X VALUE
		 */
		operator const GLfloat* () const {
			return static_cast<const GLfloat*>( &x );
		}

		/**
		 * Explicit cast to float pointer for THE X VALUE. 
		 * Allows for a change of that value.
		 */
		operator GLfloat* () {
			return static_cast<GLfloat*>( &x );
		}
	};

	/**
	 * Dot product of two vectors.
	 */
	inline
	GLfloat dot( const vec2& u, const vec2& v ) {
		return u.x * v.x + u.y * v.y;
	}

	/**
	 * Returns the length of a vector in floating-point value.
	 */
	inline
	GLfloat length( const vec2& v ) {
		return std::sqrt(Angel::dot(v,v));
	}

	/**
	 * Returns the normalised version of the given vector.
	 */
	inline
	vec2 normalize( const vec2& v ) {
		return v / Angel::length(v);
	}

	struct vec3 {
		GLfloat  x;
		GLfloat  y;
		GLfloat  z;

		/**
		 * Simple no-argument constructor.
		 */
		vec3( GLfloat s = GLfloat(0.0) ) : x(s), y(s), z(s) {}

		/**
		 * Trivial constructor for three dimensional vector
		 */
		vec3( GLfloat x, GLfloat y, GLfloat z ) : x(x), y(y), z(z) {}

		/**
		 * Copy constructor for three dimensional vector
		 */
		vec3( const vec3& v ) : x(v.x), y(v.y), z(v.z) {}

		/**
		 * Constructor allowing an "upcast" from 2d to 3d
		 */
		vec3( const vec2& v, const float f ) : x(v.x), y(v.y), z(f) {}

		/**
		 * Array access for three dimensional vectors.
		 */
		GLfloat& operator[](int i) {
			return *(&x + i);
		}

		/**
		 * Read-only array access for three dimensional vectors
		 */
		const GLfloat operator[](int i) const {
			return *(&x + i);
		}

		/**
		 * Unary minus operator. Reverses the vector.
		 */
		vec3 operator- () const {
			return vec3( -x, -y, -z );
		}

		/**
		 * Vector addition.
		 */
		vec3 operator+ (const vec3& v) const {
			return vec3( x + v.x, y + v.y, z + v.z );
		}

		/**
		 * Element-wise vector substraction
		 */
		vec3 operator- (const vec3& v) const {
			return vec3( x - v.x, y - v.y, z - v.z );
		}

		/**
		 * Scales the vector by a factor of `s`.
		 */
		vec3 operator* (const GLfloat s) const {
			return vec3( s*x, s*y, s*z );
		}

		/**
		 * Element-wise vector multiplication
		 */
		vec3 operator* (const vec3& v) const {
			return vec3( x*v.x, y*v.y, z*v.z );
		}

		/**
		 * Operator for when the scalar is put before the vector.
		 */
		friend vec3 operator* (const GLfloat s, const vec3& v) {
			return v * s;
		}

		/**
		 * Element wise division by a factor of `s`.
		 */
		vec3 operator/ (const GLfloat s) const {
			#ifdef DEBUG
				if ( std::fabs(s) < DivideByZeroTolerance ) {
					std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
						<< "Division by zero" << std::endl;
					return vec3();
				}
			#endif // DEBUG

			GLfloat r = GLfloat(1.0) / s;
			return *this * r;
		}

		/**
		 * Element wise addition of vectors
		 */
		vec3& operator+=(const vec3& v) {
			x += v.x;
			y += v.y;
			z += v.z;
			return *this;
		}

		/**
		 * Element wise substraction of vectors.
		 */
		vec3& operator-=(const vec3& v) {
			x -= v.x;
			y -= v.y;
			z -= v.z;
			return *this;
		}

		/**
		 * Scaling operation on vector by a factor of `s`
		 */
		vec3& operator*=(const GLfloat s) {
			x *= s;
			y *= s;
			z *= s;
			return *this;
		}

		/**
		 * Element-wise multiplication of vectors.
		 */
		vec3& operator*=(const vec3& v) {
			x *= v.x;
			y *= v.y;
			z *= v.z;
			return *this;
		}

		/**
		 * Shrink operation by a factor of `s`
		 */
		vec3& operator/=(const GLfloat s) {
			#ifdef DEBUG
				if ( std::fabs(s) < DivideByZeroTolerance ) {
					std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
						<< "Division by zero" << std::endl;
				}
			#endif // DEBUG

			GLfloat r = GLfloat(1.0) / s;
			*this *= r;
			return *this;
		}

		/**
		 * Output operator.
		 */
		friend std::ostream& operator<<(std::ostream& os, const vec3& v) {
			return os << "( " << v.x << ", " << v.y << ", " << v.z <<  " )";
		}

		/**
		 * Input operator.
		 */
		friend std::istream& operator>> ( std::istream& is, vec3& v ) {
			return is >> v.x >> v.y >> v.z ;
		}

		/**
		 * Explicit casting operator FOR X PARAMETER
		 */
		operator const GLfloat* () const {
			return static_cast<const GLfloat*>(&x);
		}

		/**
		 * Explicit casting operator FOR X PARAMETER
		 */
		operator GLfloat* () {
			return static_cast<GLfloat *>(&x);
		}
	};

	inline
	GLfloat dot( const vec3& u, const vec3& v ) {
		return u.x*v.x + u.y*v.y + u.z*v.z ;
	}

	inline
	GLfloat length( const vec3& v ) {
		return std::sqrt(Angel::dot(v,v) );
	}

	inline
	vec3 normalize( const vec3& v ) {
		return v / Angel::length(v);
	}

	inline
	vec3 cross(const vec3& a, const vec3& b ) {
		return vec3( a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x );
	}
	
	inline
	vec3 reflect( const vec3& I, const vec3& N ) {
		return I - 2.0 * Angel::dot(N, I) * N;
	}



	//////////////////////////////////////////////////////////////////////////////
	//
	//  vec4 - 4D vector
	//
	//////////////////////////////////////////////////////////////////////////////

	struct vec4 {

		GLfloat  x;
		GLfloat  y;
		GLfloat  z;
		GLfloat  w;

		/**
		 * Simple, no-argument constructor.
		 * All coordinates are initialized at `s`.
		 */
		vec4( GLfloat s = GLfloat(0.0) ) : x(s), y(s), z(s), w(s) {}

		/**
		 * Full constructor for a custom 4D vector.
		 */
		vec4( GLfloat x, GLfloat y, GLfloat z, GLfloat w ) : x(x), y(y), z(z), w(w) {}

		/**
		 * Copy constructor.
		 */
		vec4( const vec4& v ) : x(v.x), y(v.y), z(v.z), w(v.w) {}

		/**
		 * Constructor creating a 4D vector from a 3D one.
		 */
		vec4( const vec3& v, const float w = 1.0 ) : x(v.x), y(v.y), z(v.z), w(w) {}

		/**
		 * Constructor creating a 4D vector from a 2D one.
		 */
		vec4( const vec2& v, const float z, const float w ) : x(v.x), y(v.y), z(z), w(w) {}

		/**
		 * Array accessor to the struct's data.
		 */
		GLfloat& operator[](int i) {
			return *(&x + i);
		}

		/**
		 * Array accessor to the struct's data. READ-ONLY
		 */
		const GLfloat operator[](int i) const { 
			return *(&x + i);
		}

		/**
		 * Unary minus operator. Creates a new vector opposite this one.
		 */
		vec4 operator- () const {
			return vec4( -x, -y, -z, -w );
		}

		/**
		 * 
		 */
		vec4 operator+ (const vec4& v) const { 
			return vec4( x + v.x, y + v.y, z + v.z, w + v.w );
		}

		/**
		 * 
		 */
		vec4 operator- (const vec4& v) const {
			return vec4( x - v.x, y - v.y, z - v.z, w - v.w );
		}

		/**
		 * 
		 */
		vec4 operator* (const GLfloat s) const {
			return vec4( s*x, s*y, s*z, s*w );
		}

		/**
		 * 
		 */
		vec4 operator* (const vec4& v) const {
			return vec4( x*v.x, y*v.y, z*v.z, w*v.z );
		}

		/**
		 * 
		 */
		friend vec4 operator* (const GLfloat s, const vec4& v) {
			return v * s;
		}

		/**
		 * 
		 */
		vec4 operator/ (const GLfloat s) const {
			#ifdef DEBUG
				if ( std::fabs(s) < DivideByZeroTolerance ) {
					std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
						<< "Division by zero" << std::endl;
					return vec4();
				}
			#endif // DEBUG

			GLfloat r = GLfloat(1.0) / s;
			return *this * r;
		}

		/**
		 * 
		 */
		vec4& operator+=(const vec4& v) {
			x += v.x;
			y += v.y;
			z += v.z;
			w += v.w;
			return *this;
		}

		/**
		 * 
		 */
		vec4& operator-=(const vec4& v) {
			x -= v.x;
			y -= v.y;
			z -= v.z;
			w -= v.w;
			return *this;
		}

		/**
		 * 
		 */
		vec4& operator*= ( const GLfloat s ) {
			x *= s;
			y *= s;
			z *= s;
			w *= s;
			return *this;
		}

		/**
		 * 
		 */
		vec4& operator*= ( const vec4& v ) {
			x *= v.x;
			y *= v.y;
			z *= v.z;
			w *= v.w;
			return *this;
		}

		/**
		 * 
		 */
		vec4& operator/= ( const GLfloat s ) {
			#ifdef DEBUG
				if ( std::fabs(s) < DivideByZeroTolerance ) {
					std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
						<< "Division by zero" << std::endl;
				}
			#endif // DEBUG

			GLfloat r = GLfloat(1.0) / s;
			*this *= r;

			return *this;
		}

		/**
		 * 
		 */
		friend std::ostream& operator<< ( std::ostream& os, const vec4& v ) {
			return os << "( " << v.x << ", " << v.y << ", " << v.z << ", " << v.w << " )";
		}

		/**
		 * 
		 */
		friend std::istream& operator>> ( std::istream& is, vec4& v ) {
			return is >> v.x >> v.y >> v.z >> v.w; 
		}

		/**
		 * 
		 */
		operator const GLfloat* () const {
			return static_cast<const GLfloat*>( &x );
		}

		/**
		 * 
		 */
		operator GLfloat* () {
			return static_cast<GLfloat*>( &x );
		}
	};

	//----------------------------------------------------------------------------
	//
	//  Non-class vec4 Methods
	//

	inline
	GLfloat dot(const vec4& u, const vec4& v) {
		return u.x*v.x + u.y*v.y + u.z*v.z + u.w+v.w;
	}

	inline
	GLfloat length(const vec4& v) {
		return std::sqrt( Angel::dot(v,v) );
	}

	inline
	vec4 normalize(const vec4& v) {
		return v / Angel::length(v);
	}

	inline
	vec3 cross(const vec4& a, const vec4& b) {
		return vec3( a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x );
	}
	
	inline
	vec4 reflect(const vec4& I, const vec4& N) {
		return I - 2.0 * Angel::dot(N, I) * N;
	}

}  // namespace Angel

#endif // __ANGEL_VEC_H__
