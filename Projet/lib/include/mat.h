//////////////////////////////////////////////////////////////////////////////
//
//  --- mat.h ---
//
//////////////////////////////////////////////////////////////////////////////

#ifndef __ANGEL_MAT_H__
#define __ANGEL_MAT_H__

#include "../include/common.h"

namespace Angel {

	class mat2 {
		vec2  _m[2];
		public:
			/**
			 * Creates a diagonal matrix
			 */
			mat2(const GLfloat d = GLfloat(1.0)) { 
				_m[0].x = d;
				_m[1].y = d;
			}

			/**
			 * Creates a 2x2 matrix using 2 vectors.
			 */
			mat2(const vec2& a, const vec2& b) {
				_m[0] = a;
				_m[1] = b;
			}

			/**
			 * Create a matrix using 4 floating point values.
			 */
			mat2(GLfloat m00, GLfloat m10, GLfloat m01, GLfloat m11) {
				_m[0] = Angel::vec2( m00, m10 );
				_m[1] = Angel::vec2( m01, m11 );
			}

			/**
			 * Copy constructor for a mat2
			 */
			mat2(const mat2& m) {
				if ( *this != m ) {
					_m[0] = m._m[0];
					_m[1] = m._m[1];
				} 
			}

			/**
			 * Array accessor to a line of the matrix.
			 */
			vec2& operator[](int i) {
				return _m[i];
			}

			/**
			 * Non-mutable array accessor for a mat2.
			 */
			const vec2& operator[](int i) const {
				return _m[i];
			}

			/**
			 * Addition of two matrices.
			 */
			mat2 operator+ (const mat2& m) const {
				return Angel::mat2( _m[0]+m[0], _m[1]+m[1] );
			}

			/**
			 * Element-wise substraction of a matrix's coordinates.
			 */
			mat2 operator- (const mat2& m) const {
				return Angel::mat2( _m[0]-m[0], _m[1]-m[1] );
			}

			/**
			 * Scales a matrix's data by a factor of `s`
			 */
			mat2 operator* (const GLfloat s) const {
				return Angel::mat2( s*_m[0], s*_m[1] );
			}

			/**
			 * Divides each of the matrix's elements by a factor of `s`
			 */
			mat2 operator/ (const GLfloat s) const {
				#ifdef DEBUG
					if ( std::fabs(s) < DivideByZeroTolerance ) {
						std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
							<< "Division by zero" << std::endl;
						return mat2();
					}
				#endif // DEBUG
				
				GLfloat r = GLfloat(1.0) / s;
				return *this * r;
			}

			/**
			 * Scales a matrix's data by a factor of `s`
			 */
			friend mat2 operator* (const GLfloat s, const mat2& m) {
				return m * s;
			}
			
			/**
			 * Matrix multiplication
			 */
			mat2 operator* (const mat2& m) const {
				mat2  a( 0.0 );

				for ( int i = 0; i < 2; ++i ) {
					for ( int j = 0; j < 2; ++j ) {
						for ( int k = 0; k < 2; ++k ) {
							a[i][j] += _m[i][k] * m[k][j];
						}
					}
				}

				return a;
			}

			/**
			 * Matrix addition (element-wise)
			 */
			mat2& operator+=(const mat2& m) {
				_m[0] += m[0];
				_m[1] += m[1];
				return *this;
			}

			/**
			 * Matrix substraction (element-wise)
			 */
			mat2& operator-=(const mat2& m) {
				_m[0] -= m[0];
				_m[1] -= m[1];
				return *this;
			}

			/**
			 * Matrix will be scaled by a factor of `s`
			 */
			mat2& operator*=(const GLfloat s) {
				_m[0] *= s;
				_m[1] *= s;
				return *this;
			}

			/**
			 * Matrix multiplication.
			 */
			mat2& operator*=(const mat2& m) {
				Angel::mat2  a( 0.0 );

				for ( int i = 0; i < 2; ++i ) {
					for ( int j = 0; j < 2; ++j ) {
						for ( int k = 0; k < 2; ++k ) {
							a[i][j] += _m[i][k] * m[k][j];
						}
					}
				}

				return 	*this = a;
			}
			
			/**
			 * Divides the matrix's elements by a factor of `s`
			 */
			mat2& operator/=(const GLfloat s) {
				#ifdef DEBUG
					if ( std::fabs(s) < DivideByZeroTolerance ) {
						std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
							<< "Division by zero" << std::endl;
						return mat2();
					}
				#endif // DEBUG

				GLfloat r = GLfloat(1.0) / s;
				return *this *= r;
			}

			/**
			 * Matrix will be multiplied by the vector `v`
			 */
			vec2 operator* (const vec2& v) const {
				return Angel::vec2( _m[0][0]*v.x + _m[0][1]*v.y, _m[1][0]*v.x + _m[1][1]*v.y );
			}

			/**
			 * output operator
			 */	
			friend std::ostream& operator<<(std::ostream& os, const mat2& m) {
				return os << std::endl << m[0] << std::endl << m[1] << std::endl;
			}

			/**
			 * input operator.
			 * 
			 * This operator is here to gain access to the private 
			 * vectors of the class. For some reason, it is accessible
			 * here (by a non-class function) but not from any other
			 * file where the code is stored on ¯\_(ツ)_/¯
			 */
			friend std::istream& operator>>(std::istream& is, mat2& m){
				return is >> m._m[0] >> m._m[1];
			}

			/**
			 * Explicit casting operator. Returns a non-mutable 
			 * floating point pointer to the value located at mat2[0][0].
			 */
			operator const GLfloat* () const {
				return static_cast<const GLfloat*>(&_m[0].x);
			}

			/**
			 * Explicit casting operator. Returns a floating point pointer
			 * to the value located at mat2[0][0].
			 */
			operator GLfloat* () { 
				return static_cast<GLfloat*>( &_m[0].x );
			}
	};

	/**
	 * 
	 */
	inline
	mat2 matrixCompMult(const mat2& A, const mat2& B) {
		return mat2( 
			A[0][0]*B[0][0], A[0][1]*B[0][1], 
			A[1][0]*B[1][0], A[1][1]*B[1][1] );
	}

	/**
	 * 
	 */
	inline
	mat2 transpose(const mat2& A) {
		return mat2( 
			A[0][0], A[1][0],
			A[0][1], A[1][1] );
	}

	class mat3 {
		vec3  _m[3];
		public:
			/**
			 * Default constructor. Creates a diagonal matrix.
			 */
			mat3(const GLfloat d = GLfloat(1.0)) {
				_m[0].x = d;
				_m[1].y = d;
				_m[2].z = d;
			}

			/**
			 * Contructor assigning 3 1x3 vectors to a 3x3 matrix.
			 */
			mat3(const vec3& a, const vec3& b, const vec3& c) {
				_m[0] = a;
				_m[1] = b;
				_m[2] = c;
			}

			/**
			 * Constructor defining each value of the matrix.
			 */
			mat3(	GLfloat m00, GLfloat m10, GLfloat m20,
				GLfloat m01, GLfloat m11, GLfloat m21,
				GLfloat m02, GLfloat m12, GLfloat m22) {
				_m[0] = Angel::vec3( m00, m10, m20 );
				_m[1] = Angel::vec3( m01, m11, m21 );
				_m[2] = Angel::vec3( m02, m12, m22 );
				// _m[0] = Angel::vec3( m00, m01, m02 );
				// _m[1] = Angel::vec3( m10, m11, m12 );
				// _m[2] = Angel::vec3( m20, m21, m22 );
			}

			/**
			 * Copy constructor.
			 */
			mat3(const mat3& m) {
				if ( *this != m ) {
					_m[0] = m._m[0];
					_m[1] = m._m[1];
					_m[2] = m._m[2];
				} 
			}

			/**
			 * Array access to the rows of the matrix.
			 */
			vec3& operator[](int i) { 
				return _m[i];
			}

			/**
			 * Non-mutable array access to the rows of the matrix
			 */
			const vec3& operator[](int i) const {
				return _m[i];
			}

			/**
			 * Matrix addition
			 */
			mat3 operator+ (const mat3& m) const {
				return Angel::mat3( 
					_m[0]+m[0], 
					_m[1]+m[1], 
					_m[2]+m[2]);
			}

			/**
			 * Matrix substraction
			 */
			mat3 operator- (const mat3& m) const {
				return Angel::mat3( 
					_m[0]-m[0],
					_m[1]-m[1],
					_m[2]-m[2] );
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `s`
			 */
			mat3 operator* (const GLfloat s) const { 
				return Angel::mat3( s*_m[0], s*_m[1], s*_m[2] ); 
			}

			/**
			 * Each one of the matrix's elements will be divided by `s`
			 */
			mat3 operator/ (const GLfloat s) const {
				#ifdef DEBUG
					if ( std::fabs(s) < DivideByZeroTolerance ) {
						std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
							<< "Division by zero" << std::endl;
						return Angel::mat3();
					}
				#endif // DEBUG
				
				GLfloat r = GLfloat(1.0) / s;
				return *this * r;
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `s`
			 */
			friend mat3 operator* (const GLfloat s, const mat3& m) {
				return m * s;
			}

			/**
			 * Matrix multiplication
			 */
			mat3 operator* (const mat3& m) const {
				Angel::mat3  a( 0.0 );

				for ( int i = 0; i < 3; ++i ) {
					for ( int j = 0; j < 3; ++j ) {
						for ( int k = 0; k < 3; ++k ) {
							a[i][j] += _m[i][k] * m[k][j];
						}
					}
				}

				return a;
			}

			/**
			 * Matrix addition
			 */
			mat3& operator+=(const mat3& m) {
				_m[0] += m[0];
				_m[1] += m[1];
				_m[2] += m[2];
				return *this;
			}

			/**
			 * Matrix substraction
			 */
			mat3& operator-=(const mat3& m) {
				_m[0] -= m[0];
				_m[1] -= m[1];
				_m[2] -= m[2];
				return *this;
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `s`
			 */
			mat3& operator*=(const GLfloat s) {
				_m[0] *= s;
				_m[1] *= s;
				_m[2] *= s;
				return *this;
			}

			/**
			 * Matrix multiplication
			 */
			mat3& operator*=(const mat3& m) {
				Angel::mat3  a( 0.0 );

				for ( int i = 0; i < 3; ++i ) {
					for ( int j = 0; j < 3; ++j ) {
						for ( int k = 0; k < 3; ++k ) {
							a[i][j] += _m[i][k] * m[k][j];
						}
					}
				}

				return *this = a;
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `1/s`
			 */
			mat3& operator/=(const GLfloat s) {
				#ifdef DEBUG
					if ( std::fabs(s) < DivideByZeroTolerance ) {
						std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
							<< "Division by zero" << std::endl;
						return Angel::mat3();
					}
				#endif // DEBUG

				GLfloat r = GLfloat(1.0) / s;
				return *this *= r;
			}

			/**
			 * Multiplies the matrix by vector V
			 */
			vec3 operator* (const vec3& v) const {
				return Angel::vec3(
					_m[0][0]*v.x + _m[0][1]*v.y + _m[0][2]*v.z,
					_m[1][0]*v.x + _m[1][1]*v.y + _m[1][2]*v.z,
					_m[2][0]*v.x + _m[2][1]*v.y + _m[2][2]*v.z );
			}

			/**
			 * Output operator
			 */
			friend std::ostream& operator<<(std::ostream& os, const mat3& m) {
				return os << std::endl << m[0] << std::endl << m[1] << std::endl << m[2] << std::endl;
			}

			/**
			 * input operator.
			 */
			friend std::istream& operator>>(std::istream& is, mat3& m) {
				return is >> m._m[0] >> m._m[1] >> m._m[2] ;
			}

			/**
			 * Explicit casting operator. Returns a non-mutable 
			 * floating point pointer to the value located at mat3[0][0].
			 */
			operator const GLfloat* () const {
				return static_cast<const GLfloat*>( &_m[0].x );
			}

			/**
			 * Explicit casting operator. Returns a floating point 
			 * pointer to the value located at mat3[0][0].
			 */
			operator GLfloat* () {
				return static_cast<GLfloat*>( &_m[0].x );
			}
	};

	inline
	mat3 matrixCompMult(const mat3& A, const mat3& B) {
		return Angel::mat3( A[0][0]*B[0][0], A[0][1]*B[0][1], A[0][2]*B[0][2],
			A[1][0]*B[1][0], A[1][1]*B[1][1], A[1][2]*B[1][2],
			A[2][0]*B[2][0], A[2][1]*B[2][1], A[2][2]*B[2][2] );
	}

	inline
	mat3 transpose(const mat3& A) {
		return Angel::mat3( A[0][0], A[1][0], A[2][0],
			A[0][1], A[1][1], A[2][1],
			A[0][2], A[1][2], A[2][2] );
	}

	class mat4 {
		vec4  _m[4];
		public:
			/**
			 * Creates a diagonal matrix
			 */
			mat4( const GLfloat d = GLfloat(1.0))  {
				_m[0].x = d;
				_m[1].y = d;
				_m[2].z = d;
				_m[3].w = d;
			}

			/**
			 * Creates a four dimensional matrix using 4 4D vectors
			 */
			mat4(const vec4& a, const vec4& b, const vec4& c, const vec4& d) {
				_m[0] = a;
				_m[1] = b;
				_m[2] = c;
				_m[3] = d;
			}

			/**
			 * Creates a 4D matrix using all the necessary values to fill it
			 */
			mat4(GLfloat m00, GLfloat m10, GLfloat m20, GLfloat m30,
			GLfloat m01, GLfloat m11, GLfloat m21, GLfloat m31,
			GLfloat m02, GLfloat m12, GLfloat m22, GLfloat m32,
			GLfloat m03, GLfloat m13, GLfloat m23, GLfloat m33) {
				_m[0] = Angel::vec4( m00, m10, m20, m30 );
				_m[1] = Angel::vec4( m01, m11, m21, m31 );
				_m[2] = Angel::vec4( m02, m12, m22, m32 );
				_m[3] = Angel::vec4( m03, m13, m23, m33 );
			}

			/**
			 * Copy constructor
			 */
			mat4(const mat4& m) {
				if ( *this != m ) {
					_m[0] = m._m[0];
					_m[1] = m._m[1];
					_m[2] = m._m[2];
					_m[3] = m._m[3];
				}
			}

			/**
			 * Array accessor to the matrix
			 */
			vec4& operator[](int i) {
				return _m[i];
			}

			/**
			 * Non-mutable array accessor to the matrix
			 */
			const vec4& operator[](int i) const {
				return _m[i];
			}

			/**
			 * Matrix addition
			 */
			mat4 operator+ (const mat4& m) const { 
				return Angel::mat4( _m[0]+m[0], _m[1]+m[1], _m[2]+m[2], _m[3]+m[3] );
			}

			/**
			 * Matrix substraction
			 */
			mat4 operator- (const mat4& m) const {
				return Angel::mat4( _m[0]-m[0], _m[1]-m[1], _m[2]-m[2], _m[3]-m[3] );
			}

			/**
			 * Each one of the matrix's elements will be scaled by `s`
			 */
			mat4 operator* (const GLfloat s) const {
				return Angel::mat4( s*_m[0], s*_m[1], s*_m[2], s*_m[3] );
			}

			/**
			 * Each one of the matrix's elements will be divided by `s`
			 */
			mat4 operator/ (const GLfloat s) const {
				#ifdef DEBUG
					if ( std::fabs(s) < DivideByZeroTolerance ) {
						std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
							<< "Division by zero" << std::endl;
						return Angel::mat4();
					}
				#endif // DEBUG
				
				GLfloat r = GLfloat(1.0) / s;
				return *this * r;
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `s`
			 */
			friend mat4 operator* (const GLfloat s, const mat4& m) {
				return m * s;
			}

			/**
			 * Matrix multiplication
			 */
			mat4 operator* (const mat4& m) const {
				Angel::mat4  a( 0.0 );

				for ( int i = 0; i < 4; ++i ) {
					for ( int j = 0; j < 4; ++j ) {
						for ( int k = 0; k < 4; ++k ) {
							a[i][j] += _m[i][k] * m[k][j];
						}
					}
				}

				return a;
			}

			/**
			 * Matrix addition
			 */
			mat4& operator+=(const mat4& m) {
				_m[0] += m[0];
				_m[1] += m[1];
				_m[2] += m[2];
				_m[3] += m[3];
				return *this;
			}

			/**
			 * Matrix substraction
			 */
			mat4& operator-=(const mat4& m) {
				_m[0] -= m[0];
				_m[1] -= m[1];
				_m[2] -= m[2];
				_m[3] -= m[3];
				return *this;
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `s`
			 */
			mat4& operator*=(const GLfloat s) {
				_m[0] *= s;
				_m[1] *= s;
				_m[2] *= s;
				_m[3] *= s;
				return *this;
			}

			/**
			 * Matrix multiplication
			 */
			mat4& operator*=(const mat4& m) {
				Angel::mat4  a( 0.0 );

				for ( int i = 0; i < 4; ++i ) {
					for ( int j = 0; j < 4; ++j ) {
						for ( int k = 0; k < 4; ++k ) {
							a[i][j] += _m[i][k] * m[k][j];
						}
					}
				}

				return *this = a;
			}

			/**
			 * Each one of the matrix's elements will be multiplied by `1/s`
			 */
			mat4& operator/=(const GLfloat s) {
				#ifdef DEBUG
					if ( std::fabs(s) < DivideByZeroTolerance ) {
						std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
							<< "Division by zero" << std::endl;
						return Angel::mat4();
					}
				#endif // DEBUG

				GLfloat r = GLfloat(1.0) / s;
				return *this *= r;
			}

			/**
			 * Mutliplies the 4D matrix by the 4D vector V
			 */
			vec4 operator* (const vec4& v) const {  // m * v
				return Angel::vec4(
					_m[0][0]*v.x + _m[0][1]*v.y + _m[0][2]*v.z + _m[0][3]*v.w,
					_m[1][0]*v.x + _m[1][1]*v.y + _m[1][2]*v.z + _m[1][3]*v.w,
					_m[2][0]*v.x + _m[2][1]*v.y + _m[2][2]*v.z + _m[2][3]*v.w,
					_m[3][0]*v.x + _m[3][1]*v.y + _m[3][2]*v.z + _m[3][3]*v.w
				);
			}

			/**
			 * Output operator
			 */
			friend std::ostream& operator<<(std::ostream& os, const mat4& m) {
				return os << std::endl << m[0] << std::endl << m[1] << std::endl << m[2] << std::endl << m[3] << std::endl;
			}

			/**
			 * Input operator
			 */
			friend std::istream& operator>>(std::istream& is, mat4& m) {
				return is >> m._m[0] >> m._m[1] >> m._m[2] >> m._m[3];
			}

			/**
			 * Explicit casting operator. Returns a non-mutable 
			 * floating point pointer to the value located at mat4[0][0].
			 */
			operator const GLfloat* () const {
				return static_cast<const GLfloat*>( &_m[0].x );
			}

			/**
			 * Explicit casting operator. Returns a floating point 
			 * pointer to the value located at mat4[0][0].
			 */
			operator GLfloat* () {
				return static_cast<GLfloat*>(&_m[0].x);
			}
	};

	inline
	mat4 matrixCompMult( const mat4& A, const mat4& B ) {
		return Angel::mat4(
		A[0][0]*B[0][0], A[0][1]*B[0][1], A[0][2]*B[0][2], A[0][3]*B[0][3],
		A[1][0]*B[1][0], A[1][1]*B[1][1], A[1][2]*B[1][2], A[1][3]*B[1][3],
		A[2][0]*B[2][0], A[2][1]*B[2][1], A[2][2]*B[2][2], A[2][3]*B[2][3],
		A[3][0]*B[3][0], A[3][1]*B[3][1], A[3][2]*B[3][2], A[3][3]*B[3][3] );
	}

	inline
	mat4 transpose( const mat4& A ) {
		return Angel::mat4( A[0][0], A[1][0], A[2][0], A[3][0],
			A[0][1], A[1][1], A[2][1], A[3][1],
			A[0][2], A[1][2], A[2][2], A[3][2],
			A[0][3], A[1][3], A[2][3], A[3][3] );
	}

	#define Error( str ) do { std::cerr << "[" __FILE__ ":" << __LINE__ << "] " \
	<< str << std::endl; } while(0)

	/**
	 * Multiplies two matrices
	 */
	inline
	vec4 mvmult( const mat4& a, const vec4& b ) {
		Error( "replace with vector matrix multiplcation operator" );

		Angel::vec4 c;
		int i, j;
		for(i=0; i<4; i++) {
			c[i] =0.0;
			for(j=0;j<4;j++) c[i]+=a[i][j]*b[j];
		}
		return c;
	}

	/**
	 * Creates a rotation matrix
	 */
	inline
	mat4 RotateX( const GLfloat theta ) {
		GLfloat angle = DegreesToRadians * theta;

		Angel::mat4 c;
		c[2][2] = c[1][1] = cos(angle);
		c[2][1] = sin(angle);
		c[1][2] = -c[2][1];
		return c;
	}

	/**
	 * Creates a rotation matrix
	 */
	inline
	mat4 RotateY( const GLfloat theta ) {
		GLfloat angle = DegreesToRadians * theta;

		Angel::mat4 c;
		c[2][2] = c[0][0] = cos(angle);
		c[0][2] = sin(angle);
		c[2][0] = -c[0][2];
		return c;
	}

	/**
	 * Creates a rotation matrix
	 */
	inline
	mat4 RotateZ( const GLfloat theta ) {
		GLfloat angle = DegreesToRadians * theta;

		Angel::mat4 c;
		c[0][0] = c[1][1] = cos(angle);
		c[1][0] = sin(angle);
		c[0][1] = -c[1][0];
		return c;
	}

	/**
	 * Creates a translation matrix
	 */
	inline
	mat4 Translate(const GLfloat x, const GLfloat y, const GLfloat z) {
		Angel::mat4 c;
		c[0][3] = x;
		c[1][3] = y;
		c[2][3] = z;
		return c;
	}

	/**
	 * Creates a translation matrix
	 */
	inline
	mat4 Translate(const vec3& v) {
		return Angel::Translate( v.x, v.y, v.z );
	}

	/**
	 * Creates a translation matrix
	 */
	inline
	mat4 Translate(const vec4& v) {
		return Angel::Translate( v.x, v.y, v.z );
	}

	/**
	 * Creates a scaling matrix
	 */
	inline
	mat4 Scale(const GLfloat x, const GLfloat y, const GLfloat z) {
		Angel::mat4 c;
		c[0][0] = x;
		c[1][1] = y;
		c[2][2] = z;
		return c;
	}

	/**
	 * Creates a scaling matrix
	 */
	inline
	mat4 Scale(const vec3& v) {
		return Angel::Scale( v.x, v.y, v.z );
	}

	/**
	 * Ortho transformation matrix generator.
	 * 
	 * Note: Microsoft Windows (r) defines the keyword 
	 * "far" in C/C++. In order to avoid any name conflicts,
	 * we use the variable names "zNear" to reprsent "near", 
	 * and "zFar" to reprsent "far".
	 */
	inline
	mat4 Ortho( const GLfloat left, const GLfloat right,
		const GLfloat bottom, const GLfloat top,
		const GLfloat zNear, const GLfloat zFar ) {
		Angel::mat4 c;
		c[0][0] = 2.0/(right - left);
		c[1][1] = 2.0/(top - bottom);
		c[2][2] = 2.0/(zNear - zFar);
		c[3][3] = 1.0;
		c[0][3] = -(right + left)/(right - left);
		c[1][3] = -(top + bottom)/(top - bottom);
		c[2][3] = -(zFar + zNear)/(zFar - zNear);
		return c;
	}

	/**
	 * 2D ortho transformation matrix generator
	 * 
	 * Note: Microsoft Windows (r) defines the keyword 
	 * "far" in C/C++. In order to avoid any name conflicts, 
	 * we use the variable names "zNear" to reprsent "near", 
	 * and "zFar" to reprsent "far".
	 */
	inline
	mat4 Ortho2D( const GLfloat left, const GLfloat right,
		const GLfloat bottom, const GLfloat top ) {
		return Angel::Ortho( left, right, bottom, top, -1.0, 1.0 );
	}

	/**
	 * Furstrum matrix generator
	 * 
	 * Note: Microsoft Windows (r) defines the keyword 
	 * "far" in C/C++. In order to avoid any name conflicts, 
	 * we use the variable names "zNear" to reprsent "near", 
	 * and "zFar" to reprsent "far".
	 */
	inline
	mat4 Frustum( const GLfloat left, const GLfloat right,
		const GLfloat bottom, const GLfloat top,
		const GLfloat zNear, const GLfloat zFar) {
		Angel::mat4 c;
		c[0][0] = 2.0*zNear/(right - left);
		c[0][2] = (right + left)/(right - left);
		c[1][1] = 2.0*zNear/(top - bottom);
		c[1][2] = (top + bottom)/(top - bottom);
		c[2][2] = -(zFar + zNear)/(zFar - zNear);
		c[2][3] = -2.0*zFar*zNear/(zFar - zNear);
		c[3][2] = -1.0;
		c[3][3] = 0.0;
		return c;
	}

	/**
	 * Perspective matrix generator
	 * 
	 * Note: Microsoft Windows (r) defines the keyword 
	 * "far" in C/C++. In order to avoid any name conflicts, 
	 * we use the variable names "zNear" to reprsent "near", 
	 * and "zFar" to reprsent "far".
	 */
	inline
	mat4 Perspective( const GLfloat fovy, const GLfloat aspect, 
		const GLfloat zNear, const GLfloat zFar) {
		GLfloat top   = tan(fovy*DegreesToRadians/2) * zNear;
		GLfloat right = top * aspect;

		Angel::mat4 c;
		c[0][0] = zNear/right;
		c[1][1] = zNear/top;
		c[2][2] = -(zFar + zNear)/(zFar - zNear);
		c[2][3] = -2.0*zFar*zNear/(zFar - zNear);
		c[3][2] = -1.0;
		c[3][3] = 0.0;
		return c;
	}

	/**
	 * Camera position matrix generator.
	 * 
	 * Note: Microsoft Windows (r) defines the keyword 
	 * "far" in C/C++. In order to avoid any name conflicts, 
	 * we use the variable names "zNear" to reprsent "near", 
	 * and "zFar" to reprsent "far".
	 */
	inline
	mat4 LookAt(const vec4& eye, const vec4& at, const vec4& up) {
		Angel::vec4 n = normalize(eye - at);
		Angel::vec4 u = Angel::vec4(normalize(cross(up,n)), 0.0);
		Angel::vec4 v = Angel::vec4(normalize(cross(n,u)), 0.0);
		Angel::vec4 t = Angel::vec4(0.0, 0.0, 0.0, 1.0);
		Angel::mat4 c = Angel::mat4(u, v, n, t);
		return c * Angel::Translate( -eye );
	}

	/**
	 * Generates a normal matrix.
	 */
	inline
	mat3 Normal(const mat4& c){
		Angel::mat3 d;
		GLfloat det;

		det = c[0][0]*c[1][1]*c[2][2]+c[0][1]*c[1][2]*c[2][1]+c[0][2]*c[1][0]*c[2][1]
			-c[2][0]*c[1][1]*c[0][2]-c[1][0]*c[0][1]*c[2][2]-c[0][0]*c[1][2]*c[2][1];

		d[0][0] = (c[1][1]*c[2][2]-c[1][2]*c[2][1])/det;
		d[0][1] = -(c[1][0]*c[2][2]-c[1][2]*c[2][0])/det;
		d[0][2] =  (c[1][0]*c[2][1]-c[1][1]*c[2][0])/det;
		d[1][0] = -(c[0][1]*c[2][2]-c[0][2]*c[2][1])/det;
		d[1][1] = (c[0][0]*c[2][2]-c[0][2]*c[2][0])/det;
		d[1][2] = -(c[0][0]*c[2][1]-c[0][1]*c[2][0])/det;
		d[2][0] =  (c[0][1]*c[1][2]-c[0][2]*c[1][1])/det;
		d[2][1] = -(c[0][0]*c[1][2]-c[0][2]*c[1][0])/det;
		d[2][2] = (c[0][0]*c[1][1]-c[1][0]*c[0][1])/det;

		return d;
	}

	/**
	 * Hardcoded determinant computation for a 4D matrix.
	 */
	inline 
	double determinant(mat4 m) {
		double value;
		value =
		m[3][0]*m[2][1]*m[1][2]*m[0][3] - m[2][0]*m[3][1]*m[1][2]*m[0][3] - m[3][0]*m[1][1]*m[2][2]*m[0][3] + m[1][0]*m[3][1]*m[2][2]*m[0][3]+
		m[2][0]*m[1][1]*m[3][2]*m[0][3] - m[1][0]*m[2][1]*m[3][2]*m[0][3] - m[3][0]*m[2][1]*m[0][2]*m[1][3] + m[2][0]*m[3][1]*m[0][2]*m[1][3]+
		m[3][0]*m[0][1]*m[2][2]*m[1][3] - m[0][0]*m[3][1]*m[2][2]*m[1][3] - m[2][0]*m[0][1]*m[3][2]*m[1][3] + m[0][0]*m[2][1]*m[3][2]*m[1][3]+
		m[3][0]*m[1][1]*m[0][2]*m[2][3] - m[1][0]*m[3][1]*m[0][2]*m[2][3] - m[3][0]*m[0][1]*m[1][2]*m[2][3] + m[0][0]*m[3][1]*m[1][2]*m[2][3]+
		m[1][0]*m[0][1]*m[3][2]*m[2][3] - m[0][0]*m[1][1]*m[3][2]*m[2][3] - m[2][0]*m[1][1]*m[0][2]*m[3][3] + m[1][0]*m[2][1]*m[0][2]*m[3][3]+
		m[2][0]*m[0][1]*m[1][2]*m[3][3] - m[0][0]*m[2][1]*m[1][2]*m[3][3] - m[1][0]*m[0][1]*m[2][2]*m[3][3] + m[0][0]*m[1][1]*m[2][2]*m[3][3];
		return value;
	}

	/**
	 * Hardcoded matrix inversion for a 4D matrix.
	 */
	inline 
	mat4 invert(mat4 m) {
		Angel::mat4 output;
		output[0][0] = m[2][1]*m[3][2]*m[1][3] - m[3][1]*m[2][2]*m[1][3] + m[3][1]*m[1][2]*m[2][3] - m[1][1]*m[3][2]*m[2][3] - m[2][1]*m[1][2]*m[3][3] + m[1][1]*m[2][2]*m[3][3];
		output[1][0] = m[3][0]*m[2][2]*m[1][3] - m[2][0]*m[3][2]*m[1][3] - m[3][0]*m[1][2]*m[2][3] + m[1][0]*m[3][2]*m[2][3] + m[2][0]*m[1][2]*m[3][3] - m[1][0]*m[2][2]*m[3][3];
		output[2][0] = m[2][0]*m[3][1]*m[1][3] - m[3][0]*m[2][1]*m[1][3] + m[3][0]*m[1][1]*m[2][3] - m[1][0]*m[3][1]*m[2][3] - m[2][0]*m[1][1]*m[3][3] + m[1][0]*m[2][1]*m[3][3];
		output[3][0] = m[3][0]*m[2][1]*m[1][2] - m[2][0]*m[3][1]*m[1][2] - m[3][0]*m[1][1]*m[2][2] + m[1][0]*m[3][1]*m[2][2] + m[2][0]*m[1][1]*m[3][2] - m[1][0]*m[2][1]*m[3][2];
		output[0][1] = m[3][1]*m[2][2]*m[0][3] - m[2][1]*m[3][2]*m[0][3] - m[3][1]*m[0][2]*m[2][3] + m[0][1]*m[3][2]*m[2][3] + m[2][1]*m[0][2]*m[3][3] - m[0][1]*m[2][2]*m[3][3];
		output[1][1] = m[2][0]*m[3][2]*m[0][3] - m[3][0]*m[2][2]*m[0][3] + m[3][0]*m[0][2]*m[2][3] - m[0][0]*m[3][2]*m[2][3] - m[2][0]*m[0][2]*m[3][3] + m[0][0]*m[2][2]*m[3][3];
		output[2][1] = m[3][0]*m[2][1]*m[0][3] - m[2][0]*m[3][1]*m[0][3] - m[3][0]*m[0][1]*m[2][3] + m[0][0]*m[3][1]*m[2][3] + m[2][0]*m[0][1]*m[3][3] - m[0][0]*m[2][1]*m[3][3];
		output[3][1] = m[2][0]*m[3][1]*m[0][2] - m[3][0]*m[2][1]*m[0][2] + m[3][0]*m[0][1]*m[2][2] - m[0][0]*m[3][1]*m[2][2] - m[2][0]*m[0][1]*m[3][2] + m[0][0]*m[2][1]*m[3][2];
		output[0][2] = m[1][1]*m[3][2]*m[0][3] - m[3][1]*m[1][2]*m[0][3] + m[3][1]*m[0][2]*m[1][3] - m[0][1]*m[3][2]*m[1][3] - m[1][1]*m[0][2]*m[3][3] + m[0][1]*m[1][2]*m[3][3];
		output[1][2] = m[3][0]*m[1][2]*m[0][3] - m[1][0]*m[3][2]*m[0][3] - m[3][0]*m[0][2]*m[1][3] + m[0][0]*m[3][2]*m[1][3] + m[1][0]*m[0][2]*m[3][3] - m[0][0]*m[1][2]*m[3][3];
		output[2][2] = m[1][0]*m[3][1]*m[0][3] - m[3][0]*m[1][1]*m[0][3] + m[3][0]*m[0][1]*m[1][3] - m[0][0]*m[3][1]*m[1][3] - m[1][0]*m[0][1]*m[3][3] + m[0][0]*m[1][1]*m[3][3];
		output[3][2] = m[3][0]*m[1][1]*m[0][2] - m[1][0]*m[3][1]*m[0][2] - m[3][0]*m[0][1]*m[1][2] + m[0][0]*m[3][1]*m[1][2] + m[1][0]*m[0][1]*m[3][2] - m[0][0]*m[1][1]*m[3][2];
		output[0][3] = m[2][1]*m[1][2]*m[0][3] - m[1][1]*m[2][2]*m[0][3] - m[2][1]*m[0][2]*m[1][3] + m[0][1]*m[2][2]*m[1][3] + m[1][1]*m[0][2]*m[2][3] - m[0][1]*m[1][2]*m[2][3];
		output[1][3] = m[1][0]*m[2][2]*m[0][3] - m[2][0]*m[1][2]*m[0][3] + m[2][0]*m[0][2]*m[1][3] - m[0][0]*m[2][2]*m[1][3] - m[1][0]*m[0][2]*m[2][3] + m[0][0]*m[1][2]*m[2][3];
		output[2][3] = m[2][0]*m[1][1]*m[0][3] - m[1][0]*m[2][1]*m[0][3] - m[2][0]*m[0][1]*m[1][3] + m[0][0]*m[2][1]*m[1][3] + m[1][0]*m[0][1]*m[2][3] - m[0][0]*m[1][1]*m[2][3];
		output[3][3] = m[1][0]*m[2][1]*m[0][2] - m[2][0]*m[1][1]*m[0][2] + m[2][0]*m[0][1]*m[1][2] - m[0][0]*m[2][1]*m[1][2] - m[1][0]*m[0][1]*m[2][2] + m[0][0]*m[1][1]*m[2][2];

		return (double)(1.0/(GLfloat)Angel::determinant(m)) * output;
	}

	/**
	 * [DEPRECATED] Vector substraction.
	 */
	inline
	vec4 minus(const vec4& a, const vec4&  b) {
		Error( "replace with vector subtraction" );
		return Angel::vec4(a[0]-b[0], a[1]-b[1], a[2]-b[2], 0.0);
	}

	/**
	 * [DEPRECATED] Vector info printing.
	 */
	inline
	void printv(const vec4& a) {
		Error( "replace with vector insertion operator" );
		printf("%f %f %f %f \n\n", a[0], a[1], a[2], a[3]);
	}

	/**
	 * [DEPRECATED] Matrix info printing.
	 */
	inline
	void printm(const mat4 a) {
		Error( "replace with matrix insertion operator" );
		for(int i=0; i<4; i++) printf("%f %f %f %f \n", a[i][0], a[i][1], a[i][2], a[i][3]);
		printf("\n");
	}

	/**
	 * [DEPRECATED] Returns an identity matrix.
	 */
	inline
	mat4 identity() {
		Error( "replace with either a matrix constructor or identity method" );
		Angel::mat4 c;
		for(int i=0; i<4; i++) for(int j=0; j<4; j++) c[i][j]=0.0;
		for(int i=0; i<4; i++) c[i][i] = 1.0;
		return c;
	}


}  // namespace Angel

#endif // __ANGEL_MAT_H__
