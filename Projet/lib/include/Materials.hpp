/**
 * Just some materials definitions
 */

#ifndef MATERIALS_HPP
#define MATERIALS_HPP

#include "common.h"
#include "vec.h"

namespace RealisticShading {
	/**
	 * All color dominant variants
	 */
	namespace colors {
		/**
		 * Color red
		 */
		struct red {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(1.0, 0.0, 0.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color orange
		 */
		struct orange {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.992157, 0.513726, 0.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color yellow
		 */
		struct yellow {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(1.0, 0.964706, 0.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color green
		 */
		struct green {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.0, 1.0, 0.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color indigo
		 */
		struct indigo {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.0980392, 0.0, 0.458824, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color blue
		 */
		struct blue {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.0, 0.0, 1.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color violet
		 */
		struct violet {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.635294, 0.0, 1.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color white
		 */
		struct white {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.992157, 0.992157, 0.992157, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color black
		 */
		struct black {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color medium gray
		 */
		struct medium_gray {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.454902, 0.454902, 0.454902, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};

		/**
		 * Color light gray
		 */
		struct light_gray {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.682353, 0.682353, 0.682353, 1.0);
			vec4 specular= vec4(0.0225, 0.0225, 0.0225, 1.0);
			double shininess = 12.8;
		};
	}

	namespace materials {
		/**
		 * glass 
		 */
		struct glass {
			vec4 ambient = vec4(0.0, 0.0, 0.0, 1.0);
			vec4 diffuse = vec4(0.588235, 0.670588, 0.729412, 1.0);
			vec4 specular= vec4(0.9, 0.9, 0.9, 1.0);
			double shininess = 96.0;
		};

		/**
		 * bronze 
		 */
		struct bronze {
			vec4 ambient = vec4(0.2125, 0.1275, 0.054, 1.0);
			vec4 diffuse = vec4(0.714, 0.4284, 0.18144, 1.0);
			vec4 specular= vec4(0.393548, 0.271906, 0.166721, 1.0);
			double shininess = 25.6;
		};

		/**
		 * chrome
		 */
		struct chrome {
			vec4 ambient = vec4(0.25, 0.25, 0.25, 1.0);
			vec4 diffuse = vec4(0.4, 0.4, 0.4, 1.0);
			vec4 specular= vec4(0.774597, 0.774597, 0.774597, 1.0);
			double shininess = 76.8;
		};

		/**
		 * copper 
		 */
		struct copper {
			vec4 ambient = vec4(0.19125, 0.0735, 0.0225, 1.0);
			vec4 diffuse = vec4(0.7038, 0.27048, 0.0828, 1.0);
			vec4 specular= vec4(0.256777, 0.137622, 0.086014, 1.0);
			double shininess = 12.8;
		};

		/**
		 * gold 
		 */
		struct gold {
			vec4 ambient = vec4(0.24725, 0.1995, 0.0745, 1.0);
			vec4 diffuse = vec4(0.75164, 0.60648, 0.22648, 1.0);
			vec4 specular= vec4(0.628281, 0.555802, 0.366065, 1.0);
			double shininess = 51.2;
		};

		/**
		 * ruby 
		 */
		struct ruby {
			vec4 ambient = vec4(0.1745, 0.01175, 0.01175, 0.55);
			vec4 diffuse = vec4(0.61424, 0.04136, 0.04136, 0.55);
			vec4 specular= vec4(0.727811, 0.626959, 0.626959, 0.55);
			double shininess = 76.8;
		};
		
	}
}

#endif