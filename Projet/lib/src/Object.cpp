//////////////////////////////////////////////////////////////////////////////
//
//  --- Object.cpp ---
//  Created by Brian Summa
//
//////////////////////////////////////////////////////////////////////////////


#include "../include/common.h"

/**
 * COORDINATES : 
 * World to Object --> multiply world vectors by INVC,
 * Object to World --> mutliply object vectors by C
 */

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
Object::IntersectionValues Sphere::intersect(vec4 p0_w, vec4 V_w, bool verbose){
	IntersectionValues result;
	result.name = this->name;
	V_w.w = 0.0;
	vec4 V_o = this->INVC * normalize(V_w);
	vec4 p0_o = this->INVC * p0_w;

	/**
	 * check if the ray interacts with the square
	 */
	result.t_o = raySphereIntersection(p0_o, V_o);

	if (result.t_o == std::numeric_limits<double>::infinity()) {
		result.name = "NOT HIT";
		result.t_w = std::numeric_limits<double>::infinity();
	} else {
		// // compute P_o :
		// result.P_o = p0_o + result.t_o * V_o;
		// result.P_w = this->C * result.P_o;
		// if (verbose) {
		// 	std::cout << "P_w : " << result.P_w << std::endl;
		// }
		// // get "real" value of t
		// result.t_w = result.t_o; // length( result.P_w - p0_w ) / length(V_w);
		// // compute normal in object coordinates
		// result.N_o = normalize(result.P_o - vec4(this->mesh.center, 1.0));
		// result.N_o.w = 0.0;
		// // compute normal in world coordinates
		// result.N_w = normalize(this->TRANINVC * result.N_o);
		// result.N_w.w = 0.0;
		// // put color in P_o, since we don't really care about it anymore
		// result.P_o = this->shadingValues.color;

		/**
		 * N.B. : Because of the fucking floating point errors happening all over the place,
		 * we need to set manually every last coordinate of each point to 1, and the vector's
		 * to 0. Just so we don't end up with the length of some normalize vectors at > 1.
		 */
		result.P_o = p0_o + result.t_o * V_o;
		result.P_w = this->C * result.P_o;

		vec4 no = result.P_o - vec4(this->mesh.center, 1.0);	no.w = 0.0;
		vec4 N_o = normalize(no);				N_o.w = 0.0;

		vec4 nw = this->TRANINVC * N_o;				nw.w = 0.0;
		vec4 N_w = normalize(nw);				N_w.w = 0.0;

		result.t_w = result.t_o; // length( result.P_w - p0_w ) / length(V_w);
		result.N_o = N_o;
		result.N_w = N_w;
		result.P_o = this->shadingValues.color;
	}

	return result;
}

/* -------------------------------------------------------------------------- */
/* ------ Ray = p0 + t*V  sphere at origin O and radius r    : Find t ------- */
double Sphere::raySphereIntersection(vec4 p0, vec4 V, vec4 O, double r){
	double t   = std::numeric_limits< double >::infinity();
	/**
	 * We are looking for a point on the surface of the sphere.
	 * [[ in object coordinates ]]
	 * Such a point P is defined (relative to the center point
	 * pC) as such :
	 * 
	 * |P - pC|² - r² = 0                                               (1)
	 * 
	 * (Since it is on the sphere, and thus at a distance r from
	 * the center point pC). But we know the point P must also
	 * be a part of the ray. As such, P can be defined as :
	 * 
	 * P = p0 + t * V                                                   (2)
	 * 
	 * If we plug that into the (1) equation, we get :
	 * 
	 * t²|V|² + 2*|p0-pC|t|V| + |p0-pC|²-r² = 0                         (3)
	 * 
	 * We can now definitively tell if a point is on the sphere or
	 * not just by looking at the determinant of this equation !
	 * We know that if delta [[defined below]] is less than 0, we 
	 * don't have any solutions. If it is equal to 0, then we have
	 * only one solution (but due to the lack of precision of
	 * floating point numbers, we don't check for it), and if it is
	 * higher than 0, then we have two solutions.
	 * 
	 * N.B. : delta = b² - 4 * a * c                                    (4)
	 */
	double a = pow(length(V), 2.0);
	double b = 2.0 * dot(V, O-p0);
	double c = pow(length(p0-O), 2.0) - r * r;

	// compute determinant of sphere
	double det = pow(b,2.0) - 4.0 * a * c;
	// if det positive, ray intersected sphere
	if (det > 0.0) {
		double t1 = (  b - sqrt(det)) / (2.0 * a);
		double t2 = (- b - sqrt(det)) / (2.0 * a);
		if (t2 < t1 && t2 > 0.0) {
			t = t2;
		} else {
			t = t1;
		}
	}
	return t;
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
Object::IntersectionValues Square::intersect(vec4 p0_w, vec4 V_w, bool verbose){
	IntersectionValues result;
	//TODO: Ray-square setup
	vec4 V_o = this->INVC * V_w;
	vec4 p0_o = this->INVC * p0_w;
	result.name = this->name;
	result.t_w = std::numeric_limits<double>::infinity();
	/**
	 * check if the ray intersects the sphere
	 */
	result.t_o = raySquareIntersection(p0_o, V_o);

	if (result.t_o != std::numeric_limits<double>::infinity() && result.t_o > 0.0) {
		result.P_o = p0_o + result.t_o * V_o;
		result.P_w = (this->C) * result.P_o;
		result.P_o = this->shadingValues.color;
		result.N_w = this->TRANINVC * this->mesh.normals[0];
		result.N_w.w = 0.0;
		result.t_w = length(result.P_w - p0_w) / length(V_w);
	} else {
		result.t_w = std::numeric_limits<double>::infinity();
	}

	return result;
}

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
double Square::raySquareIntersection(vec4 p0, vec4 V){
	double t   = std::numeric_limits< double >::infinity();				// Find value of t such that : 
	//TODO: Ray-square intersection;						// P_i = P_0 + u * t

	// we are in object coordinates
	vec4 min = this->mesh.vertices[0];
	vec4 max = this->mesh.vertices[1];

	vec4 norm = this->mesh.normals[1];
	double d = - (norm.x * max.x) - (norm.y * max.y) - (norm.z * max.z);

	double X = norm.x * V.x + norm.y * V.y + norm.z * V.z;
	double Y = norm.x * p0.x + norm.y * p0.y + norm.z * p0.z + d;

	if (X != 0.0) {
		t = (- Y / X);
		vec4 ptInter = (p0 + t * V);
		/**
		 * If we want to delete those weird "holes" at the edges of 2 squares
		 * with one edge in common, we can add the following conditions to the
		 * "if" block right underneath :

		|| 
		nearlyEqual(ptInter.x, min.x, EPSILON) && ptInter.y >= min.y && ptInter.x < max.x && ptInter.y < max.y ||
		ptInter.x >= min.x && nearlyEqual(ptInter.y, min.y, EPSILON) && ptInter.x < max.x && ptInter.y < max.y ||
		ptInter.x >= min.x && ptInter.y >= min.y && nearlyEqual(ptInter.x, max.x, EPSILON) && ptInter.y < max.y ||
		ptInter.x >= min.x && ptInter.y >= min.y && ptInter.x < max.x && nearlyEqual(ptInter.y, max.y, EPSILON)

		 * Be warned though, it fills in the holes, but they might not be illuminated/shaded,
		 * since floating point errors are a thing. They were the reason there was holes in
		 * the first place, those pixels might not be "detected" by the Phong shading method.
		 * (Tested empirically, and deemed to be true.)
		 */
		if ((ptInter.x >= min.x && ptInter.y >= min.y && ptInter.x < max.x && ptInter.y < max.y)) {
			return t;
		} else {
			return -1;
		}
	} else {
		if (Y == 0.0) {
			t = 0.0;
		}
	}

	return t;
}
