#include "./lib/include/common.h"
#include "./lib/include/SourcePath.h"
#include <unistd.h>
#include <chrono>
#include <error.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

using namespace Angel;

// fctHeaders.h contains the function headers, and their documentation !
#include "lib/include/fctHeaders.h"
// ( Also contains the class/functions used to write to PNG )

// Program options :

#define RETURN_AMBIENT_COLOR_IF_EMPTY
#define ENABLE_SHADOWS
//#define ENABLE_TRANSLUCENCY
#define ENABLE_RAY_BOUNCING

#ifdef ENABLE_SHADOWS
	#define APPLY_SOFT_SHADOWS
	#define APPLY_DETERMINISTIC_SOFT_SHADOWS
#endif

// Recursion depth for raytracer
int maxDepth = 1;

unsigned char* imageBuffer;

unsigned int nbThreads = 2;

// Scene variables
enum {_SPHERE, _SQUARE, _BOX};
int scene = _BOX; //Simple sphere, square or cornell box
std::vector < Object * > sceneObjects;
point4 lightPosition;
color4 lightColor;
point4 cameraPosition;

vec4 regularizeColor(vec4 color) {
	vec4 regular = color;
	regular[0] = std::min(1.0, (double)regular[0]);
	regular[1] = std::min(1.0, (double)regular[1]);
	regular[2] = std::min(1.0, (double)regular[2]);
	regular[3] = std::min(1.0, (double)regular[3]);
	return regular;
}

vec4 castRay(vec4 p0, vec4 E, Object *lastHitObject, int depth){
	/**
	 * ========================================== QUESTIONS ==========================================
	 * 
	 * 1. Y'a t'il un quelconque lien entre les variables Ka Kd Ks ? Doivent
	 *    elles toutes être égales à un par exemple ?
	 * 
	 * 2. Si une valeur de K est supérieure à 1, doit on la seuiller à 1 ?
	 *    [Dans le cours tous les K sont entre 0 et 1]
	 * 
	 * 3. Ce PDF est il bon pour l'illumination globale ?
	 *    http://artis.imag.fr/Members/David.Roger/whitted.pdf
	 */

	vec4 color = vec4(0.0,0.0,0.0,0.0);

	if(depth > maxDepth){ return color; }

	// Variables to compute the intersection
	double minDist = std::numeric_limits<double>::infinity();
	Object::IntersectionValues infoClosest;
	Object::IntersectionValues infoCurrent;
	Object* closestObject = NULL;

	// Compute closest intersection
	for (int i = 0; i < sceneObjects.size(); ++i) {
		// Check if the intersection is empty or not
		infoCurrent = sceneObjects[i]->intersect(p0, E);
		// If the object is closer than the closest one
		if ((infoCurrent.t_w) > 0 && (infoCurrent.t_w) < minDist) {
			infoClosest = infoCurrent;
			closestObject = sceneObjects[i];
			minDist = infoCurrent.t_w;
			color = infoCurrent.P_o;
		}
	}

	if (closestObject == NULL) {
		return vec4(0.0,0.0,0.0,0.0);
	}

	#ifdef ENABLE_SHADOWS

	#ifdef APPLY_SOFT_SHADOWS

	#ifdef APPLY_DETERMINISTIC_SOFT_SHADOWS
	double percentInShadow = shadowFeelerBox_Deterministic(infoClosest.P_w, closestObject, 20);
	#else
	double percentInShadow = shadowFeelerBox_Random(infoClosest.P_w, closestObject, 20);
	#endif

	#else
	double percentInShadow = (shadowFeeler(infoClosest.P_w, closestObject) == true) ? 1.5 : 0.5;
	#endif

	if (percentInShadow < 1.0) {
	#endif
		vec4 N = normalize(infoClosest.N_w);                 N.w = 0.0;
		vec4 L = normalize(lightPosition - infoClosest.P_w); L.w = 0.0;
		vec4 V = normalize(p0 - infoClosest.P_w);            V.w = 0.0;
		vec4 R = normalize(2.0 * dot(N,L) * N - L);          R.w = 0.0;

		double theta = dot(N,L);
		double alpha = dot(R,V);
		if (theta < 0.0) {theta = 0.0;}
		if (alpha < 0.0) {alpha = 0.0;}

		// Compute the ambient for the object, with its associated alpha :
		color4 matAmbient = closestObject->shadingValues.color * closestObject->shadingValues.Ka;
		// Compute the diffuse for the object, with its associated alpha :
		color4 matDiffuse = closestObject->shadingValues.color * closestObject->shadingValues.Kd;
		// Compute the specular highlights for the object, with its alpha :
		color4 matSpecular = lightColor * closestObject->shadingValues.Ks;

		color4 objAmbient = (matAmbient * GLState::light_ambient);
		color4 objDiffuse = (matDiffuse * GLState::light_diffuse * theta);
		color4 objSpecular = (matSpecular * GLState::light_specular * pow(alpha, closestObject->shadingValues.Kn));

		objAmbient[3] = 1.0;
		objDiffuse[3] = 1.0;
		objSpecular[3] = 1.0;

		color = (objAmbient + objDiffuse + objSpecular);
		color = regularizeColor(color);

		#ifdef ENABLE_TRANSLUCENCY
			if (closestObject->shadingValues.Kt > 0.0) {
				double translucency_coefficient = closestObject->shadingValues.Kt;
				// Compute color of ray inside sphere : 
				// color4 translucency_color = castRay()
				color4 fullColor = translucency_coefficient * translucency_color + (1.0 - translucency_coefficient) * color;
				color = regularizeColor(fullColor);
			}
		#endif

		#ifdef APPLY_SOFT_SHADOWS
			color4 shadow = vec4(0.0,0.0,0.0,1.0);
			double percentInLight = 1.0 - percentInShadow;
			color = percentInShadow * shadow + percentInLight * color;
		#endif

		#ifdef ENABLE_RAY_BOUNCING
			// Compute the direction of the new vector
			vec4 refracted = 2.0 * dot(V,N) * N - V;
			// Get the color of the bounced ray(s)
			color4 addedColor = regularizeColor(castRay(infoClosest.P_w, refracted, closestObject, depth+1));
			// Blend it with the current color
			color4 realColor = (1-closestObject->shadingValues.Kr)*(color) + closestObject->shadingValues.Kr * addedColor;
			color = realColor;
		#endif
	#ifdef ENABLE_SHADOWS
	} else {
		color = vec4(0.0,0.0,0.0,1.0);
	}
	#endif

	return color;
}

void castRayDebug(vec4 p0, vec4 dir) {
	std::vector<Object::IntersectionValues> intersections;

	for(unsigned int i=0; i < sceneObjects.size(); i++){
		intersections.push_back(sceneObjects[i]->intersect(p0, dir));
		intersections[intersections.size()-1].ID_ = i;
	}

	for(unsigned int i=0; i < intersections.size(); i++){
		if(intersections[i].t_w != std::numeric_limits< double >::infinity()){
			std::cout << "Hit " << intersections[i].name << " " << intersections[i].ID_ << "\n";
			std::cout << "Dist : " << intersections[i].t_w << std::endl;
			std::cout << "P: " <<  intersections[i].P_o << "\n";
			std::cout << "N: " <<  intersections[i].N_w << "\n";
			vec4 L = lightPosition-intersections[i].P_o;
			L  = normalize(L);
			std::cout << "L: " << L << "\n";
		}
	}
}

void castRayDebugClosest(vec4 p0, vec4 dir){
	
	std::vector < Object::IntersectionValues > intersections;

	double dist = std::numeric_limits<double>::infinity();
	Object::IntersectionValues infoClosest;
	Object::IntersectionValues infoCurrent;
	Object* closest = nullptr;
	
	for(unsigned int i=0; i < sceneObjects.size(); i++){
		infoCurrent = sceneObjects[i]->intersect(p0, dir);
		if (infoCurrent.t_w < dist && infoCurrent.t_w > 0.0) {
			infoClosest = infoCurrent;
			infoClosest.ID_ = i;
			dist = infoClosest.t_w;
			closest = sceneObjects[i];
		}
	}

	if (closest != nullptr) {
		std::cout << "Hit " << infoClosest.name << " " << infoClosest.ID_ << "\n";
		std::cout << "Dist : " << infoClosest.t_w << std::endl;
		std::cout << "P: " <<  infoClosest.P_w << "\n";
		std::cout << "N: " <<  infoClosest.N_w << " // Length : " << length(infoClosest.N_w) << " // dot : " << dot(infoClosest.N_w,infoClosest.N_w) << "\n";
		vec4 L = normalize(lightPosition-infoClosest.P_w);
		vec4 V = normalize(p0 - infoClosest.P_w);
		vec4 N = normalize(infoClosest.N_w);
		vec4 R = normalize(2.0 * dot(L,N) * N - L);
		double theta = dot(L,N);
		double alpha = dot(R,V);
		std::cout << "L: " << L << "\n";
		std::cout << "V: " << V << "\n";
		std::cout << "N: " << N << "\n";
		std::cout << "R: " << R << "\n";
		std::cout << "Theta : " << theta << "\n";
		std::cout << "Alpha : " << alpha << "\n";
		std::cout << "In shadow ? " << std::boolalpha << shadowFeelerBox_Deterministic(infoClosest.P_w, closest, 10) << "\n" ;
		std::cout << "Bounces : "; 
		bounceRayVerbose(p0, dir, 0);
		std::cout << std::endl;

	}
}

bool shadowFeeler(vec4 p0, Object *object, bool showInfo) {
	double maxValue = length(lightPosition - p0);
	double minValue = EPSILON;
	Object::IntersectionValues results;
	vec4 dir = lightPosition - p0;
	dir[3] = 0.0;

	for (int i = 0;  i < sceneObjects.size(); ++i) {
		results = sceneObjects[i]->intersect(p0, normalize(lightPosition-p0));
		if (results.t_w != std::numeric_limits<double>::infinity() && results.t_w < maxValue && results.t_w > minValue) {
			if (showInfo == true) {
				std::cout << "Path from " << object->name << " at (" << p0.x << "," << p0.y << "," << p0.z;
				std::cout << ") to light using " << normalize(lightPosition-p0).x << "," << normalize(lightPosition-p0).y ;
				std::cout << "," << normalize(lightPosition-p0).z << ") obstructed by " << results.name << " on (" << results.P_w.x << ",";
				std::cout << results.P_w.y << "," << results.P_w.z << ") with t at " << results.t_w << "." << std::endl;
			}
			return true;
		}
	}
	return false;
}

double shadowFeelerBox_Random(vec4 p0, Object *object, int nbIter){
	/**
	 * The code will check for each object in the scene
	 * if the ray from the point of intersection to the
	 * light is interrupted. In which case we will return
	 * black !
	 */

	Object::IntersectionValues results;
	int nbIntersections = 0;
	double iterations = 4.0 * (double) nbIter;

	for (int i = 0; i < nbIter; ++i) {
		double fMin = 0.0;
		double fMax = 0.15;
		double rX = fMin + ((double)std::rand()/(double)RAND_MAX) * fMax;
		double rZ = fMin + ((double)std::rand()/(double)RAND_MAX) * fMax;
		double minValue = EPSILON;
		double maxValue;
		vec4 localLightPos = lightPosition;

		for (int i = 0;  i < sceneObjects.size(); ++i) {
			if (sceneObjects[i] != object) {
				localLightPos.x += rX;
				localLightPos.y += rZ;
				maxValue = length(localLightPos - p0);
				results = sceneObjects[i]->intersect(p0, normalize(localLightPos - p0));
				if (results.t_w != std::numeric_limits<double>::infinity() && results.t_w < maxValue && results.t_w > minValue) {
					nbIntersections++;
				}
				localLightPos.z -= 2.0 * rZ;
				maxValue = length(localLightPos - p0);
				results = sceneObjects[i]->intersect(p0, normalize(localLightPos - p0));
				if (results.t_w != std::numeric_limits<double>::infinity() && results.t_w < maxValue && results.t_w > minValue) {
					nbIntersections++;
				}
				localLightPos.x -= 2.0 * rX;
				maxValue = length(localLightPos - p0);
				results = sceneObjects[i]->intersect(p0, normalize(localLightPos - p0));
				if (results.t_w != std::numeric_limits<double>::infinity() && results.t_w < maxValue && results.t_w > minValue) {
					nbIntersections++;
				}
				localLightPos.z += 2.0 * rZ;
				maxValue = length(localLightPos - p0);
				results = sceneObjects[i]->intersect(p0, normalize(localLightPos - p0));
				if (results.t_w != std::numeric_limits<double>::infinity() && results.t_w < maxValue && results.t_w > minValue) {
					nbIntersections++;
				}
			}
		}
	}

	double percentage = (double)nbIntersections / iterations;
	return percentage;
}

double shadowFeelerBox_Deterministic(const vec4 p0, const Object *object, const int nbIter){
	/**
	 * The code will check for each object in the scene
	 * if the ray from the point of intersection to the
	 * light is interrupted. In which case we will return
	 * the number of intersected rays !
	 */

	Object::IntersectionValues results;

	int nbIntersections = 0;
	int iterations = 4 * nbIter;

	double step = 0.25 / (double)(nbIter+1);
	double addX = 0.0;
	double addZ = 0.0;
	double minValue = EPSILON;
	double maxValue;
	vec4 localLightPos = lightPosition;

	for (int i = 0; i < nbIter; ++i) {
		addX += step;
		addZ += step;
		localLightPos = lightPosition;

		localLightPos.x += addX;
		localLightPos.z += addZ;
		if (scanAllObjects(p0, localLightPos, object)) {
			nbIntersections ++;
		}

		localLightPos.z -= 2.0 * addZ;
		if (scanAllObjects(p0, localLightPos, object)) {
			nbIntersections ++;
		}

		localLightPos.x -= 2.0 * addX;
		if (scanAllObjects(p0, localLightPos, object)) {
			nbIntersections ++;
		}

		localLightPos.z += 2.0 * addZ;
		if (scanAllObjects(p0, localLightPos, object)) {
			nbIntersections ++;
		}
	}

	double percentage = (double)nbIntersections / (double)iterations;
	return percentage;
}

void bounceRayVerbose(vec4 p0, vec4 dir, int depth) {
	if (depth > maxDepth) {return;}

	double dist = std::numeric_limits<double>::infinity();
	Object::IntersectionValues infoClosest;
	Object::IntersectionValues infoCurrent;
	Object* closest = nullptr;
	
	for(unsigned int i=0; i < sceneObjects.size(); i++){
		infoCurrent = sceneObjects[i]->intersect(p0, dir);
		if (infoCurrent.t_w < dist && infoCurrent.t_w > 0.0) {
			infoClosest = infoCurrent;
			infoClosest.ID_ = i;
			dist = infoClosest.t_w;
			closest = sceneObjects[i];
		}
	}

	infoClosest.N_w[3] = 0.0;
	infoClosest.P_w[3] = 1.0;
	vec4 N = normalize(infoClosest.N_w);                 N.w = 0.0;
	vec4 L = normalize(lightPosition - infoClosest.P_w); L.w = 0.0;
	vec4 V = normalize(p0 - infoClosest.P_w);            V.w = 0.0;
	vec4 R = normalize(2.0 * dot(N,L) * N - L);          R.w = 0.0;
	vec4 refracted = 2.0 * dot(V,N) * N - V;

	std::cout << "HIT ! : " << infoClosest.name << " // ";

	bounceRayVerbose(infoClosest.P_w, refracted, depth+1);
	return;
}

bool scanAllObjects(const vec4 p0, const vec4 light, const Object* closest) {
	double minValue = EPSILON;
	double maxValue = length(light - p0);
	vec4 dir = normalize(light - p0);

	Object::IntersectionValues result;

	for (int i = 0; i < sceneObjects.size(); ++i) {
		result = sceneObjects[i]->intersect(p0, dir);
		if (result.t_w < maxValue && result.t_w > minValue) {
			// if (sceneObjects[i] == closest) {
			// 	std ::cout << "/same";
			// }
			return true;
		}
	}
	return false;
}

void rayTrace(){
	std::cout << "Warning : we make the supposition the area light is on the plane XZ." << "\n";
	std::cout << "          If it is not, we might observe some weird soft shadows." << "\n";

	unsigned char *buffer = new unsigned char[GLState::window_width*GLState::window_height*4];

	auto startTime = std::chrono::high_resolution_clock::now();

	for(unsigned int i=0; i < GLState::window_width; i++){
		for(unsigned int j=0; j < GLState::window_height; j++){
			int idx = j*GLState::window_width+i;
			std::vector < vec4 > ray_o_dir = findRay(i,j);
			vec4 color = castRay(ray_o_dir[0], vec4(ray_o_dir[1].x, ray_o_dir[1].y, ray_o_dir[1].z,0.0), NULL, 0);
			buffer[4*idx]   = color.x*255;
			buffer[4*idx+1] = color.y*255;
			buffer[4*idx+2] = color.z*255;
			buffer[4*idx+3] = color.w*255;
		}
	}

	auto endTime = std::chrono::high_resolution_clock::now();

	write_image("output.png", buffer, GLState::window_width, GLState::window_height, 4);

	auto imageWriteTime = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double, std::milli> timeRaytracing = endTime - startTime;
	std::chrono::duration<double, std::milli> timeWritingImg = imageWriteTime - endTime;

	std::cout << "Raytraced image computation took " << timeRaytracing.count() << " ms.\n";
	std::cout << "The image took " << timeWritingImg.count() << " ms to write to disk.\n";

	delete[] buffer;
}

void rayTrace_MT() {
	/**
	 * We will launch 2 threads for the test, and then we'll see about the rest.
	 */

	std::cout << std::endl << "Info    : You are using the multi-threaded version of the raytracer." << std::endl << std::endl;
	std::cout << "Warning : we make the supposition the area light is on the plane XZ." << "\n";
	std::cout << "          If it is not, we might observe some weird soft shadows." << "\n";

	// unsigned char *buffer = new unsigned char[GLState::window_width*GLState::window_height*4];

	int alignedMemory = posix_memalign((void**)&imageBuffer, 0x1000, GLState::window_width*GLState::window_height*4 * sizeof(unsigned char));
	if (alignedMemory != 0) {
		perror("posix_memalign(3) ");
		std::cerr << "could not align memory" << std::endl;
		rayTrace();
		exit(EXIT_FAILURE);
	}

	/**
	 * Open a shared memory segment here, and launch threads later.
	 */
	// get key
	key_t shmKey = ftok("./raytracerSHM", 1);
	if (shmKey < 0) {
		perror("ftok(3) ");
		std::cerr << "Could not open the file for SHM. Falling back to regular raytracing method ..." << std::endl;
		rayTrace();
		exit(EXIT_FAILURE);
	}
	// get shm id
	int shmId = shmget(shmKey, GLState::window_height*GLState::window_width*4, IPC_CREAT | 0660);
	if (shmId < 0) {
		perror("shmget(2) ");
		std::cerr << "Could not open SHM segment. Falling back to regular raytracing methods ..." << std::endl;
		rayTrace();
		exit(EXIT_FAILURE);
	}
	// attach shm to memory at address in imageBuffer
	imageBuffer = (unsigned char*)shmat(shmId, imageBuffer, SHM_RND | 0660);
	if ((void*)imageBuffer == (void*)-1) {
		perror("shmat(3) ");
		std::cerr << "Could not attach SHM segment. Falling back to regular raytracing methods ..." << std::endl;
		rayTrace();
		int destSHM = shmctl(shmId, IPC_RMID, NULL);
		if (destSHM != 0) {
			perror("shmctl(3) ");
			std::cerr << "Could not close SHM." << std::endl;
		}
		exit(EXIT_FAILURE);
	}
	// here, SHM has been started, and we need to launch threads :

	threadInfo* infos = new threadInfo[nbThreads];
	unsigned int step = GLState::window_height / nbThreads;
	for (unsigned int i = 0; i < nbThreads; ++i) {
		infos[i] = threadInfo{i*step, (i+1)*step};
	}
	pthread_t* ids = new pthread_t[nbThreads];

	std::cout << "Window height : " << GLState::window_height << std::endl;

	auto startTime = std::chrono::high_resolution_clock::now();

	/**
	 * start threads
	 */
	for (unsigned int i = 0; i < nbThreads; ++i) {
		if (pthread_create(&(ids[i]), NULL, rayTracing_Thread_Worker, (void*)&(infos[i]) ) != 0) {
			perror("pthread_create(3) ");
			std::cerr << "Could not start threads. Falling back to regular raytracing methods ..." << std::endl;
			rayTrace();
			int destSHM = shmctl(shmId, IPC_RMID, NULL);
			if (destSHM != 0) {
				perror("shmctl(3) ");
				std::cerr << "Could not close SHM." << std::endl;
			}
			exit(EXIT_FAILURE);
		}
	}

	/**
	 * join threads
	 */
	for(unsigned int i = 0; i < nbThreads; ++i) {
		if (pthread_join(ids[i], NULL) != 0) {
			perror("pthread_join(3) ");
			std::cerr << "Could not wait for thread. Falling back to regular raytracing methods ..." << std::endl;
			rayTrace();
			int destSHM = shmctl(shmId, IPC_RMID, NULL);
			if (destSHM != 0) {
				perror("shmctl(3) ");
				std::cerr << "Could not close SHM." << std::endl;
			}
			exit(EXIT_FAILURE);
		}
	}

	auto endTime = std::chrono::high_resolution_clock::now();

	write_image("output.png", imageBuffer, GLState::window_width, GLState::window_height, 4);

	auto imageWriteTime = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double, std::milli> timeRaytracing = endTime - startTime;
	std::chrono::duration<double, std::milli> timeWritingImg = imageWriteTime - endTime;

	int destSHM = shmctl(shmId, IPC_RMID, NULL);
	if (destSHM != 0) {
		perror("shmctl(3) ");
		std::cerr << "Could not close SHM." << std::endl;
	}

	std::cout << "Raytraced image computation took " << timeRaytracing.count() << " ms.\n";
	std::cout << "The image took " << timeWritingImg.count() << " ms to write to disk.\n";

	// delete[] buffer;
}

void* rayTracing_Thread_Worker(void* info) {
	threadInfo tInfo = threadInfo(*( (threadInfo*)info ));

	unsigned int lowerBound = tInfo.lowerBound;
	unsigned int higherBound = lowerBound + tInfo.heightToRender;

	/**
	 * Opened the shared memory segment in rayTrace_MT(), got the buffer info from there
	 */

	std::cout << "Worker started, printing from line " << lowerBound << " to " << higherBound << std::endl;

	for(unsigned int i=0; i < GLState::window_width; i++){
		for(unsigned int j=lowerBound; j < higherBound; j++){
			int idx = j*GLState::window_width+i;
			std::vector < vec4 > ray_o_dir = findRay(i,j);
			vec4 color = castRay(ray_o_dir[0], vec4(ray_o_dir[1].x, ray_o_dir[1].y, ray_o_dir[1].z,0.0), NULL, 0);
			imageBuffer[4*idx]   = color.x*255;
			imageBuffer[4*idx+1] = color.y*255;
			imageBuffer[4*idx+2] = color.z*255;
			imageBuffer[4*idx+3] = color.w*255;
		}
	}

	std::cout << "Finished casting all rays " << std::endl;

	return NULL;
}

int main(void){
	
	GLFWwindow* window;
	
	glfwSetErrorCallback(error_callback);
	
	if (!glfwInit())
		exit(EXIT_FAILURE);
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	glfwWindowHint(GLFW_SAMPLES, 4);
	
	window = glfwCreateWindow(768, 768, "OpenGL : Raytracer", NULL, NULL);
	if (!window){
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	
	glfwSetKeyCallback(window, keyCallback);
	glfwSetMouseButtonCallback(window, mouseClick);
	glfwSetCursorPosCallback(window, mouseMove);
	
	
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glfwSwapInterval(1);
	
	switch(scene){
		case _SPHERE:
			initUnitSphere();
			break;
		case _SQUARE:
			initUnitSquare();
			break;
		case _BOX:
			initCornellBox();
			break;
	}
	
	initGL();
	
	while (!glfwWindowShouldClose(window)){
		
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		
		GLState::window_height = height;
		GLState::window_width  = width;
		
		glViewport(0, 0, width, height);
		
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		mat4 track_ball =  mat4(GLState::curmat[0][0], GLState::curmat[1][0],
					GLState::curmat[2][0], GLState::curmat[3][0],
					GLState::curmat[0][1], GLState::curmat[1][1],
					GLState::curmat[2][1], GLState::curmat[3][1],
					GLState::curmat[0][2], GLState::curmat[1][2],
					GLState::curmat[2][2], GLState::curmat[3][2],
					GLState::curmat[0][3], GLState::curmat[1][3],
					GLState::curmat[2][3], GLState::curmat[3][3]);
		
		GLState::sceneModelView  =  Translate(-cameraPosition) *   //Move Camera Back
		Translate(GLState::ortho_x, GLState::ortho_y, 0.0) * track_ball *                   //Rotate Camera
		Scale(GLState::scalefactor,
			GLState::scalefactor,
			GLState::scalefactor);   //User Scale
		
		GLfloat aspect = GLfloat(width)/height;
		
		switch(scene){
			case _SPHERE:
			case _SQUARE:
				GLState::projection = Perspective( 45.0, aspect, 0.01, 100.0 );
				break;
			case _BOX:
				GLState::projection = Perspective( 45.0, aspect, 4.5, 100.0 );
				break;
		}
		
		glUniformMatrix4fv( GLState::Projection, 1, GL_TRUE, GLState::projection);
		
		for(unsigned int i=0; i < sceneObjects.size(); i++){
			drawObject(sceneObjects[i], GLState::objectVao[i], GLState::objectBuffer[i]);
		}
		
		glfwSwapBuffers(window);
		glfwPollEvents();
		
	}
	
	glfwDestroyWindow(window);
	
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

std::vector <vec4> findRay(GLdouble x, GLdouble y){
	y = GLState::window_height-y;

	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	GLdouble modelViewMatrix[16];
	GLdouble projectionMatrix[16];
	for(unsigned int i=0; i < 4; i++){
		for(unsigned int j=0; j < 4; j++){
			modelViewMatrix[j*4+i]  =  GLState::sceneModelView[i][j];
			projectionMatrix[j*4+i] =  GLState::projection[i][j];
		}
	}


	GLdouble nearPlaneLocation[3];
	_gluUnProject(x, y, 0.0, modelViewMatrix, projectionMatrix,
			viewport, &nearPlaneLocation[0], &nearPlaneLocation[1],
			&nearPlaneLocation[2]);

	GLdouble farPlaneLocation[3];
	_gluUnProject(x, y, 1.0, modelViewMatrix, projectionMatrix,
			viewport, &farPlaneLocation[0], &farPlaneLocation[1],
			&farPlaneLocation[2]);


	vec4 ray_origin = vec4(nearPlaneLocation[0], nearPlaneLocation[1], nearPlaneLocation[2], 1.0);
	vec3 temp = vec3(farPlaneLocation[0]-nearPlaneLocation[0],
			 farPlaneLocation[1]-nearPlaneLocation[1],
			 farPlaneLocation[2]-nearPlaneLocation[2]);
	temp = normalize(temp);
	vec4 ray_dir = vec4(temp.x, temp.y, temp.z, 0.0);

	std::vector < vec4 > result(2);
	result[0] = ray_origin;
	result[1] = ray_dir;

	return result;
}

void initGL(){
	
	GLState::light_ambient  = vec4(lightColor.x, lightColor.y, lightColor.z, 1.0 );
	GLState::light_diffuse  = vec4(lightColor.x, lightColor.y, lightColor.z, 1.0 );
	GLState::light_specular = vec4(lightColor.x, lightColor.y, lightColor.z, 1.0 );
	
	
	std::string vshader = source_path + "/shaders/vshader.glsl";
	std::string fshader = source_path + "/shaders/fshader.glsl";
	
	GLchar* vertex_shader_source = readShaderSource(vshader.c_str());
	GLchar* fragment_shader_source = readShaderSource(fshader.c_str());
	
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, (const GLchar**) &vertex_shader_source, NULL);
	glCompileShader(vertex_shader);
	check_shader_compilation(vshader, vertex_shader);
	
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, (const GLchar**) &fragment_shader_source, NULL);
	glCompileShader(fragment_shader);
	check_shader_compilation(fshader, fragment_shader);
	
	GLState::program = glCreateProgram();
	glAttachShader(GLState::program, vertex_shader);
	glAttachShader(GLState::program, fragment_shader);
	
	glLinkProgram(GLState::program);
	check_program_link(GLState::program);
	
	glUseProgram(GLState::program);
	
	glBindFragDataLocation(GLState::program, 0, "fragColor");
	
	// set up vertex arrays
	GLState::vPosition = glGetAttribLocation( GLState::program, "vPosition" );
	GLState::vNormal = glGetAttribLocation( GLState::program, "vNormal" );
	
	// Retrieve transformation uniform variable locations
	GLState::ModelView = glGetUniformLocation( GLState::program, "ModelView" );
	GLState::NormalMatrix = glGetUniformLocation( GLState::program, "NormalMatrix" );
	GLState::ModelViewLight = glGetUniformLocation( GLState::program, "ModelViewLight" );
	GLState::Projection = glGetUniformLocation( GLState::program, "Projection" );
	
	GLState::objectVao.resize(sceneObjects.size());
	glGenVertexArrays( sceneObjects.size(), &GLState::objectVao[0] );
	
	GLState::objectBuffer.resize(sceneObjects.size());
	glGenBuffers( sceneObjects.size(), &GLState::objectBuffer[0] );
	
	for(unsigned int i=0; i < sceneObjects.size(); i++){
		glBindVertexArray( GLState::objectVao[i] );
		glBindBuffer( GL_ARRAY_BUFFER, GLState::objectBuffer[i] );
		size_t vertices_bytes = sceneObjects[i]->mesh.vertices.size()*sizeof(vec4);
		size_t normals_bytes  =sceneObjects[i]->mesh.normals.size()*sizeof(vec3);
		
		glBufferData( GL_ARRAY_BUFFER, vertices_bytes + normals_bytes, NULL, GL_STATIC_DRAW );
		size_t offset = 0;
		glBufferSubData( GL_ARRAY_BUFFER, offset, vertices_bytes, &sceneObjects[i]->mesh.vertices[0] );
		offset += vertices_bytes;
		glBufferSubData( GL_ARRAY_BUFFER, offset, normals_bytes,  &sceneObjects[i]->mesh.normals[0] );
		
		glEnableVertexAttribArray( GLState::vNormal );
		glEnableVertexAttribArray( GLState::vPosition );
		
		glVertexAttribPointer( GLState::vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
		glVertexAttribPointer( GLState::vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertices_bytes));
		
	}
	
	
	
	glEnable( GL_DEPTH_TEST );
	glShadeModel(GL_SMOOTH);
	
	glClearColor( 0.8, 0.8, 1.0, 1.0 );
	
	//Quaternion trackball variables, you can ignore
	GLState::scaling  = 0;
	GLState::moving   = 0;
	GLState::panning  = 0;
	GLState::beginx   = 0;
	GLState::beginy   = 0;
	
	TrackBall::matident(GLState::curmat);
	TrackBall::trackball(GLState::curquat , 0.0f, 0.0f, 0.0f, 0.0f);
	TrackBall::trackball(GLState::lastquat, 0.0f, 0.0f, 0.0f, 0.0f);
	TrackBall::add_quats(GLState::lastquat, GLState::curquat, GLState::curquat);
	TrackBall::build_rotmatrix(GLState::curmat, GLState::curquat);
	
	GLState::scalefactor = 1.0;
	GLState::render_line = false;
	
}

static void error_callback(int error, const char* description) {
	fprintf(stderr, "Error: %s\n", description);
}

void initCornellBox(){
	cameraPosition = point4( 0.0, 0.0, 6.0, 1.0 );
	lightPosition = point4( 0.0, 1.5, 0.0, 1.0 );
	lightColor = color4( 1.0, 1.0, 1.0, 1.0);
	
	sceneObjects.clear();
	
	{ //Back Wall
		sceneObjects.push_back(new Square("Back Wall"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(1.0,1.0,1.0,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(Translate(0.0, 0.0, -2.0)*Scale(2.0,2.0,1.0));
	}
	
	{ //Left Wall
		sceneObjects.push_back(new Square("Left Wall"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(1.0,0.0,0.0,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(RotateY(90)*Translate(0.0, 0.0, -2.0)*Scale(2.0,2.0,1.0));
	}
	
	{ //Right Wall
		sceneObjects.push_back(new Square("Right Wall"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(0.5,0.0,0.5,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(RotateY(-90)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0 ));
	}
	
	{ //Floor
		sceneObjects.push_back(new Square("Floor"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(1.0,1.0,1.0,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(RotateX(-90)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0));
	}
	
	{ //Ceiling
		sceneObjects.push_back(new Square("Ceiling"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(1.0,1.0,1.0,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(RotateX(90)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0));
	}
	
	{ //Front Wall
		sceneObjects.push_back(new Square("Front Wall"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(0.0,1.0,1.0,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(RotateY(180)*Translate(0.0, 0.0, -2.0)*Scale(2.0, 2.0, 1.0));
	}
	
	
	{
	sceneObjects.push_back(new Sphere("Glass sphere"));
	Object::ShadingValues _shadingValues;
	_shadingValues.color = vec4(1.0,0.0,0.0,1.0);
	_shadingValues.Ka = 0.0;
	_shadingValues.Kd = 0.0;
	_shadingValues.Ks = 0.0;
	_shadingValues.Kn = 16.0;
	_shadingValues.Kt = 1.0;
	_shadingValues.Kr = 0.1;
	sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
	sceneObjects[sceneObjects.size()-1]->setModelView(Translate(1.0, -1.25, 0.5)*Scale(0.75, 0.75, 0.75));
	}
	
	{
	sceneObjects.push_back(new Sphere("Mirrored Sphere"));
	Object::ShadingValues _shadingValues;
	_shadingValues.color = vec4(1.0,1.0,1.0,1.0);
	_shadingValues.Ka = 0.3;
	_shadingValues.Kd = 0.4;
	_shadingValues.Ks = 1.0;
	_shadingValues.Kn = 16.0;
	_shadingValues.Kt = 0.0;
	_shadingValues.Kr = 0.3; // def : 0.0
	sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
	sceneObjects[sceneObjects.size()-1]->setModelView(Translate(-1.0, -1.25, -1.0)*Scale(0.75, 0.75, 0.75));
	}
}

void initUnitSquare(){
	cameraPosition = point4( 0.0, 0.0, 3.0, 1.0 );
	lightPosition = point4( 0.0, 0.0, 4.0, 1.0 );
	lightColor = color4( 1.0, 1.0, 1.0, 1.0);
	
	sceneObjects.clear();
	
	{ //Back Wall
		sceneObjects.push_back(new Square("Unit Square"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(0.0,1.0,1.0,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(mat4());
	}
	
}

void initUnitSphere(){
	cameraPosition = point4( 0.0, 0.0, 3.0, 1.0 );
	lightPosition = point4( 0.0, 0.0, 4.0, 1.0 );
	lightColor = color4( 1.0, 1.0, 1.0, 1.0);

	sceneObjects.clear();

	{
		sceneObjects.push_back(new Sphere("Diffuse sphere Blue"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(0.4,0.1,0.5,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(mat4()* Translate(1.0,0.0,-0.0)* Scale(0.5,0.5,0.5) );  //* Translate(1.0,0.0,0.0) * Angel::Scale(0.1,0.1,0.1)
	}

	{
		sceneObjects.push_back(new Sphere("Diffuse sphere Brown"));
		Object::ShadingValues _shadingValues;
		_shadingValues.color = vec4(0.5,0.3,0.2,1.0);
		_shadingValues.Ka = 0.0;
		_shadingValues.Kd = 1.0;
		_shadingValues.Ks = 0.0;
		_shadingValues.Kn = 16.0;
		_shadingValues.Kt = 0.0;
		_shadingValues.Kr = 0.0;
		sceneObjects[sceneObjects.size()-1]->setShadingValues(_shadingValues);
		sceneObjects[sceneObjects.size()-1]->setModelView(mat4()* Translate(-0.5,0.5,-0.5));  //* Translate(1.0,0.0,0.0) * Angel::Scale(0.1,0.1,0.1)
	}
	
}

void drawObject(Object * object, GLuint vao, GLuint buffer){

	color4 material_ambient(object->shadingValues.color.x*object->shadingValues.Ka,
				object->shadingValues.color.y*object->shadingValues.Ka,
				object->shadingValues.color.z*object->shadingValues.Ka, 1.0 );
	color4 material_diffuse(object->shadingValues.color.x,
				object->shadingValues.color.y,
				object->shadingValues.color.z, 1.0 );
	color4 material_specular(object->shadingValues.Ks,
				 object->shadingValues.Ks,
				 object->shadingValues.Ks, 1.0 );
	float  material_shininess = object->shadingValues.Kn;

	color4 ambient_product  = GLState::light_ambient * material_ambient;
	color4 diffuse_product  = GLState::light_diffuse * material_diffuse;
	color4 specular_product = GLState::light_specular * material_specular;

	glUniform4fv( glGetUniformLocation(GLState::program, "AmbientProduct"), 1, ambient_product );
	glUniform4fv( glGetUniformLocation(GLState::program, "DiffuseProduct"), 1, diffuse_product );
	glUniform4fv( glGetUniformLocation(GLState::program, "SpecularProduct"), 1, specular_product );
	glUniform4fv( glGetUniformLocation(GLState::program, "LightPosition"), 1, lightPosition );
	glUniform1f(  glGetUniformLocation(GLState::program, "Shininess"), material_shininess );

	glBindVertexArray(vao);
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glVertexAttribPointer( GLState::vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	glVertexAttribPointer( GLState::vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(object->mesh.vertices.size()*sizeof(vec4)) );

	mat4 objectModelView = GLState::sceneModelView*object->getModelView();


	glUniformMatrix4fv( GLState::ModelViewLight, 1, GL_TRUE, GLState::sceneModelView);
	glUniformMatrix3fv( GLState::NormalMatrix, 1, GL_TRUE, Normal(objectModelView));
	glUniformMatrix4fv( GLState::ModelView, 1, GL_TRUE, objectModelView);

	glDrawArrays( GL_TRIANGLES, 0, object->mesh.vertices.size() );
}

void mouseMove(GLFWwindow* window, double x, double y){
	
	int W, H;
	glfwGetFramebufferSize(window, &W, &H);
	
	
	float dx=(x-GLState::beginx)/(float)W;
	float dy=(GLState::beginy-y)/(float)H;
	
	if (GLState::panning)
		{
		GLState::ortho_x  +=dx;
		GLState::ortho_y  +=dy;
		
		GLState::beginx = x; GLState::beginy = y;
		return;
		}
	else if (GLState::scaling)
		{
		GLState::scalefactor *= (1.0f+dx);
		
		GLState::beginx = x;GLState::beginy = y;
		return;
		}
	else if (GLState::moving)
		{
		TrackBall::trackball(GLState::lastquat,
				(2.0f * GLState::beginx - W) / W,
				(H - 2.0f * GLState::beginy) / H,
				(2.0f * x - W) / W,
				(H - 2.0f * y) / H );

		TrackBall::add_quats(GLState::lastquat, GLState::curquat, GLState::curquat);
		TrackBall::build_rotmatrix(GLState::curmat, GLState::curquat);
		
		GLState::beginx = x;GLState::beginy = y;
		return;
		}
}

static void mouseClick(GLFWwindow* window, int button, int action, int mods){
	if (GLFW_RELEASE == action){
		GLState::moving=GLState::scaling=GLState::panning=false;
		return;
	}

	if( mods & GLFW_MOD_SHIFT){
		GLState::scaling=true;
	}else if( mods & GLFW_MOD_ALT ){
		GLState::panning=true;
	}else{
		GLState::moving=true;
		TrackBall::trackball(GLState::lastquat, 0, 0, 0, 0);
	}

	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	GLState::beginx = xpos; GLState::beginy = ypos;

	std::vector < vec4 > ray_o_dir = findRay(xpos, ypos);
	castRayDebugClosest(ray_o_dir[0], vec4(ray_o_dir[1].x, ray_o_dir[1].y, ray_o_dir[1].z,0.0));	
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	if (key == GLFW_KEY_1 && action == GLFW_PRESS){
		scene = _SPHERE;
		initUnitSphere();
	}
	if (key == GLFW_KEY_2 && action == GLFW_PRESS){
		scene = _SQUARE;
		initUnitSquare();
	}
	if (key == GLFW_KEY_3 && action == GLFW_PRESS){
		scene = _BOX;
		initCornellBox();
	}
	if (key == GLFW_KEY_R && action == GLFW_PRESS) {
		rayTrace_MT();
	}
	if (key == GLFW_KEY_Q && action == GLFW_PRESS) {
		rayTrace();
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}
